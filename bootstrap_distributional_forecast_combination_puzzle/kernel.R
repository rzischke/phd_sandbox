source("mararch.R")

library(ggplot2, quietly = TRUE)
library(grid, quietly = TRUE)
library(gridExtra, quietly = TRUE)
library(lemon, warn.conflicts = FALSE, quietly = TRUE)
library(dplyr, quietly = TRUE)
library(distributional, quietly = TRUE)
library(RColorBrewer, quietly = TRUE)
library(latex2exp, quietly = TRUE)

dgp <- mararch(
  1.0,
  list(
    ararch(
      ar.coefs = c(1, 0.5),
      arch.coefs = c(1,0.5)
    )
  )
)

model <- mararch(
  c(0.7359144,0.2640856),
  list(
    ararch(
      ar.coefs = c(1.0109313, 0.4956316),
      arch.coefs = c(1.992832)
    ),
    ararch(
      ar.coefs = c(2.002449),
      arch.coefs = c(1.2114243,0.5528235)
    )
  )
)

mle.sample.sizes <- c(100, 200, 450, 1000, 2000)
num.sample.sizes <- length(mle.sample.sizes)
num.mles = 20000
ytm2 <- 2.0
ytm1 <- 1.5

sample.forecast.distributions <- function(mles, past) {
  data <- c(past,NA_real_)
  forecast.distributions <- vector("list", length(mles))
  for (i in 1:length(mles)) {
    forecast.distributions[[i]] <- mararch.one.step.forecasts(mles[[i]],data)[length(data)]
  }
  return(forecast.distributions)
}

forecast.distribution.kl <- function(dgp, est, past, n) {
  true.conditional <- sample.forecast.distributions(list(dgp), past)[[1]]
  draws <- generate(true.conditional, n)[[1]]
  vectorfied.est <- mararch.vectorfy(est)
  weights.est <- est$weights
  conditional.means.est <- mararch.conditional.means(vectorfied.est, past)
  conditional.variances.est <- mararch.conditional.variances(vectorfied.est, past, conditional.means.est)
  conditional.means.est <- conditional.means.est[nrow(conditional.means.est),]
  conditional.variances.est <- conditional.variances.est[nrow(conditional.variances.est),]
  densities <- matrix(data = NA, nrow = n, ncol = length(weights.est))
  for (j in 1:length(weights.est)) {
    densities[,j] <- dnorm(draws, conditional.means.est[j], sqrt(conditional.variances.est[j]))
  }
  log.likelihoods <- log(densities %*% weights.est)
  return(mean(log.likelihoods))
}

mararch.forecast.combination.delta.score.experiment <- function(
  dgp.mararch,       # devectorfied mararch model
  model.mararch,     # devectorfied mararch model
  rho.members,
  rho.combination,
  mle.sample.sizes,
  opt.sample.size,
  div.sample.size,
  num.mles
) {
  mle.sample.sizes <- sort(mle.sample.sizes)
  total.sample.size <- max(mle.sample.sizes) + div.sample.size
  num.mle.sample.sizes <- length(mle.sample.sizes)
  
  results <- matrix(
    nrow = num.mle.sample.sizes,
    ncol = 1 + 6*num.mles,
    dimnames = list(
      NULL,
      c(
        "sample.size",
        paste0("fix.mle.kl.", 1:num.mles),
        paste0("est.mle.kl.", 1:num.mles),
        paste0("delta.est.fix.mle.kl.", 1:num.mles),
        paste0("fix.mle.E.kl.", 1:num.mles),
        paste0("est.mle.E.kl.", 1:num.mles),
        paste0("delta.est.fix.mle.E.kl.", 1:num.mles)
      )
    )
  )
  
  results[,"sample.size"] <- mle.sample.sizes
  
  opt.comb.est.sample <- mararch.sim(dgp.mararch, opt.sample.size)$sample
  opt.comb.mararch <- mararch.comb.log.score(model.mararch, opt.comb.est.sample, rho.members, rho.combination)
  
  simulate.mle.impl <- function(m, sample) {
    print(paste("MLE",m))
    fix.col <- paste0("fix.mle.kl.", m)
    est.col <- paste0("est.mle.kl.", m)
    delta.col <- paste0("delta.est.fix.mle.kl.", m)
    fix.E.col <- paste0("fix.mle.E.kl.", m)
    est.E.col <- paste0("est.mle.E.kl.", m)
    delta.E.col <- paste0("delta.est.fix.mle.E.kl.", m)
    for (row in 1:length(mle.sample.sizes)) {
      n <- mle.sample.sizes[row]
      if (n %% 50 == 0) {
        print(paste("n =", n))
      }
      
      fix.mle <- mararch.comb.log.score(opt.comb.mararch, sample[1:n], rho.members, rho.combination, estimate.weights = FALSE)
      results[row, fix.col] <<- forecast.distribution.kl(dgp, fix.mle, c(ytm2, ytm1), div.sample.size)
      results[row, fix.E.col] <<- mararch.kl(dgp.mararch, sample[(n+1):total.sample.size], fix.mle)
      
      est.mle <- mararch.comb.log.score(opt.comb.mararch, sample[1:n], rho.members, rho.combination, estimate.weights = TRUE)
      results[row, est.col] <<- forecast.distribution.kl(dgp, est.mle, c(ytm2, ytm1), div.sample.size)
      results[row, est.E.col] <<- mararch.kl(dgp.mararch, sample[(n+1):total.sample.size], est.mle)
    }
    results[,delta.col] <<- results[,est.col] - results[,fix.col]
    results[,delta.E.col] <<- results[,est.E.col] - results[,fix.E.col]
  }
  
  simulate.mle <- function(m) {
    sample <- mararch.sim(dgp.mararch, total.sample.size)$sample
    tryCatch(
      simulate.mle.impl(m, sample),
      error = function(e) {
        print("MLE Failed, trying again with a different sample.")
        sample2 <- mararch.sim(dgp.mararch, total.sample.size)$sample
        simulate.mle.impl(m, sample2)
      }
    )
  }
  
  for (m in 1:num.mles) {
    simulate.mle(m)
  }
  
  return(results)
}

results <- mararch.forecast.combination.delta.score.experiment(
  dgp,
  model,
  rho.members = 0.5,
  rho.combination = 0.5,
  mle.sample.sizes = mle.sample.sizes,
  opt.sample.size = 1000000,
  div.sample.size = 50000,
  num.mles = num.mles
)

plot.frame <- tibble(
  score = c(
    rep("Fixed conditioning values", num.mles*num.sample.sizes),
    rep("Expectation over conditioning values", num.mles*num.sample.sizes)
  ),
  sample_size = rep(mle.sample.sizes, each = num.mles, times = 2),
  root.n.kl.delta = c(
    sapply(1:num.sample.sizes, function(x) { sqrt(mle.sample.sizes[x])*results[x,(2+2*num.mles):(1+3*num.mles)] }),
    sapply(1:num.sample.sizes, function(x) { sqrt(mle.sample.sizes[x])*results[x,(2+5*num.mles):(1+6*num.mles)] })
  )
)

plots.list <- vector("list", num.sample.sizes)
for (i in 1:num.sample.sizes) {
  ss <- mle.sample.sizes[[i]]
  plots.list[[i]] <- ggplot(plot.frame %>% filter(sample_size == ss), aes(x=root.n.kl.delta, colour=score)) +
    geom_density() +
    xlim(-1,1) +
    xlab(TeX("\\sqrt{n}(LS Weights Oracle - LS Weights Estimated)")) +
    ggtitle(paste0("n = ",ss))
}

kernel.plots.dir <- "kernel_plots"
if (!dir.exists(kernel.plots.dir)) {
  dir.create(kernel.plots.dir)
}
for (i in 1:length(plots.list)) {
  ggsave(
    paste0("kernel_", i, ".png"),
    plot = plots.list[[i]],
    device = "png",
    path = kernel.plots.dir,
    width = 25,
    height = 15,
    units = "cm"
  )
}
