\documentclass{article}
\usepackage[margin=2cm]{geometry}
\usepackage{mathtools, mathrsfs, amsmath, amsfonts, amssymb, amsthm, tensor, enumitem, pdflscape, float, booktabs, multicol}
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,
    filecolor=black,      
    citecolor = black,
    urlcolor=blue
}

\usepackage{natbib}
\bibliographystyle{plainnat}
\setcitestyle{authoryear, open={(},close={)}}

\begin{document}

\title{A Plan for Investigating the Forecast Combination Puzzle on a Linear Pool of COVID-19 Forecasts}

\author{Ryan Zischke}

\maketitle

\section{Aim}

\begin{itemize}
\item Demonstrate our forecast combination puzzle methodology on a linear pool of distributional COVID-19 forecasts.
\item In a diverse linear pool of distributional forecasts of cumulative COVID-19 deaths across many regions, which performance better: the MLE weights, or equal weights?
\end{itemize}

\section{Data}

\begin{itemize}
\item A global COVID-19 dataset is aggregated on an ongoing basis by The Center for Systems Science and Engineering (CSSE) at Johns Hopkins University \citep{Dong2020}.
\item This dataset is available via the \href{https://github.com/RamiKrispin/coronavirus}{\underline{coronavirus}} R package, which is updated daily.
\item I don't believe this dataset contains demographic information, we will have to search elsewhere if we have such a need.
\end{itemize}

\section{Models}

I propose a linear pool of five models:
\begin{enumerate}
\item The exponential smoothing model of \citet{Hyndman2002}, used by \citet{Petropoulos2020} to forecast cumulative confirmed COVID-19 cases, globally.
\item The ARIMA model, with parameters selected as per \citet{Hyndman2007}, used by \citet{Chakraborty2020} for forecasting the cumulative number of confirmed COVID-19 cases in five countries.
\item The wavelet-based forecasting method of \citet{Aminghafari2007}, also used by \citet{Chakraborty2020}.
\item The compartmental model used in \citet{Carletti2020} to estimate key epidemiology parameters and forecast cumulative COVID-19 deaths on a per country basis.
\begin{itemize}
\item Compartmental models arise from epidemiological theory.
\item In these models, the population moves through (usually mutually exclusive) states such as: susceptible, infected, recovered, dead, etc.
\item Some compartmental models have more compartments (for example, to capture the difference between symptomatic and asymptomatic cases \ldots) and some have less (\ldots or not!).
\item See either \citet{Carletti2020} or \citet{Baek2020} for an introduction.
\item \citet{Carletti2020} uses the SIRD model (Susceptible, infected, recovered or dead).
\item The key empirical issue in these models is how to estimate parameters when the observed number of cases is corrupted by imperfect testing.
\item \citet{Carletti2020} discards the reported number of cases as unreliable, working exclusively with the reported number of COVID-19 caused deaths.
\end{itemize}
\item \citet{Baek2020} uses a dynamic compartmental model where the rate of virus transmission varies as a function of demographic and mobility factors, which are related to transmission rate via a mixed-effects model. There is also a parameter for the fraction of infections that are observed, also estimated as a mixed-effects model containing demographic and mobility factors. \citet{Baek2020} apply this model to forecasting COVID-19 cases among US states.
\end{enumerate}

These models are diverse, and represent a variety of assumptions.
\begin{itemize}
\item Three models are generic (ARIMA, exponential smoothing, and wavelet), while the other two \citep{Carletti2020, Baek2020} attempt to account for the behaviour of pandemics specifically.
\item Three of the models were applied to COVID-19 earlier in the year, and were subsequently published in peer reviewed journals \citep{Chakraborty2020, Petropoulos2020}, while the other two are hot-off-the-press pre-prints uploaded in the last couple of months \citep{Carletti2020, Baek2020}. It is worth emphasising that compartmental models of COVID-19 do feature in peer reviewed journals: see \citet{Anastassopoulou2020} for an early example of applying the SIRD model to global COVID-19 data.
\item Of the two compartmental models, one has time-varying parameters \citep{Baek2020}, and the other does not \citep{Carletti2020}.
\item Of the two compartmental models, one attempts to estimate the level of under-reporting \citep{Baek2020}, while the authors of the other argue that non-stationarity in the evolution of testing activity (and hence reporting rate) over time renders the reported number of cases unusable, and elect to estimate parameters using only the number of deaths \citep{Carletti2020}.
\item One model incorporates demographic information \citep{Baek2020}, while the others do not (and are therefore ``simpler'', with less parameters to estimate).
\end{itemize}

This pool of models has other desirable features.
\begin{itemize}
\item All models produce continuous predictive distributions, so we can estimate forecast combination weights with MLE, and use our current methodology as-is.
\item As far as I can tell, no two models in the pool are nested.
\end{itemize}

\section{Implementation}

\begin{itemize}
\item We can implement the five models in the framework of the \href{https://fable.tidyverts.org/}{\underline{fable}} R package.
\item In particular, each model would be implemented using the interface defined \href{https://fabletools.tidyverts.org/articles/extension_models.html}{\underline{here}}.
\item I've already implemented a ``linear\_pool'' model for fable that takes as parameters an arbitrary number of other fable models and their weights to produce the corresponding linear pool, see ``linear\_pool.R''.
\item Existing functionality within the fable package, such as the production of fan charts (see e.g. Figure 1 of \citealp{Aastveit2018}), can then be leveraged once these models are implemented in the fable framework.
\item See ``covid\_explore.R'' for code that produces (via fable) a fan chart of cumulative global COVID-19 cases produced using a linear pool of ARIMA and exponential smoothing forecasts (two of the five models proposed above), which are already provided by the fable package.
\end{itemize}

\bibliography{library.bib}

\end{document}
