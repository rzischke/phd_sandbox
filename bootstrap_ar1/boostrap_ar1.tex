\documentclass[12pt]{article}
\usepackage{mathtools,mathrsfs,amsfonts,amssymb,amsthm}
\usepackage[margin=2cm]{geometry}
\usepackage{parskip}

\usepackage[english]{babel}
\newtheorem{theorem}{Theorem}

\usepackage{natbib}
\bibliographystyle{plainnat}

\begin{document}

\title{Bootstrapping the Least Squares Estimator of an AR-1 Coefficient}
\author{Ryan Zischke}
\maketitle

\section{Introduction}

Let $(Y_t)_{t \in \mathbb{Z}}$ be an AR-1 process with the following properties:
\begin{subequations}
\begin{align}
\alpha &\in (-1, 1), \\
\sigma^2 &\in (0, \infty), \\
\epsilon_t &\overset{i.i.d.}{\sim} N(0,\sigma^2)\ \forall\ t \in \mathbb{Z}, \\
Y_t &= \alpha Y_{t-1} + \epsilon_t.
\end{align}
\end{subequations}
Our aim is to prove the validity of the bootstrap procedure of the least squares estimator of $\alpha$, which we will denote by $\hat{\alpha}$. Let the residules be $(\hat{\epsilon}_t), t = 1, \cdots, n$. Then the variance of the noise, $\sigma^2$, will be estimated as the sample variance of the residuals, $\hat{\sigma}^2$. Given a sample of observations for $(Y_t), t = 0, \cdots, n$, we have
\begin{subequations}
\begin{align}
\hat{\alpha} &= \frac{\sum_{t = 1}^{n} Y_t Y_{t-1}}{\sum_{t = 1}^{n} Y_{t-1}^2}, \label{eqn:alphahat} \\
\hat{\epsilon}_t &= Y_t - \hat{\alpha}Y_{t-1}\ \forall\ t = 1, \cdots, n, \\
\hat{\sigma}^2 &= \frac{1}{n} \sum_{t = 1}^n \hat{\epsilon}_t^2.
\end{align}
\end{subequations}

The bootstrap procedure is as follows. We will replicate a realisation of the AR-1 process using our least-squares model, and our bootstrapped $\hat{\alpha}^*$ will be the least-squares estimate of the autoregressive coefficient given the replicate observations. Let $(Y^*_t), t = 0, \cdots, n$ denote the replicate AR-1 process, which is defined by the following:
\begin{subequations}
\begin{align}
\epsilon^*_t &\overset{i.i.d.}{\sim} N(0, \hat{\sigma}^2)\ \forall\ t = 1, \cdots, n, \\
Y^*_0 &= Y_0, \\
Y^*_t &= \hat{\alpha}Y^*_{t-1} + \epsilon^*_t\ \forall\ t = 1, \cdots, n.
\end{align}
\end{subequations}
We obtain $\hat{\alpha}^*$ from replicate observations $(Y^*_t)$ in the same way we obtain $\hat{\alpha}$ from observations $(Y_t)$:
\begin{equation}
\hat{\alpha}^* = \frac{\sum_{t = 1}^n Y^*_t Y^*_{t-1}}{\sum_{t = 1}^n Y^{*2}_{t-1}}.
\end{equation}

To distinguish the replicate process from the underlying replicated processes, denote by $P^*$ the probability measure of the former and denote by $P$ the probability measure of the latter. In the same way, $\mathbb{E}^*$, $\mathrm{Var}^*$, $\mathrm{Cov}^*$, $\cdots$ represent expectations with respect to the replicate measure $P^*$, whereas $\mathbb{E}$, $\mathrm{Var}$, $\mathrm{Cov}$, $\cdots$ represent expectations with respect to $P$. Expectations over $P^*$ are implicitly conditional on non-starred variables such as $\hat{\alpha}$ and $\hat{\sigma}$.

Our aim is to show that the histogram formed by repeated draws of $\hat{\alpha}^*$ approaches the distribution of $\hat{\alpha}$ as $n \to \infty$ once appropriate normalisation is applied. Formally, given
\begin{subequations}
\begin{align}
\hat{\nu}^2 &= \frac{1}{n} \sum_{t = 1}^n Y_{t-1}^2, \\
\hat{\nu}^{*2} &= \frac{1}{n} \sum_{t = 1}^n Y_{t-1}^{*2}, \\
\end{align}
\end{subequations}
we will show that
\begin{equation}
\sup_x \lvert P^*(\sqrt{n} \hat{\nu}^{*2} (\hat{\alpha}^* - \hat{\alpha}) \leq x) - P(\sqrt{n} \hat{\nu}^2 (\hat{\alpha} - \alpha) \leq x) \rvert = o_p(n^{-\frac{1}{2}}). \label{eqn:result}
\end{equation}

Proof of this will proceed in three stages:
\begin{enumerate}
\item prove the existance of an Edgeworth expansion for $P(\sqrt{n} \nu (\hat{\alpha} - \alpha) \leq x)$,
\item prove the existance of an Edgeworth expansion for $P^*(\sqrt{n} \nu^*(\hat{\alpha}^* - \hat{\alpha}) \leq x)$, and
\item establish (\ref{eqn:result}) by substituting in the above Edgeworth expansions.
\end{enumerate}

\section{Edgeworth Expansion of the Coefficient Estimate}

\cite{Goetze1983} identify sufficient conditions for the existance of the Edgeworth expansion of a sum of weekly dependent random vectors. Let $(X_t)_{t \in \mathbb{Z}}$ be an $\mathbb{R}$-valued stochastic process on the probability triple $(\Omega, \mathscr{F}, P)$. Denote by $\mathscr{D}_j$ the sigma field of $X_j$ or $\epsilon_j$, at our option, so that $\mathscr{F} = \cup_{j \in \mathbb{Z}} \mathscr{D}_j$, and write $\cup_{j=a}^b \mathscr{D}_j = \mathscr{D}_a^b$. Assume there exists $\phi, \delta, c, d \in \mathbb{R}_{>0}$ such that, for all $t \in \mathbb{Z}$ and $m,p \in \mathbb{N}$, 
\begin{subequations}
\begin{gather}
\mathbb{E}[X_t] = 0, \label{eqn:abegin} \\
\mathbb{E}[\lvert X_t \rvert^4] \leq \beta < \infty, \label{eqn:a2}\\
\exists\ \mathscr{D}_{t - m}^{t + m}\ \mathrm{measurable\ r.v.}\ Y_{t,m},\ \mathbb{E}[\lvert X_t - Y_{t,m} \rvert] \leq c \exp (- \phi m), \label{eqn:a3} \\
\forall\ A \in \mathscr{D}_{-\infty}^t,\ B \in \mathscr{D}_{t + m}^{\infty}, \lvert P(A \cap B) - P(A)P(B) \rvert \leq c \exp (- \phi m), \label{eqn:a4} \\
\forall\ \lVert \tau \rVert \geq d,\ \mathbb{E} \left \lbrace \left\lvert \mathbb{E} \left[ \exp \left( i \tau \sum_{j = t - m}^{t + m} X_j \right) \Bigg\vert \mathscr{D}_j, j \neq t \right ] \right \rvert \right \rbrace < 1 - \delta, \label{eqn:a5} \\
\forall\ A \in \mathscr{D}_{t - p}^{t + p}, \mathbb{E} \left[ P(A \mid \mathscr D_j, j \neq t) - P(A \mid \mathscr D_j, 0 < \vert j - t \vert \leq m + p) \right] \leq c \exp (- \phi m), \label{eqn:a6} \\
\lim_{n \to \infty} \mathrm{Var} \left( \frac{1}{\sqrt{n}} \sum_{\tau = 1}^n X_{\tau} \right) = 1. \label{eqn:aend}
\end{gather}
\end{subequations}
The statistic of interest is
\begin{equation}
S_n = \frac{1}{\sqrt{n}} \sum_{\tau = 1}^n X_\tau.
\end{equation}
Let $\Psi_n$ be the first two terms of an Edgeworth expansion around the CDF of a standard normally distributed random variable. We may extract the following expression from Chapter 2.4 in \cite{Hall1997}:
\begin{equation}
\Psi_n(x ; \delta^2 , \gamma) = \Phi(x) - \frac{1}{6} \gamma \delta^{-3} (x^2 - 1) n^{-\frac{1}{2}} \phi(x),
\end{equation}
where $\Phi$ is the standard normal CDF, and $\phi$ is the standard normal PDF. \cite{Goetze1983} provide the following theorem.
\begin{theorem}
\label{th:edgeworth}
Under assumptions (\ref{eqn:abegin}) - (\ref{eqn:aend}), we have
\begin{equation}
P(S_n \leq x) = \Psi \left( x ; \delta^2 = \mathbb{E}[X_0^2], \gamma = \mathbb{E}[X_0^3] \right) + o(n^{-\frac{1}{2}}),
\end{equation}
where the remainder term does not depend on $x$.
\end{theorem}

It remains to find a candidate for $(X)$ that is suitable for the problem at hand. We will begin with the definition of $\hat{\alpha}$ in (\ref{eqn:alphahat}), and then rearrange for the statistic in the second probability expression in (\ref{eqn:result}):
\begin{subequations}
\begin{align}
\hat{\alpha} &= \frac{\sum_{t = 1}^{n} Y_t Y_{t-1}}{\sum_{t = 1}^{n} Y_{t-1}^2}, \\
\therefore \left( \sum_{t = 1}^{n} Y_{t-1}^2 \right) \hat{\alpha} &= \sum_{t=1}^{n} Y_t Y_{t-1}, \\
\therefore \left( \sum_{t = 1}^{n} Y_{t-1}^2 \right) (\hat{\alpha} - \alpha) &= \left( \sum_{t=1}^n Y_t Y_{t-1} \right) - \alpha \sum_{t=1}^n Y_{t-1}^2, \\
\therefore n \hat{\nu}^2 (\hat{\alpha} - \alpha) &= \sum_{t = 1}^n Y_t Y_{t-1} - \alpha Y_{t-1}^2 \\
&= \sum_{t=1}^n Y_{t-1} (Y_t - \alpha Y_{t-1}) \\
&= \sum_{t=1}^n Y_{t-1} \epsilon_t, \\
\therefore \sqrt{n} \hat{\nu}^2 (\hat{\alpha} - \alpha) &= \frac{1}{\sqrt{n}} \sum_{t=1}^n Y_{t-1} \epsilon_t.
\end{align}
So that our statistic has unit variance asymptotically, we will apply an appropriate normalisation. Let
\begin{equation}
s^2 = \frac{\sigma^4}{1 - \alpha^2},
\end{equation}
then we have
\begin{equation}
\frac{\sqrt{n} \hat{\nu}^2 (\hat{\alpha} - \alpha)}{s} = \frac{1}{\sqrt{n}} \sum_{t=1}^n \frac{Y_{t-1}\epsilon_t}{s}. \label{eqn:stat}
\end{equation}
\end{subequations}
Note that this normalisation is purely theoretical in nature, allowing for the existence of a standard Edgeworth expansion around the standard normal CDF. In practice, we could instead examine, via bootstrap, the statistic $\sqrt{n}\hat{\nu}^2(\hat{\alpha} - \alpha)$, with no change in convergence properties. Equation \ref{eqn:stat} implies the following definitions for $S_n$ and $(X)$:
\begin{subequations}
\begin{align}
S_n &= \frac{\sqrt{n} \hat{\nu}^2 (\hat{\alpha} - \alpha)}{s}, \\
X_t &= \frac{Y_{t - 1} \epsilon_t}{s}. \label{eqn:xtfyt}
\end{align}
\end{subequations}
A Wold representation of $(Y)$ is available under our assumptions, giving the following Wold-like representation for $(X)$:
\begin{equation}
X_t = \sum_{r = 0}^{\infty} \alpha^r \frac{\epsilon_{t-r-1} \epsilon_t}{s}. \label{eqn:xwold}
\end{equation}

\subsection{Verifying \ref{eqn:abegin}}

We will use the Wold representation of $(X)$ together with the independence of different noise terms to verify (\ref{eqn:abegin}):
\begin{subequations}
\begin{align}
\mathbb{E}[X_t] &= \mathbb{E} \left[ \sum_{r=0}^\infty \alpha^r \frac{\epsilon_{t-r-1} \epsilon_t}{s} \right] \\
&= \sum_{r=0}^\infty \alpha^r \frac{\mathbb{E}[\epsilon_{t-r-1}] \mathbb{E}[\epsilon_t]}{s} \\
&= 0.
\end{align}
\end{subequations}

\subsection{Verifying \ref{eqn:a2}}

Here we expand the fourth power of the Wold representation of $(X_t)$ and take the expectation:
\begin{subequations}
\begin{align}
\mathbb{E}[X_t^4] &= \mathbb{E} \left[ \left( \sum_{r=0}^\infty \alpha^r \frac{\epsilon_{t-r-1} \epsilon_t}{s} \right)^4 \right] \\
&= \mathbb{E} \left[ \frac{1}{s^4} \sum_{r_1 = 0}^\infty \sum_{r_2 = 0}^\infty \sum_{r_3=0}^\infty \sum_{r_4 = 0}^\infty \alpha^{r_1 + r_2 + r_3 + r_4} \prod_{k=1}^4 \epsilon_t \epsilon_{t - r_k - 1} \right] \\
&=  \frac{1}{s^4} \sum_{r_1 = 0}^\infty \sum_{r_2 = 0}^\infty \sum_{r_3=0}^\infty \sum_{r_4 = 0}^\infty \alpha^{r_1 + r_2 + r_3 + r_4} \mathbb{E}[\epsilon_t^4] \mathbb{E} \left[ \prod_{k=1}^4 \epsilon_{t - r_k - 1} \right]
\end{align}
We have
\begin{align}
\max_{r \in \mathbb{N}^4 \cup \{0\}} \mathbb{E} \left[ \prod_{k=1}^4 \epsilon_{t - r_k - 1} \right] &=\max \left \lbrace \mathbb{E}[\epsilon_t]^4, \mathbb{E}[\epsilon_t] \mathbb{E}[\epsilon_t^3], \mathbb{E}[\epsilon_t^2]^2, \mathbb{E}[\epsilon_t^4] \right \rbrace \\
&= \max \{ 0^4, 0 \times 0^3, \sigma^4, 3 \sigma^4 \} \\
&= 3 \sigma^4,
\end{align}
which can be used to bound the expectation:
\begin{align}
\mathbb{E}[X_t^4] &\leq  \frac{1}{s^4}\sum_{r_1 = 0}^\infty \sum_{r_2 = 0}^\infty \sum_{r_3=0}^\infty \sum_{r_4 = 0}^\infty 9 \alpha^{r_1 + r_2 + r_3 + r_4} \sigma^8 \\
&= 9 \frac{\sigma^8}{s^4} \left( \sum_{r=0}^\infty \alpha^r \right)^4 \\
&= \frac{9 \sigma^8}{s^4 (1 - \alpha)^4} \\
&= \frac{(1 - \alpha^2)^2}{\sigma^8} \times \frac{9 \sigma^8}{(1 - \alpha)^4} \\
&= (1 - \alpha)^2 (1 + \alpha)^2 \times \frac{9}{(1 - \alpha)^4} \\
&= 9 \left( \frac{1 + \alpha}{1 - \alpha} \right)^2 \\
&< \infty.
\end{align}
\end{subequations}

\newpage

\subsection{Verifying \ref{eqn:a3}}

Choose
\begin{equation}
Y_{t,m} = \sum_{r=0}^{m-1} \alpha^r \frac{\epsilon_{t-r-1} \epsilon_t}{s}.
\end{equation}
For $\alpha = 0$, only the ``$r=0$'' term is non-zero, and $X_t = Y_{t,m}$. For $\alpha \neq 0$, we have
\begin{subequations}
\begin{align}
\mathbb{E}[\lvert X_t - Y_{t,m} \rvert] &= \mathbb{E} \left[ \left \lvert \sum_{r=0}^\infty \alpha^r \frac{\epsilon_{t-r-1} \epsilon_t}{s} - \sum_{r=0}^{m-1} \alpha^r \frac{\epsilon_{t-r-1} \epsilon_t}{s} \right \rvert \right] \\
&= \mathbb{E} \left[ \left \lvert \sum_{r=m}^\infty \alpha^r \frac{\epsilon_{t-r-1} \epsilon_t}{s} \right \rvert \right] \\
&\leq \mathbb{E} \left[ \sum_{r=m}^\infty \lvert \alpha \rvert^r \frac{\lvert \epsilon_{t-r-1} \rvert \lvert \epsilon_t \rvert}{s} \right] \\
&= \sum_{r=m}^\infty \lvert \alpha \rvert^r \frac{\mathbb{E}[\lvert \epsilon_t \rvert]^2}{s} \\
&= \frac{\mathbb{E}[\lvert \epsilon_t \rvert]^2}{s} \sum_{r=m}^\infty \lvert \alpha \rvert^r \\
&\leq \frac{\mathbb{E}[\epsilon_t^2]}{s} \sum_{r=m}^\infty \lvert \alpha \rvert^r\quad\mathrm{(Jensen's\ inequality)} \\
&= \frac{\sigma^2}{s} \sum_{r=m}^\infty \lvert \alpha \rvert^r \\
&= \frac{\sigma^2}{s} \left( \sum_{r=0}^\infty \lvert \alpha \rvert^r - \sum_{r=0}^{m-1} \lvert \alpha \rvert^r \right) \\
&= \frac{\sigma^2}{s} \left( \frac{1}{1 - \lvert \alpha \rvert} - \frac{1 - \lvert \alpha \rvert^m}{1 - \lvert \alpha \rvert} \right) \\
&= \frac{\sigma^2}{s} \frac{\lvert \alpha \rvert^m}{1 - \lvert \alpha \rvert} \\
&= \frac{\sigma^2}{s(1 - \lvert \alpha \rvert)} \exp \{ \ln (\lvert \alpha \rvert) m\}.
\end{align}
\end{subequations}
We have thereby verified (\ref{eqn:a3}) by proving the existance of $\phi,c > 0$. In particular, (\ref{eqn:a3}) is satisfied for
\begin{subequations}
\begin{align}
\phi &= - \ln(\lvert \alpha \rvert), \\
c &= \frac{\sigma^2}{s(1 - \lvert \alpha \rvert)}.
\end{align}
\end{subequations}

\subsection{Verifying \ref{eqn:a4}}

This property is commonly referred to as the \textit{strong mixing} condition. In particular, we require that the strong mixing coefficient of $(X_t)$,
\begin{equation}
\varrho_m = \sup_{A \in \mathscr{D}_{-\infty}^{t}, B \in \mathscr{D}_{t+m}^{\infty}} \lvert P(A \cap B) - P(A) P(B) \rvert,
\end{equation}
decays at a exponential rate. We will use the following theorem from Chapter 14.4 of \cite{Davidson1994} to establish the rate of convergence of the strong mixing coefficient.
\begin{subequations}
\begin{theorem}
\label{th:sufmix}
Let $W_t = \sum_{j = 0}^{\infty} \theta_j Z_{t-j}$ define a random sequence $(W_t)_{t \in \mathbb{Z}}$ such that for some $r \in (0,2]$,
\begin{enumerate}
\item $Z_t$ is uniformly $L_r$-bounded, independent, continuous with p.d.f. $f_{Z_t}$, and there exists $\delta > 0, M < \infty$ such that $\lvert a \rvert \leq \delta$ implies
\begin{equation}
\sup_t \int_{-\infty}^{\infty} \left \lvert f_{Z_t}(z + a) - f_{Z_t}(z) \right \rvert dz \leq M \lvert a \rvert; \label{eqn:th2a}
\end{equation}
\item $\sum_{t=0}^{\infty} G_t(r)^{1/(1+r)} < \infty$, where
\begin{equation}
G_t(r) = 2 \sum_{j=t}^\infty \lvert \theta_j \rvert^r; \label{eqn:th2b}
\end{equation}
\item for all non-zero complex numbers $x$ with $\lvert x \rvert \leq 1$,
\begin{equation}
\theta(x) = \sum_{j=1}^{\infty} \theta_j x^j \neq 0. \label{eqn:th2c}
\end{equation}
\end{enumerate}
Then $(W_t)$ is strong mixing with $\varrho_m = \mathcal{O}(\sum_{t = m+1}^\infty G_t(r)^{1/(1+r)})$.
\end{theorem}
\end{subequations}
Initially, it would seem that we have $W_t = X_t$, $\theta_j = \alpha^j$ and $Z_t = \epsilon_{t-1} \epsilon_t$. This candidate is unsuitable however, since $(Z_t)$ would not be independent sequence. Instead, we will find the order of the strong mixing coefficient of a related process, and adapt the proof of theorem 14.1 from chapter 14.1 of \cite{Davidson1994} to imply the same of $(X_t)$.
\begin{theorem}
\label{th:transofmmix}
Let $(U_t)_{t \in \mathbb{Z}}$ and $(V_t)_{t \in \mathbb{Z}}$ be two stochastic processes related by a measurable function $g$, so that $V_t = g(U_t, U_{t-1}, \cdots, U_{t - \tau})$ for finite $\tau$. If there exists $\varphi > 0$ such that $(U_t)$ is strong mixing with coefficient $\varrho^u_m = \mathcal{O}(e^{-\varphi m})$, then $(V_t)$ is strong mixing with coefficient $\varrho^v_m = \mathcal{O}(e^{-\varphi m})$ as well.
\end{theorem}
\begin{proof}
Replace ``$\mathcal{O}(m^{- \varphi})$'' with ``$\mathcal{O}(e^{- \varphi m})$'' in the proof for theorem 14.1 from chapter 14.1 of \cite{Davidson1994}.
\end{proof}
Our related process will be $(Y_t)$ itself, and we will apply theorem \ref{th:sufmix} with $W_t = Y_t$, $\theta_j = \alpha^j$, $Z_t = \epsilon_t$ and $r = 1$.

Here we will address the first condition of theorem \ref{th:sufmix}. The innovations $Z_t = \epsilon_t$ are i.i.d.~normal. Thus the mean is defined and identical over $t$, so $Z_t$ is uniformly $L_1$-bounded. The innovations are independent and continuous by definition. Equation \ref{eqn:th2a} can be established with a Lipschitz estimate. Denote by $f$ the $N(0, \sigma^2)$ PDF. Then for all $a > 0$, we have
\begin{subequations}
\begin{align}
\sup_t \int_{-\infty}^{\infty} \left \lvert f_{Z_t}(z + a) - f_{Z_t}(z) \right \rvert dz &= \int_{-\infty}^{\infty} \left \lvert f(z + a) - f(z) \right \rvert dz \\
&\leq a \int_{-\infty}^{\infty} \sup_{x \in (z, z+a)} \lvert f'(x) \rvert dz. \label{eqn:asup}
\end{align}
The $N(0,\sigma^2)$ PDF has inflection points only at $\pm \sigma$, with positive increasing derivative before $- \sigma$ and negative increasing derivative after $\sigma$. It is also symmetric, so that $f'(-\sigma) = -f'(\sigma) > 0$. This gives
\begin{equation}
\sup_{x \in (z, z+a)} \lvert f'(x) \rvert \leq \begin{cases}
f'(z + a) & z \in (- \infty, -\sigma - a) \\
f'(-\sigma) & z \in [- \sigma - a, \sigma] \\
- f'(z) & z \in (\sigma, \infty)
\end{cases}.
\end{equation}
Our final bound follows after substituting this result into \ref{eqn:asup}:
\begin{align}
\int_{-\infty}^{\infty} \sup_{x \in (z, z+a)} \lvert f'(x) \rvert dz &\leq \int_{-\infty}^{-\sigma-a} f'(z + a) dz + \int_{-\sigma-a}^{\sigma} f'(-\sigma) dz - \int_{\sigma}^{\infty} f'(z) dz \\
&= 2 f(\sigma) + (2 \sigma + a) f'(-\sigma), \\
\therefore\ \sup_t \int_{-\infty}^{\infty} \left \lvert f_{Z_t}(z + a) - f_{Z_t}(z) \right \rvert dz &\leq \left[ 2 f(\sigma) + (2 \sigma + a) f'(-\sigma) \right] a.
\end{align}
We can apply the same argument for $a < 0$, and then conclude that, for all $a \in \mathbb{R}$,
\begin{equation}
\sup_t \int_{-\infty}^{\infty} \left \lvert f_{Z_t}(z + a) - f_{Z_t}(z) \right \rvert dz \leq \left[ 2 f(\sigma) + (2 \sigma + \lvert a \rvert) f'(-\sigma) \right] \lvert a \rvert.
\end{equation}
Equation \ref{eqn:th2a} is therefore satisfied by any $\delta > 0$ and
\begin{equation}
M = 2 f(\sigma) + (2 \sigma + \delta) f'(-\sigma).
\end{equation}
\end{subequations}

\begin{subequations}
In the second condition of theorem \ref{th:sufmix}, we have
\begin{equation}
G_t(1) = 2 \sum_{j=t}^{\infty} \lvert \alpha \rvert ^j.
\end{equation}
By virtue of $\lvert \alpha \rvert < 1$, $G_t(1)$ can be re-expressed as the difference of two convergent geometric series':
\begin{align}
G_t(1) &= 2 \left( \sum_{j = 0}^{\infty} \lvert \alpha \rvert^j - \sum_{j=0}^{t} \lvert \alpha \rvert^j \right) \\
&= 2 \left( \frac{1}{1 - \lvert \alpha \rvert} - \frac{1 - \lvert \alpha \rvert^{t}}{1 - \lvert \alpha \rvert} \right) \\
&= \frac{2 \lvert \alpha \rvert^{t}}{1 - \lvert \alpha \rvert}.
\end{align}
With $\sqrt{\lvert \alpha \rvert} < 1$, $\sum_{t=0}^{\infty} G_t(r)^{1/(1+r)}$ is a converging geometric series when $r = 1$:
\begin{align}
\sum_{t=0}^{\infty} G_t(1)^{\frac{1}{2}} &= \sum_{t = 0}^{\infty}\sqrt{\frac{2 \lvert \alpha \rvert^{t}}{1 - \lvert \alpha \rvert}} \\
&= \frac{\sqrt{2}}{(1 - \sqrt{ \lvert \alpha \rvert })\sqrt{1 - \lvert \alpha \rvert}} \\
&< \infty.
\end{align}
\end{subequations}
The second condition of theorem 2 is thereby satisfied.

\begin{subequations}
The third condition of theorem \ref{th:sufmix} converges as $\theta(.)$ is a geometric series. For all non-zero complex numbers $x$ with $\lvert x \rvert \leq 1$, we have $\lvert \alpha x \rvert \leq 1$, and
\begin{align}
\theta(x) &= \sum_{j=1}^{\infty} \alpha^j x^j \\
&= \sum_{j=1}^{\infty} (\alpha x)^j \\
&= \frac{\alpha x}{1 - \alpha x} \\
&\neq 0.
\end{align}
\end{subequations}

\begin{subequations}
Having satisfied all three conditions of theorem \ref{th:sufmix}, we conclude that $(Y_t)$ is strong mixing with strong mixing coefficient
\begin{align}
\varrho_m &= \mathcal{O} \left( \sum_{t = m+1}^{\infty} G_t(1)^{\frac{1}{2}} \right) \\
&= \mathcal{O} \left( \sum_{t = m+1}^{\infty} \sqrt{ \frac{2 \lvert \alpha \rvert^t}{1 - \lvert \alpha\rvert}} \right) \\
&= \mathcal{O} \left( \frac{\sqrt{2 \lvert \alpha \rvert}}{(1 - \lvert \alpha \rvert) \sqrt{1 - \lvert \alpha \rvert}} \sqrt{\alpha}^m \right) \\
&= \mathcal{O} \left( e^{\frac{1}{2} \ln (\lvert \alpha \rvert) m } \right).
\end{align}
Now apply theorem \ref{th:transofmmix} with the following function g:
\begin{equation}
X_t = g(Y_t, Y_{t-1}) = \frac{Y_{t-1}(Y_t - \alpha Y_{t-1})}{s}.
\end{equation}
We conclude that $(X_t)$ has a strong mixing coefficient of the same order as $(Y_t)$. Condition (\ref{eqn:a4}) is therefore satisfied for $\phi = \ln(\lvert \alpha \rvert)/2$, with the existence of $c > 0$ implied by the definition of ``$\mathcal{O}$''.
\end{subequations}

\subsection{Verifying \ref{eqn:a5}}

This property eliminates the possibility that the partial sum $\sum X_\tau$ has a (conditional) probability distribution that concentrates onto a single point, as would be the case if $\sigma^2 = 0$, for example. In this case, $S_n$ would not approach a normal distribution, and the Edgeworth expansion would therefore not be valid.

Suppose we could separate our partial sum into a linear function of $\epsilon_t$ as follows:
\begin{subequations} \label{eqn:betared}
\begin{equation}
\sum_{j = t - m}^{t + m} X_j = Z_{t,m} \epsilon_t + \beta_{t,m},
\end{equation}
where $Z_{t,m}$ and $\beta_{t,m}$ are functions of the innovations and independent of $\epsilon_t$, for all $t \in \mathbb{Z}$, $m \in \mathbb{N}$. With this independence and because $\lvert \exp(i \theta) \rvert = 1$ for all $\theta \in \mathbb{R}$, a process need only satisfy ($ \ref{eqn:a5}$) with $Z_{t,m} \epsilon_t$ in lieu of the partial sum. In particular, we have
\begin{align}
&\mathbb{E} \left \lbrace \left\lvert \mathbb{E} \left[ \exp \left( i \tau \sum_{j = t - m}^{t + m} X_j \right) \Bigg\vert \mathscr{D}_j, j \neq t \right ] \right \rvert \right \rbrace \\
=\ &\mathbb{E} \left \lbrace \left\lvert \mathbb{E} \left[ \exp \left( i \tau (Z_{t,m} \epsilon_t + \beta_{t,m}) \right) \Bigg\vert \mathscr{D}_j, j \neq t \right ] \right \rvert \right \rbrace \\
=\ &\mathbb{E} \left \lbrace \left\lvert \mathbb{E} \left[ \exp \left( i \tau \beta_{t,m} \right) \exp \left( i \tau Z_{t,m} \epsilon_t \right) \Bigg\vert \mathscr{D}_j, j \neq t \right ] \right \rvert \right \rbrace \\
=\ &\mathbb{E} \left \lbrace \left\lvert \exp \left (i \tau \beta_{t,m} \right) \right\rvert \left\lvert \mathbb{E} \left[ \exp \left( i \tau Z_{t,m} \epsilon_t \right) \Bigg\vert \mathscr{D}_j, j \neq t \right ] \right \rvert \right \rbrace \\
=\ &\mathbb{E} \left \lbrace  \left\lvert \mathbb{E} \left[ \exp \left( i \tau Z_{t,m} \epsilon_t \right) \Bigg\vert \mathscr{D}_j, j \neq t \right ] \right \rvert \right \rbrace.
\end{align}
\end{subequations}

\begin{subequations}
To find $Z_{t,m}$, we will first factor $\epsilon_t$ out of $X_j$. With reference to the Wold-like representation of $X_j$ in equation (\ref{eqn:xwold}), there are three different cases depending on the index $j$. The case $j \leq t - 1$ is trivial: the indices of the $\epsilon$ terms have $j - r - 1 \neq t$ and $j \neq t$ for all $r \geq 0$, and so $X_j$ is independent of $\epsilon_t$. If $j=t$, then $j - r - 1 \neq t$ for all $r \geq 0$, and we have
\begin{align}
X_j &= \sum_{r = 0}^{\infty} \alpha^r \frac{\epsilon_{j - r - 1} \epsilon_j}{s} \\
&= \sum_{r = 0}^{\infty} \alpha^r \frac{\epsilon_{t - r - 1} \epsilon_t}{s} \\
&= \epsilon_t \sum_{r = 0}^{\infty} \alpha^r \frac{\epsilon_{t - r - 1}}{s}.
\end{align}
In the third case, $j \geq t + 1$ and therefore $j - r - 1 = t$ for $r = j - t - 1 \geq 0$. To extract the term in the sum over $r$ that contains $\epsilon_t$ we will split the sum into three parts: $r < j - t - 1$, $r = j - t - 1$ and $r > j - t - 1$. That is,
\begin{align}
X_j &= \sum_{r = 0}^{\infty} \alpha^r \frac{\epsilon_{j - r - 1} \epsilon_j}{s} \\
&= \epsilon_t \frac{\alpha^{j-t-1}\epsilon_j}{s} + \sum_{r = 0}^{j-t-2} \alpha^r \frac{\epsilon_{j-r-1} \epsilon_{j}}{s} +  \sum_{r = j -  t}^{\infty} \alpha^r \frac{\epsilon_{j-r-1} \epsilon_{j}}{s}.
\end{align}
Consolidating these three cases gives
\begin{equation}
X_j = \begin{cases}
\sum_{r = 0}^{\infty} \alpha^r \frac{\epsilon_{j - r - 1} \epsilon_j}{s} & j \leq t - 1 \\
\epsilon_t \sum_{r = 0}^{\infty} \alpha^r \frac{\epsilon_{t - r - 1}}{s} & j = t \\
\epsilon_t \frac{\alpha^{j - t - 1} \epsilon_j}{s} +  \sum_{r = 0}^{j-t-2} \alpha^r \frac{\epsilon_{j-r-1} \epsilon_{j}}{s} +  \sum_{r = j -  t}^{\infty} \alpha^r \frac{\epsilon_{j-r-1} \epsilon_{j}}{s} & j \geq t + 1
\end{cases},
\end{equation}
where every random variable beginning with ``$\Sigma$'' is independent of $\epsilon_t$.
\end{subequations}

With this representation of $X_j$, finding $Z_{t,m}$ is a matter of separating the partial sum $\sum_{j=t-m}^{t+m}X_j$ into the three cases for $j$ specified above and collecting the $\epsilon_t$ terms:
\begin{subequations}
\begin{align}
\sum_{j=t-m}^{t+m} X_j &= \left(\sum_{j = t-m}^{t-1} X_j \right) + X_t + \sum_{j=t+1}^{t+m} X_j \\
&= \sum_{j=t-m}^{t+m} \sum_{r = 0}^{\infty} \alpha^r \frac{\epsilon_{j-r-1}\epsilon_j}{s} \\
&\quad + \epsilon_t \sum_{r=0}^{\infty} \alpha^r \frac{\epsilon_{t-r-1}}{s} \\
&\quad + \sum_{j = t + 1}^{t + m} \left( \epsilon_t \frac{\alpha^{j - t - 1} \epsilon_j}{s} +  \sum_{r = 0}^{j-t-2} \alpha^r \frac{\epsilon_{j-r-1} \epsilon_{j}}{s} +  \sum_{r = j -  t}^{\infty} \alpha^r \frac{\epsilon_{j-r-1} \epsilon_{j}}{s} \right) \\
&= \left( \sum_{r=0}^{\infty} \alpha^r \frac{\epsilon_{t-r-1}}{s} + \sum_{j=t+1}^{t+m} \frac{\alpha^{j-t-1} \epsilon_j}{s} \right) \epsilon_t \\
&\quad + \sum_{j=t-m}^{t+m} \sum_{r = 0}^{\infty} \alpha^r \frac{\epsilon_{j-r-1}\epsilon_j}{s} \\
&\quad + \sum_{j=t+1}^{t+m} \left( \sum_{r = 0}^{j-t-2} \alpha^r \frac{\epsilon_{j-r-1} \epsilon_{j}}{s} +  \sum_{r = j -  t}^{\infty} \alpha^r \frac{\epsilon_{j-r-1} \epsilon_{j}}{s} \right),
\end{align}
where, once again, any random variable that begins with a ``$\Sigma$'' is independent of $\epsilon_t$. We therefore have
\begin{equation}
Z_{t,m} = \sum_{r=0}^{\infty} \frac{\alpha^r \epsilon_{t-r-1}}{s} + \sum_{j=t+1}^{t+m} \frac{\alpha^{j-t-1} \epsilon_j}{s},
\end{equation}
\end{subequations}
and a corresponding expression for $\beta_{t,m}$, where both are independent of $\epsilon_t$. Referring to equations (\ref{eqn:betared}) and surrounding text, we need not concern ourselves with the characteristics of $\beta_{t,m}$.

Since $Z_{t,m}$ is a sum of normally distributed innovations, it is itself normally distributed. We have
\begin{subequations}
\begin{align}
\mathbb{E} [ Z_{t,m} ] &= \mathbb{E} \left[  \sum_{r=0}^{\infty} \frac{\alpha^r \epsilon_{t-r-1}}{s} + \sum_{j=t+1}^{t+m} \frac{\alpha^{j-t-1} \epsilon_j}{s} \right] \\
&= \sum_{r=0}^{\infty} \frac{\alpha^r \mathbb{E}[\epsilon_{t-r-1}]}{s} + \sum_{j=t+1}^{t+m} \frac{\alpha^{j-t-1} \mathbb{E}[\epsilon_j]}{s} \\
&= 0,
\end{align}
and, because $\epsilon_i$ is independent of $\epsilon_j$ for all $i \neq j$,
\begin{align}
\mathrm{Var}(Z_{t,m}) &= \mathrm{Var} \left( \sum_{r=0}^{\infty} \frac{\alpha^r \epsilon_{t-r-1}}{s} + \sum_{j=t+1}^{t+m} \frac{\alpha^{j-t-1} \epsilon_j}{s} \right) \\
&= \sum_{r=0}^{\infty}  \frac{\alpha^{2r} \mathrm{Var}(\epsilon_{t-r-1})}{s^2} + \sum_{j=t+1}^{t+m} \frac{\alpha^{2(j-t-1)} \mathrm{Var}(\epsilon_j)}{s^2} \\
&= \sum_{r=0}^{\infty}  \frac{\alpha^{2r} \sigma^2 }{s^2} + \sum_{j=t+1}^{t+m} \frac{\alpha^{2(j-t-1)} \sigma^2 }{s^2} \\
&= \frac{\sigma^2}{s^2} \left( \sum_{r=0}^{\infty} \alpha^{2r} + \sum_{r=0}^{m-1} \alpha^{2r} \right) \\
&= \frac{\sigma^2}{s^2} \left( \frac{1}{1 - \alpha^2} + \frac{1 - \alpha^{2m}}{1 - \alpha^2} \right) \\
&= \frac{\sigma^2 (2 - \alpha^{2m})}{s^2 (1 - \alpha^2)}.
\end{align}
\end{subequations}
The distribution of $Z_{t,m}$ is therefore
\begin{subequations}
\begin{equation}
Z_{t,m} \sim N \left( 0, \frac{\sigma^2(2 - \alpha^{2m})}{s^2 (1 - \alpha^2)} \right),
\end{equation}
and we draw attention to the following chi-squared distributed random variable:
\begin{equation}
\frac{s^2 (1 - \alpha^2)}{\sigma^2 (2 - \alpha^{2m})} Z_{t,m}^2 \sim \chi^2_1.
\end{equation}
\end{subequations}
Using the characteristic function of the normal distribution and the moment generating function of the chi-squared distribution, we may derive a closed form solution of the expectation in (\ref{eqn:a5}) as follows:
\begin{subequations}
\begin{align}
&\mathbb{E} \left \lbrace \left\lvert \mathbb{E} \left[ \exp \left( i \tau \sum_{j = t - m}^{t + m} X_j \right) \Bigg\vert \mathscr{D}_j, j \neq t \right ] \right \rvert \right \rbrace \\
=\ &\mathbb{E} \left \lbrace  \left\lvert \mathbb{E} \left[ \exp \left( i \tau Z_{t,m} \epsilon_t \right) \Bigg\vert \mathscr{D}_j, j \neq t \right ] \right \rvert \right \rbrace \\
=\ &\mathbb{E} \left \lbrace \exp \left( - \frac{\sigma^2 \tau^2}{2} Z_{t,m}^2 \right) \right \rbrace \\
=\ &\mathbb{E} \left \lbrace \exp \left( - \frac{\sigma^4 \tau^2 (2 - \alpha^{2m})}{2 s^2(1 - \alpha^2)} \times \frac{s^2(1 - \alpha^2)}{\sigma^2 (2 - \alpha^{2m})} Z_{t,m}^2 \right) \right \rbrace \\
=\ &\left( 1 + \frac{\sigma^4 \tau^2 (2 - \alpha^{2m})}{s^2 (1 - \alpha^2)} \right)^{- \frac{1}{2}} \\
<\ &\left( 1 + \frac{\sigma^4 \tau^2}{s^2 (1 - \alpha^2)} \right)^{- \frac{1}{2}} \\
<\ &1.
\end{align}
\end{subequations}
Condition ($\ref{eqn:a5}$) is therefore satisfied for any $d > 0$ and
\begin{equation}
\delta = 1 - \left( 1 + \frac{\sigma^2 d^2}{s^2 (1 - \alpha)^2} \right).
\end{equation}

\subsection{Verifying \ref{eqn:a6}}

\begin{subequations}
Observe that $X_j$ may be expressed in terms of only $Y_j$ and $Y_{j-1}$ by expanding $\epsilon_j$ in equation (\ref{eqn:xtfyt}):
\begin{equation}
X_j = \frac{Y_{j-1} \epsilon_j}{s} = \frac{Y_{j-1}(Y_j - \alpha Y_{j-1})}{s}.
\end{equation}
Let $\mathscr{D}_j$ be the sigma field of $Y_j$. Then for all $t \in \mathbb{Z}, m,p \in \mathbb{N}, A \in \mathscr{D}_{t-p}^{t+p}$,
\begin{equation}
P(A \mid \mathscr{D}_j, j \neq t) = P(A \mid \mathscr{D}_j, 0 < \lvert j - t \rvert \leq m + p),
\end{equation}
and condition (\ref{eqn:a6}) is satisfied for all $c,\phi \in \mathbb{R}_{>0}$.
\end{subequations}

\subsection{Verifying \ref{eqn:aend}}

\begin{subequations}
Beginning with
\begin{align}
\mathrm{Var} \left( \frac{1}{\sqrt{n}} \sum_{t = 1}^{n} Y_{t - 1} \epsilon_t \right) &= \mathbb{E} \left[ \left( \frac{1}{\sqrt{n}} \sum_{t = 1}^{n} Y_{t - 1} \epsilon_t \right)^2 \right] \\
&= \mathbb{E} \left[ \frac{1}{n} \sum_{t_1 = 1}^n \sum_{t_2 = 1}^n Y_{t_1 - 1} Y_{t_2 - 1} \epsilon_{t_1} \epsilon_{t_2} \right] \\
&= \frac{1}{n} \sum_{t = 1}^n \mathbb{E}[Y_{t - 1}^2] \mathbb{E}[\epsilon_{t}^2] \\
&= \frac{1}{n} \sum_{t = 1}^n \frac{\sigma^4}{1 - \alpha^2} \\
&= \frac{\sigma^4}{1 - \alpha^2} \\
&= s^2,
\end{align}
we have
\begin{align}
\mathrm{Var}(S_n) &= \mathrm{Var} \left( \frac{1}{\sqrt{n}} \sum_{t = 1}^n X_t \right) \\
&= \mathrm{Var} \left( \frac{1}{\sqrt{n}} \sum_{t = 1}^n \frac{Y_{t-1} \epsilon_{t}}{s} \right) \\
&= \frac{1}{s^2} \mathrm{Var} \left( \frac{1}{\sqrt{n}} \sum_{t = 1}^n Y_{t-1} \epsilon_t \right) \\
&= \frac{1}{s^2} s^2 \\
&= 1.
\end{align}
\end{subequations}

\section{Edgeworth Expansion of the Bootstrapped Coefficient Estimate}

\begin{subequations}
Since $\hat{\alpha} \in (-1 ,1)$ with probability approaching one, and since $\hat{\sigma}^2 \in (0, \infty)$, the proof in the previous section establishes the existence of the Edgeworth expansion (with probability approaching one) of $P^*(S^*_n \leq \cdot)$ where
\begin{equation}
S^*_n = \frac{\sqrt{n} \hat{\nu}^{*2} ( \hat{\alpha}^* - \hat{\alpha} )}{s^*},
\end{equation}
and
\begin{equation}
s^{*2} = \frac{\hat{\sigma}^4}{1 - \hat{\alpha}^2}.
\end{equation}
\end{subequations}

\section{Main Result}

\begin{subequations}
Given the existence of these two Edgeworth expansions, we have
\begin{align}
&\sup_x \lvert P^*(\sqrt{n} \hat{\nu}^{*2} (\hat{\alpha}^* - \hat{\alpha}) \leq x) - P(\sqrt{n} \hat{\nu}^2 (\hat{\alpha} - \alpha) \leq x) \rvert \\
=\ &\sup_x \lvert \Psi ( x ; \delta^{*2} = \mathbb{E}^*[X^*_0], \gamma^* = \mathbb{E}^*[X^{*3}_0]) - \Psi( x ; \delta^2 = \mathbb{E}[X_0], \gamma = \mathbb{E}[X_0] ) + o(n^{-\frac{1}{2}}) \rvert, \\
\end{align}
where
\begin{equation}
X^*_t = \frac{Y^*_{t-1} \epsilon^*_t}{s}.
\end{equation}
With some algebraic manipulation, it is clear that the $1/\sqrt{n}$ coefficient of $\Psi ( x ; \delta^2 = \mathbb{E}^*[X^*_0], \gamma = \mathbb{E}^*[X^{*3}_0])$ is an analytic function of the first and second order sample moments of the (non-replicate) observations. Page 213 of \cite{Goetze1983}, and references therein, give a central limit theorem for the $\sqrt{n}$-normalised sums of strongly mixing random variables. Given that $(Y_t)$ is strong mixing, theorem \ref{th:transofmmix} holds that these sample moments are strong mixing as well. Since these sample moments are mixing they are also Ergodic (\cite{Davidson1994} theorem 13.18), and are stationary by the definition of $(Y_t)$. Thus, by the Ergodic theorem (\cite{Davidson1994} theorem 13.12) and the results above, these sample moments are $\sqrt{n}$-consistent asymptotically normal random variables under $P$. Applying the delta theorem, we then have
\begin{align}
\sqrt{n} (\gamma^* \delta^{*-3} - \gamma \delta^{-3}) &= O_p(1), \\
\therefore\ \gamma^* \delta^{*-3} - \gamma \delta^{-3} &= O_p(n^{-\frac{1}{2}}).
\end{align}
Making this substitution concludes the proof:
\begin{align}
 &\sup_x \left\lvert \Psi ( x ; \delta^{*2} = \mathbb{E}^*[X^*_0], \gamma^* = \mathbb{E}^*[X^{*3}_0]) - \Psi( x ; \delta^2 = \mathbb{E}[X_0], \gamma = \mathbb{E}[X_0] ) + o(n^{-\frac{1}{2}}) \right\rvert \\
=\ &\sup_x \left\lvert \Phi(x) - \frac{1}{6} \gamma^* \delta^{*-3} (x^2 - 1) n^{-\frac{1}{2}}\phi(x) - \Phi(x) + \frac{1}{6} \gamma \delta^{-3} (x^2 - 1) n^{-\frac{1}{2}}\phi(x) + o(n^{-\frac{1}{2}}) \right\rvert \\
=\ &\sup_x \left\lvert \frac{1}{6} (\gamma \delta^{-3} - \gamma^* \delta^{*-3})(x^2 - 1)n^{-\frac{1}{2}}\phi(x) + o(n^{-\frac{1}{2}}) \right\rvert \\
=\ &\sup_x \left\lvert \frac{1}{6} O_p(n^{-\frac{1}{2}}) (x^2 - 1) n^{-\frac{1}{2}} \phi(x) + o(n^{-\frac{1}{2}}) \right\rvert \\
=\ &\sup_x \left\lvert O_p(n^{-1}) + o(n^{-\frac{1}{2}}) \right\rvert \\
=\ &\sup_x \left\lvert o_p(n^{-\frac{1}{2}}) \right\rvert \\
=\ &o_p(n^{-\frac{1}{2}}),
\end{align}
where ``$\sup_x$'' may be removed since $(x^2 - 1)\phi(x)$ is bounded on $x \in \mathbb{R}$, and by theorem \ref{th:edgeworth}.
\end{subequations}

\bibliography{references}

\end{document}