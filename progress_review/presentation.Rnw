\documentclass{beamer}

\usepackage{
	mathtools,
	mathrsfs,
	amsmath,
	amsfonts,
	amssymb,
	amsthm,
	tensor,
	float
}

\usepackage{natbib}
\bibliographystyle{plainnat}
\setcitestyle{authoryear, open={(},close={)}}

\DeclareMathOperator*{\argmin}{\arg\!\min}
\DeclareMathOperator*{\argmax}{\arg\!\max}

\usetheme{Madrid}

\defbeamertemplate{subsubsection in toc}{subsubsections numbered}
{\leavevmode\leftskip=3em%
 \rlap{\hskip-3em\inserttocsectionnumber.\inserttocsubsectionnumber.\inserttocsubsubsectionnumber}%
 \inserttocsubsubsection\par}

\setbeamertemplate{itemize items}[circle]
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{section in toc}[sections numbered]
\setbeamertemplate{subsection in toc}[subsections numbered]
\setbeamertemplate{subsubsection in toc}[subsubsections numbered]

\AtBeginSection[]
{
 \begin{frame}
 \frametitle{Outline}
 \tableofcontents[currentsection]
 \end{frame}
}

\title[Variability and Forecast Combinations]{Sampling Variability and the Performance of Distributional Forecast Combinations}
\author{Ryan Zischke}
\date{July 1, 2020}

\begin{document}

<<echo=FALSE>>=
setwd(file.path(dirname(getwd()), "sampling_variability_forecast_combinations"))
source("simulation_study.R")
source("covid_temp.R")
@

\frame{\titlepage}

\begin{frame}{Outline}
\frametitle{Outline}
\tableofcontents
\end{frame}

\section{Research Question}

\begin{frame}
\frametitle{Research Question}
\begin{itemize}
\item<1-> Forecast combinations are among the most competitive strategies for forecasting, evidenced by the M4 \citep{Makridakis2018, Makridakis2020} and M5 \citep{Makridakis2020a, Makridakis2020b} competitions.
\item<2-> Probabilistic forecast combinations \citep{Aastveit2018} are gaining prominence, and are among the best performers of the M5 uncertainty competition \citep{Makridakis2020b}.
\item<3-> Despite their winning performance, little attention has been paid to the formalisation and exploration of the role of sampling variability.
\item<4-> Optimally-weights combinations do not necessarily outperform equally-weighted combinations (\citealp{Clemen1989, Stock2004}; \ldots), this is the `forecast combination puzzle'.
\item<5-> How does finite-sample estimation error impact the performance of distributional forecast combinations with different combination functions, different ways of estimating the parameters, and different degrees of misspecification?
\end{itemize}
\end{frame}

\section{Thesis Overview}

\begin{frame}
\frametitle{Thesis Overview}
\begin{itemize}
\item<1-> Chapter 1 - A Short History of Forecast Combinations
\begin{itemize}
\item<2-> An introductory chapter.
\item<3-> Forecast combinations have had a long, rich and successful history, spanning multiple fields and industries \citep{Clemen1989}.
\item<4-> More recently, interest in distributional forecast combinations has grown \citep{Aastveit2018}.
\item<5-> Forecast combinations are highly competitive \citep{Makridakis2018, Makridakis2020, Makridakis2020a, Makridakis2020b}.
\item<6-> Highlight the absence of the formalisation and analysis of sampling variability as it pertains to forecast performance of combinations.
\item<7-> We expect this short introductory chapter to take one month to complete.
\end{itemize}
\item<8-> Chapter 2 - A Review of Standard Asymptotic Theory
\begin{itemize}
\item<9-> An introductory chapter.
\item<10-> Review the framework of \textit{estimating functions} \citep{Jacod2018}.
\item<11-> Argue that this framework is fully equipped to address the research question.
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Thesis Overview}
\begin{itemize}
\item<1-> Chapter 3 - The Parametric Bootstrap and the Distributional Forecast Combination Puzzle
\begin{itemize}
\item<2-> First research project.
\item<3-> Apply maximum likelihood estimation theory (which is nested by the framework of \citealp{Jacod2018}) to the standard context in which the forecast combination puzzle is usually explored: assume correct specification, and take the constituent models as given.
\item<4-> Apply the parametric bootstrap to the task of estimating the sampling distribution of the forecast performance in terms of the one-step-ahead expected score.
\item<5-> Progress identical to confirmation in July 2020.
\item<6-> We expect this project to take an additional three months.
\end{itemize}
\item<7-> Chapter 4 - On Measuring the Sampling Variability of Estimating Combinations of Distributional Forecasts: Theory and Application to COVID-19 Forecasting
\begin{itemize}
\item<8-> The second research project, and the primary topic of today's talk.
\item<9-> We expect this project to take another three months to complete.
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Thesis Overview}
\begin{itemize}
\item<1-> Chapter 5 - The Robustness of Distributional Forecast Combinations to Model Misspecification
\begin{itemize}
\item<2-> We will study how the degree of model misspecification impacts on forecast performance for a variety of forecast combinations.
\item<3-> Misspecification can be measured under a variety of scores by using the associated divergence of the optimal parameter values in the forecast combination with respect to the data generating process \citep{Gneiting2007}.
\item<4-> We hypothesise that, conditional on a particular level of misspecification, a lower sampling variability will imply a higher robustness to misspecification.
\item<5-> Broadly speaking, three aspects define a combination: the constituent models, the combination function and the parameter estimates.
\item<6-> We can use the framework of \cite{Jacod2018} to analyse the impact that these aspects have on asymptotic sampling variability, and test the extent to which these trends apply in finite samples through Monte Carlo simulation.
\item<7-> We expect this project to take eight months to complete.
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Thesis Overview}
\begin{itemize}
\item<1-> Chapter 6 - Conclusion
\begin{itemize}
\item<2-> In this chapter, we will summarise the findings of the thesis, and discuss potential topics of further research.
\item<3-> We expect this chapter to take one month to complete.
\end{itemize} 
\end{itemize}
\end{frame}

\section{Distributional Forecast Combinations}

\begin{frame}
\frametitle{Distribution Forecast Combinations}
\begin{itemize}
\item<1-> Consider a sample $Y_1, Y_2, \cdots, Y_n$.
\item<2-> Let $F_1(y_t \mid y_{1:t-1} ; \gamma_1), F_2(y_t \mid y_{1:t-1} ; \gamma_2), \cdots, F_K(y_t \mid y_{1:t-1} ; \gamma_K)$ be the predictive CDFs of $K$ \textit{constituent models}, parameterised by column vectors $\gamma_1, \gamma_2, \cdots, \gamma_K$.
\item<3-> Now define a combination function $Q_{\eta}$ which produces the predictive CDF $F(y_t \mid y_{1:t-1} ; \eta, \gamma_1, \cdots, \gamma_K)$ of the \textit{distributional forecast combination} from the CDFs of the constituents. The column vector $\eta$ parameterises the combination function.
\item<4-> For convenience, stack the constituent model parameters into a single vector $\gamma$, and likewise stack all parameters into $\theta$:
	\begin{equation*} 
		\gamma = \begin{bmatrix} \gamma_1' & \gamma_2' & \cdots & \gamma_K' \end{bmatrix}',\ \theta = \begin{bmatrix} \eta' & \gamma' \end{bmatrix}'.
	\end{equation*}
\item<5-> Now we can also write the predictive CDF of the forecast combination as $F(y_t \mid y_{1:t-1} ; \eta, \gamma) \equiv F(y_t \mid y_{1:t-1} ; \theta)$.
\item<6-> Let $f_1(y_t \mid y_{1:t-1} ; \gamma_1), \cdots, f_K(y_t \mid y_{1:t-1} ; \gamma_K)$ be the predictive PDFs of the constituents, and $p(y_t \mid y_{1:t-1} ; \theta)$ that of the combination.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{A Simple Distributional Forecast Combination}
\begin{itemize}
\item<1-> Consider a linear pool \citep{Stone1961, Hall2007, Geweke2011} containing an AR(1) and an ARCH(1).
\item<2-> Our constituent models are defined by
\begin{align*}
	y_t &= \alpha_0 + \alpha_1 y_{t-1} + \epsilon_t,\ \epsilon_t \overset{i.i.d.}{\sim} N(0, \sigma^2), \\
	y_t &= \mu + \nu_t \epsilon_t,\ \nu_t^2 = \beta_0 + \beta_1 \nu_{t-1}^2 \epsilon_{t-1}^2,\ \epsilon_t \overset{i.i.d.}{\sim} N(0,1).
\end{align*}
\item<3-> This leads to the following constituent predictive densities:
\begin{align*}
f_1(y_t \mid y_{1:t-1}) &= \frac{1}{\sigma} \phi \left( \frac{y_t - \alpha_0 - \alpha_1 y_{t-1}}{\sigma} \right), \\
f_2(y_t \mid y_{1:t-1}) &= \frac{1}{\sqrt{\beta_0 + \beta_1 (y_{t-1} - \mu)^2}} \phi \left( \frac{y_t - \mu}{\sqrt{\beta_0 + \beta_1 (y_{t-1} - \mu)^2}} \right),
\end{align*}
where $\phi$ is the PDF of the standard normal distribution.
\item<4-> The predictive density of the linear pool is $p(y_t \mid y_{1:t-1}) = w f_1(y_t \mid y_{1:t-1}) + (1 - w) f_2(y_t \mid y_{1:t-1})$.
\item<5-> Per the last slide, $\gamma_1 = \begin{bmatrix} \alpha_0 & \alpha_1 & \sigma^2 \end{bmatrix}$, $\gamma_2 = \begin{bmatrix} \mu & \beta_0 & \beta_1 \end{bmatrix}$, $\eta = w$.
\end{itemize}
\end{frame}

\section{Estimating the Parameters}

\begin{frame}
\frametitle{Estimating the Parameters}
\begin{itemize}
\item<1-> We compare the consequences of sampling variability on forecast performance for two different ways of estimating the parameters of a distributional forecast combination.
\item<2-> Most forecast combinations are estimated as \textit{two-step estimators} \citep{Hall2007, Geweke2011, Gneiting2013}.
\item<3-> This means estimating the constituent model parameters $\gamma_1, \cdots, \gamma_K$ individually, and then holding those parameters fixed at their point estimates when estimating the combination parameter vector $\eta$.
\item<4-> We can also estimate the combination parameters in a \textit{one-step} fashion, which often means estimating all parameters at once to maximise some criterion function \citep{Jacod2018}.
\item<5-> As far as we are aware, we are the first to recognise that forecast combinations are amenable to the two-step estimators framework of \cite{Pagan1986} (also see \citealp{Newey1994}, Chapter 6).
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Estimating the Parameters}
\begin{itemize}
\item<1-> Let $S(\vartheta, y_t \mid y_{1:t-1})$ be the \textit{score} \citep{Gneiting2007} associated with the choice of parameter $\vartheta$, given $y_{1:t-1}$, should $y_t$ materialise. A higher score indicates a better fit.
\item<2-> Let $\mathcal{S}_n(\vartheta) = n^{-1} \sum_{t=1}^n S(\vartheta, y_t \mid y_{1:t-1})$ be the average score across the sample associated with parameter $\vartheta$.
\item<3-> The two-step estimator for the forecast combination is
\begin{align*}
\tilde{\gamma}_{jn} &= \argmax_{\gamma_j} \mathcal{S}_n(\gamma_j),\ j = 1, 2, \cdots, K, \\
\tilde{\eta}_n &= \argmax_{\eta} \mathcal{S}_n(\eta, \gamma)\text{ s.t. }\gamma = \tilde{\gamma}_n,
\end{align*}
where $\tilde{\gamma}_n = \begin{bmatrix} \tilde{\gamma}_{1n}' & \cdots & \tilde{\gamma}_{Kn}' \end{bmatrix}'$, $\tilde{\theta}_n = \begin{bmatrix} \tilde{\eta}_n' & \tilde{\gamma}_n' \end{bmatrix}'$.
\item<4-> The one-step estimator is
\begin{equation}
\hat{\theta}_n = \begin{bmatrix} \hat{\eta}_n \\ \hat{\gamma}_n \end{bmatrix} = \argmax_{\theta} \mathcal{S}_n(\eta, \gamma).
\end{equation}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Estimating the Parameters - Asymptotics}
\begin{itemize}
\item<1-> Under regularity, these average-score-maximising parameter estimates converge to their expected-score-maximising counterparts.
\item<2-> Let $\mathcal{S}(\vartheta) = \underset{n \to \infty}{\mathrm{plim}} \mathcal{S}_n(\vartheta) = \lim_{t \to \infty} \mathbb{E}[S(\vartheta, Y_t \mid Y_{1:t-1})]$.
\item<3-> Then the expected-score-maximising counterpart to the two-step estimator is
\begin{align*}
\gamma^{\star}_j &= \argmax_{\gamma_j} \mathcal{S}(\gamma_j),\ j = 1, 2, \cdots, K, \\
\eta^{\star} &= \argmax_{\eta} \mathcal{S}(\eta, \gamma)\text{ s.t. }\gamma = \gamma^{\star},
\end{align*}
where $\gamma^{\star} = \begin{bmatrix} \gamma^{\star \prime}_1 & \cdots & \gamma^{\star \prime}_K \end{bmatrix}'$, $\theta^{\star} = \begin{bmatrix} \eta^{\star \prime} & \gamma^{\star \prime} \end{bmatrix}'$.
\item<4-> For the one-step estimator, $\theta^0 = \begin{bmatrix} \eta^{0 \prime} & \gamma^{0 \prime} \end{bmatrix}' = \argmax_{\theta} \mathcal{S}(\eta, \gamma)$.
\item<5-> Under regularity, $\tilde{\theta}_n \overset{p}{\to} \theta^{\star}$ and $\hat{\theta}_n \overset{p}{\to} \theta^0$.
\item<6-> Also, $\sqrt{n}(\tilde{\theta}_n - \theta^{\star}) \Rightarrow N(0, W^{\star})$ and $\sqrt{n}(\hat{\theta}_n - \theta^{0}) \Rightarrow N(0, W^0)$ for some variance-covariance matrices $W^{\star}$ and $W^0$ \citep{Newey1994, Jacod2018}.
\end{itemize}
\end{frame}

\section{Consequences of Two-Step Estimation}

\begin{frame}
\frametitle{Consequences of Two-Step Estimation}
\begin{itemize}
\item<1-> Following \cite{Diebold1995}, \cite{Hansen2005} and \cite{Giacomini2006}, we will measure the performance of our forecast combination with the expected score.
\item<2-> We use $\mathcal{S}$ to measure forecast performance. Recall that this is the expected score of the one-step-ahead forecast distribution.
\item<3-> We will view $\mathcal{S}$ as a deterministic function, which we will evaluate at the estimated parameters.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Consequences of Two-Step Estimation}
\begin{itemize}
\item<1-> Consider an arbitrary $\sqrt{n}$-asymptotically-normal estimator $\check{\theta}_n \overset{p}{\to} \overline{\theta}$.
\item<2-> Under regularity, a Taylor series expansion gives
\begin{align*}
\mathcal{S}(\check{\theta}_n) - \mathcal{S}(\overline{\theta}) &= \sum\nolimits_i (\check{\theta}_{ni} - \overline{\theta}_i) [\partial \mathcal{S}(\overline{\theta}) / \partial \theta_i] + \mathcal{O}_p(n^{-1}) \\
&= \sum\nolimits_i \mathcal{O}_p(n^{-1/2}) [ \partial \mathcal{S}(\overline{\theta}) / \partial \theta_i ] + \mathcal{O}_p(n^{-1}).
\end{align*}
\item<3-> For every element $\overline{\theta}_i$ that maximises $\mathcal{S}(\theta)$, $\partial \mathcal{S}(\overline{\theta}) / \partial \theta_i = 0$ causing an otherwise $\mathcal{O}_p(n^{-1/2})$ term to be dropped from the sampling variability of the forecast performance $\mathcal{S}(\check{\theta}_n)$.
\item<4-> For the two-step estimator ($\check{\theta}_n = \tilde{\theta}_n$, $\overline{\theta} = \theta^{\star}$), $\eta^{\star}$ maximises $\mathcal{S}(\theta)$ so the contribution from $\tilde{\eta}_n$ is $\mathcal{O}_p(n^{-1})$. However, $\gamma^{\star}$ does not maximise $\mathcal{S}(\theta)$, so the contribution from $\tilde{\gamma}_n$ is $\mathcal{O}_p(n^{-1/2})$.
\item<5-> For the one-step estimator ($\check{\theta}_n = \hat{\theta}_n$, $\overline{\theta} = \theta^0$), all elements of $\theta^0$ maximise $\mathcal{S}(\theta)$, so there is no $\mathcal{O}_p(n^{-1/2})$ contribution to the sampling variability.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Consequences of Two-Step Estimation}
\begin{itemize}
\item<1-> So $\mathcal{S}(\hat{\theta}_n) = \mathcal{S}(\theta^0) + \mathcal{O}_p(n^{-1})$ while $\mathcal{S}(\tilde{\theta}_n) = \mathcal{S}(\theta^{\star}) + \mathcal{O}_p(n^{-1/2})$.
\item<2-> That is, the forecast performance of the one-step estimator converges faster than the performance of the two-step estimator.
\item<3-> Since $\theta^0$ maximises the expected score, not only does the one-step estimator have a higher limiting forecast performance than the two-step estimator, it also has a lower limiting sampling variability.
\item<4-> For the two-step estimator, $\lVert \mathcal{S}(\tilde{\eta}_n, \tilde{\gamma}_n) - \mathcal{S}(\eta^{\star}, \tilde{\gamma}_n) \rVert = o_p(n^{-1/2})$.
\item<5-> That is, the contribution of $\tilde{\eta}_n$ to the sampling variability of the forecast performance diminishes at a rate of $o_p(n^{-1/2})$, and the $\mathcal{O}_p(n^{-1/2})$ contribution from $\tilde{\gamma}_n$ dominates in the limit.
\item<6-> Therefore, $\sqrt{n}(\mathcal{S}(\tilde{\eta}_n, \tilde{\gamma}_n) - \mathcal{S}(\eta^{\star}, \gamma^{\star}))$ has the same asymptotic distribution as $\sqrt{n}(\mathcal{S}(\eta^{\star}, \tilde{\gamma}_n) - \mathcal{S}(\eta^{\star}, \gamma^{\star}))$, meaning that the sampling distribution of the forecast performance is, at first order, not affected by uncertainty in the parameter estimates of the weights.
\item<7-> Given optimising the combination function parameters imparts little additional sampling variability asymptotically, these results support the `optimise the weights' position in the forecast combination puzzle.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Consequences of Two-Step Estimation}
\begin{itemize}
\item<1-> The winner of the M4 forecasting competition was a forecast combination estimated via one-step estimation \citep{Smyl2020}.
\item<2-> In a competition dominated by forecast combinations, Smyl was the only competitor to estimate parameters in a one-step fashion \citep{Makridakis2020}.
\end{itemize}
\end{frame}

\section{Monte Carlo Simulation}

\begin{frame}
\frametitle{Monte Carlo Simulation}
<<echo=FALSE, fig.width=7, fig.height=4.7, out.width="4.5in", out.height="3in">>=
grid.draw(simulation.study.plot)
@
\end{frame}

\section{Estimating Forecast Performance Asymptotic Distributions}

\begin{frame}
\frametitle{Estimating Forecast Performance Asymptotic Distributions}
\begin{itemize}
\item<1-> Using the delta method, we derive the asymptotic distributions for the forecast performance for the one- and two-step estimators of the parameters for the distributional forecast combination.
\item<2-> For the two-step combination, we have
\begin{align*}
\sqrt{n} \lbrace \mathcal{S}(\tilde{\theta}_n) - \mathcal{S}(\theta^{\star}) \rbrace &\Rightarrow N(0, [\partial \mathcal{S}(\eta^{\star}, \gamma^{\star}) / \partial \gamma]' W^{\star}_{\gamma} [\partial \mathcal{S}(\eta^{\star}, \gamma^{\star} / \partial \gamma)]), \\
\therefore\ \mathcal{S}(\tilde{\theta}_n) - \mathcal{S}(\theta^{\star}) &\overset{\mathrm{apx.}}{\sim} N(0, n^{-1} [\partial \mathcal{S}(\eta^{\star}, \gamma^{\star}) / \partial \gamma]' W^{\star}_{\gamma} [\partial \mathcal{S}(\eta^{\star}, \gamma^{\star} / \partial \gamma)]),
\end{align*}
where $W^{\star}_\gamma$ is the $\gamma \gamma$-block of $W^{\star}$, the asymptotic covariance matrix of $\sqrt{n} (\tilde{\theta}_n - \theta^{\star})$.
\item<3-> For the one-step combination, we have
\begin{align*}
n \lbrace \mathcal{S}(\hat{\theta}_n) - \mathcal{S}(\theta^0) \rbrace &\Rightarrow - \frac{1}{2} X' [- \partial^2 \mathcal{S}(\theta^0) / \partial \theta \partial \theta' ] X, \\
\therefore\ \mathcal{S}(\hat{\theta}_n) - \mathcal{S}(\theta^0) &\overset{\mathrm{apx.}}{\sim} - \frac{1}{2n} X' [- \partial^2 \mathcal{S}(\theta^0) / \partial \theta \partial \theta' ] X,
\end{align*}
where $X$ is a $N(0, W^0)$ distributed random column vector, and $W^0$ is the asymptotic covariance matrix of $\sqrt{n} (\hat{\theta}_n - \theta^0)$.
\end{itemize}
\end{frame}

\section{Forecast Performance in a Forecast Combination of COVID-19 Infections}

\begin{frame}
\frametitle{Forecast Performance in a Forecast Combination of COVID-19 Infections}
\begin{itemize}
\item<1-> We develop a model to forecast (cumulative) COVID-19 infections across different states and countries around the world.
\item<2-> Data on COVID-19 infections were retrieved from the COVID-19 Data Repository by the Center for Systems Science and Engineering (CSSE) \citep{Dong2020}.
\item<3-> Country level demographic data were obtained from \cite{UniNationsStatisticsDivision2008}.
\item<4-> Google's Community Mobility Reports \citep{Aktay2020} provide data on the amount of person-time spent at home, work and in various public locations.
\item<5-> Our asymptotic distributions for the performance assume that the data is stationary. Using the strategy of \cite{Hyndman2008}, we conclude that taking the differenced series of daily case numbers as our sample is appropriate.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Forecast Performance in a Forecast Combination of COVID-19 Infections}
<<echo=FALSE, fig.width=7, fig.height=4.7, out.width="4.5in", out.height="3in">>=
grid.draw(australia_victoria_plot)
@
\end{frame}

\begin{frame}
\frametitle{Forecast Performance in a Forecast Combination of COVID-19 Infections}
\begin{itemize}
\item<1-> We use a forecast combination of two models. Both models assume that the sample is normally distributed with a mean and variance that depend on demographic characteristics and mobility characteristics.
\item<2-> The mean of the first constituent also depends on recent case numbers.
\item<3-> The variance of the second constituent also depends on recent residuals.
\item<4-> Both the one-step and two-step estimates for the parameters were conducted using maximum likelihood estimation, that is, with $S$ as the log score.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Forecast Performance in a Forecast Combination of COVID-19 Infections}
<<echo=FALSE, fig.width=7, fig.height=4.7, out.width="4.5in", out.height="3in">>=
grid.draw(divergence_plot)
@
\end{frame}

\section{Extensions and Improvements}

\begin{frame}
\frametitle{Extensions and Improvements}
\begin{itemize}
\item<1-> Add scores to the Monte Carlo and empirical section other than the log score.
\item<2-> Consider a measure of performance that incorporates multi-step forecasting \citep{Bhansali2002, Taieb2012}, and that acknowledges that the random variables forecasted by the combination occur immediately after the random variables used to estimate the parameters.
\item<3-> Explore the Bootstrap (e.g. \citealp{Andrews2002}) as an alternative to standard GMM-based estimators for the asymptotic distributions of the forecast performance.
\end{itemize}
\end{frame}

\section{References}
\begin{frame}[allowframebreaks]
\frametitle{References}
\bibliography{library.bib}
\end{frame}

\end{document}