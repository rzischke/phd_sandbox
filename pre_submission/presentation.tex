% To produce the files required to compile this document, execute geweke_plot.R and smith_plot.R, followed by figure/tex_to_pdf.sh.
% Each of these files must be executed from the (working) directory containing the file.

\documentclass[aspectratio=169]{beamer}
%\documentclass[handout,aspectratio=169]{beamer}

\usepackage{
	mathtools,
	mathrsfs,
	amsmath,
	amsfonts,
	amssymb,
	amsthm,
	tensor,
	float,
	graphicx
}

\usepackage{natbib}
\bibliographystyle{plainnat}
\setcitestyle{authoryear, open={(}, close={)}}

\DeclareMathOperator*{\argmin}{\arg\!\min}
\DeclareMathOperator*{\argmax}{\arg\!\max}

\usetheme{Madrid}

\defbeamertemplate{subsubsection in toc}{subsubsections numbered}
{\leavevmode\leftskip=3em%
 \rlap{\hskip-3em\inserttocsectionnumber.\inserttocsubsectionnumber.\inserttocsubsubsectionnumber}%
 \inserttocsubsubsection\par}

\setbeamertemplate{itemize items}[circle]
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{section in toc}[sections numbered]
\setbeamertemplate{subsection in toc}[subsections numbered]
\setbeamertemplate{subsubsection in toc}[subsubsections numbered]

\AtBeginSection[]
{
 \begin{frame}
 \frametitle{Outline}
 \tableofcontents[currentsection]
 \end{frame}
}

\title[Variability and Forecast Combinations]{Sampling Variability and the Performance of Forecast Combinations}
\author{Ryan Zischke}

\begin{document}

\frame{\titlepage}

\begin{frame}{Outline}
\frametitle{Outline}
\tableofcontents
\end{frame}

\section{Research Question}

\def \negspacesmall {3mm}

\begin{frame}
\frametitle{Research Question}
\begin{itemize}
\item<1-> \textit{Forecast combinations} are weighted combinations of forecasts from distinct models \citep{Timmermann2006, Aastveit2019}.
\vspace{\negspacesmall}
\item<2-> Highly competitive \citep{Makridakis2020, Makridakis2020a, Makridakis2020b}.
\vspace{\negspacesmall}
\item<3-> Little (theoretical) attention paid to the impact of sampling variability.
\vspace{\negspacesmall}
\item<4-> Optimally-weighted two-step combinations do not always outperform equally-weighted two-step combinations (\citealp{Clemen1989, Stock2004}; \ldots), this is the `forecast combination puzzle'.
\vspace{\negspacesmall}
\item<5-> How does finite-sample estimation error impact the performance of point and distributional forecast combinations and the hypothesis tests thereof?
\end{itemize}
\end{frame}

\section{Thesis Overview}

%\def \negspacesmall {7mm}

\begin{frame}
\frametitle{Thesis Overview}
\begin{itemize}
\item<1-> Paper 1 -- On Measuring the Sampling Variability of Estimated Combinations of Distributional Forecasts
\begin{itemize}
\item<2-> One-step asymptotically superior to two-step (including w.r.t. sampling variability).
\item<3-> All asymptotic sampling variability of two-step performance comes from constituents.
\end{itemize}
%\vspace{\negspacesmall}
\item<4-> \textbf<12->{Paper 2 -- New Insights into Forecast Combinations}
\begin{itemize}
\item<5-> One-step beats two-step in hypothesis tests of predictive accuracy (e.g. \citealp{Diebold1995}), asympotitcally.
\item<6-> Tests of predictive accuracy have no size control and no local power for two-step combinations.
\end{itemize}
\item<7-> Paper 3 -- The Parametric Bootstrap and the Distributional Forecast Combination Puzzle
\begin{itemize}
\item<8-> Two-step combinations estimated using MLE.
\item<9-> Sampling distribution estimated with parametric boostrap.
\item<10-> Rule-of-thumb for when to shift from equally-weighted to optimally-weighted.
\item<11-> Focus on when sample size is large enough for asymptotic results to prevail.
\end{itemize}
\end{itemize}
\end{frame}

\section{Forecast Combinations}

\def \negspacesmall {5mm}

\begin{frame}
\frametitle{Forecast Combinations}
\begin{itemize}
\item<1-> A recipe:
\vspace{\negspacesmall}
\begin{enumerate}
\item<2-> Obtain \textit{constituent models}, parameterised by $\gamma$.
\vspace{\negspacesmall}
\item<3-> Specify \textit{combination function} (e.g. \citealp{Stone1961, Bates1969}), parameterised by $\eta$.
\vspace{\negspacesmall}
\item<4-> Specify reward function $S$ or loss function $L = -S$ \citep{Hyndman2006, Gneiting2007}.
\vspace{\negspacesmall}
\item<5-> Optimise $\theta = [\eta^{\prime}\ \gamma^{\prime}]^{\prime}$ according to $S$ (or equivalently $L$).
\end{enumerate}
\end{itemize}
\end{frame}

\section{One- and Two-Step Estimation}

\subsection{Computation}

\def \negspace {5mm}

\begin{frame}
\frametitle{One- and Two-Step Estimation -- Computation}
\begin{itemize}
\item<1-> One-step estimation:
\vspace{\negspace}
\begin{enumerate}
\item<2-> $\hat{\theta}_T$ maximises average reward $S$ of combination.
\end{enumerate}
\vspace{\negspace}
\item<3-> Two-step estimation:
\vspace{\negspace}
\begin{enumerate}
\item<4-> $\tilde{\gamma}_{j,n}$ maximises average reward $S$ of model $j$, $\tilde{\gamma}_T = [\tilde{\gamma}^{\prime}_{1,n}\ \cdots\ \tilde{\gamma}^{\prime}_{K,n}]^{\prime}$.
\vspace{\negspace}
\item<5-> Either $\tilde{\eta}_T$ maximises average reward $S$ of combination given $\gamma = \tilde{\gamma}_T$ (optimally-weighted), or $\eta^e = [1/K\ \cdots\ 1/K]^{\prime}$ (equally-weighted).
\end{enumerate}
\vspace{\negspace}
\item<6-> $\hat{\theta}_T = [\hat{\eta}_T^{\prime}\ \hat{\gamma}_T^{\prime}]^{\prime}$, $\tilde{\theta}_T = [\tilde{\eta}_T^{\prime}\ \tilde{\gamma}_T^{\prime}]^{\prime}$, $\tilde{\theta}^e_T = [\eta^{e \prime}\ \tilde{\gamma}_T^{\prime}]^{\prime}$.
\end{itemize}
\end{frame}

\subsection{Forecast Performance Asymptotics}

\begin{frame}
\frametitle{One- and Two-Step Estimation -- Forecast Performance Asymptotics}
\begin{itemize}
\item<2-> Measure forecast performance by $\mathcal{S}_0(\theta)$: expected out-of-sample reward $S$.
\item<3-> Under regularity \citep{Newey1994}:
\begin{gather*}
\hat{\theta}_T \overset{p}{\to} \theta^0 = \argmax_{\theta} \mathcal{S}_0(\theta),\ \tilde{\theta}_T \overset{p}{\to} \theta^{\star} \neq \theta^0,\ \tilde{\theta}^e_T \overset{p}{\to} \theta^e \notin \{\theta_0, \theta^{\star}\}.
\end{gather*}
\item<4-> For $T$ large:
\item<5->[] \begin{equation*}
\mathcal{S}_0(\hat{\theta}_T) > \mathcal{S}_0(\tilde{\theta}_T) > \mathcal{S}_0(\tilde{\theta}^e_T),
\end{equation*}
\item<6->[] \begin{align*}
\mathcal{S}_0(\hat{\theta}_T) - \mathcal{S}_0(\theta^0) &\approx \frac{1}{2} (\hat{\theta}_T - \theta^0)^{\prime} H (\hat{\theta}_T - \theta^0) = \mathcal{O}_p(n^{-1}), \\
\mathcal{S}_0(\tilde{\theta}_T) - \mathcal{S}_0(\theta^{\star}) &\approx (\tilde{\gamma}_T - \gamma^{\star})^{\prime} G = \mathcal{O}_p(n^{-1/2}), \\
\mathcal{S}_0(\tilde{\theta}^e_T) - \mathcal{S}_0(\theta^e) &\approx (\tilde{\gamma}_T - \gamma^{\star})^{\prime} G^e = \mathcal{O}_p(n^{-1/2}).
\end{align*}
\end{itemize}
\end{frame}

\subsection{Hypothesis Testing}

\begin{frame}
\frametitle{One- and Two-Step Estimation -- Hypothesis Testing}
\begin{itemize}
\item<2-> Null of \textit{\textbf{no inferior predictive accuracy}} of a benchmark $\check{\vartheta}^b_T \overset{p}{\to} \vartheta^b_0$ over the alternative $\check{\vartheta}_T \overset{p}{\to} \vartheta_0$:
\begin{equation*}
\mathrm{H}_0\ :\ \mathcal{S}_0(\vartheta^b_0) \geq \mathcal{S}_0(\vartheta_0).
\end{equation*}
\item<3-> Define test statistic $D_{\tau}(\check{\vartheta}^b_T, \check{\vartheta}_T)$ by
\begin{gather*}
D_{\tau}(\check{\vartheta}^b_T, \check{\vartheta}_T) = \hat{\Omega}_T^{-1/2} \sqrt{\tau} \Delta_{\tau}(\check{\vartheta}^b_T, \check{\vartheta}_T), \\
\Delta_{\tau} = \frac{1}{\tau} \sum_{t = T}^{n-1} \left[ S(f^{(t)}_{\check{\vartheta}_T}, Y_{t+1}) - S(f^{(t)}_{\check{\vartheta}^b_T}, Y_{t+1}) \right],
\end{gather*}
where $\tau = n - T$, $\lVert \hat{\Omega}_T - \mathrm{Var}(\Delta_{\tau}) \rVert \overset{p}{\to} 0$, and $f^{(t)}_{\vartheta}$ is the (point or distributional) forecast for $Y_{t+1}$ given information up to time $t$ and parameter vector $\vartheta$.
\item<4-> At nominal size $\alpha$ and for $\Phi^{-1}$ the quantile function of a standard normal distribution, reject $\mathrm{H}_0$ when inside the rejection region
\begin{equation*}
W_{\tau}(\alpha, \check{\vartheta}^b_T, \check{\vartheta}_T) = \{ D_{\tau}(\check{\vartheta}^b_T, \check{\vartheta}_T) > \Phi^{-1}(1 - \alpha) \}.
\end{equation*}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{One- and Two-Step Estimation -- Hypothesis Testing}
\begin{itemize}
\item<1-> Testing reflects asymptotic performance ranking of estimators. For all $0 < \alpha < 1$, we have
\begin{equation*}
\lim_{n \to \infty} \mathrm{Pr}\{W_{\tau}(\alpha, \tilde{\theta}_T, \hat{\theta}_T)\} = \lim_{n \to \infty} \mathrm{Pr}\{W_{\tau}(\alpha, \tilde{\theta}^e_T, \tilde{\theta}_T)\} = 1.
\end{equation*}
\item<2-> For two-step estimation, the test of no inferior predictive accuracy of the equally-weighted benchmark $\tilde{\theta}^e_T$ over the optimally-weighted alternative $\tilde{\theta}_T$ has low power and no size control.
\item<3-> In particular, for $0 < \alpha < 1$ and a sequence of DGPs where the limiting optimal two-step weights $\eta^{\star}$ satisfy $\sqrt{T}\lVert \eta^{\star} - \eta^e \rVert < B < \infty$, we have 
\begin{equation*}
\lim_{n \to \infty} \mathrm{Pr}\{W_{\tau}(\alpha, \tilde{\theta}^e_T, \tilde{\theta}_T)\} = 0.
\end{equation*}
\end{itemize}
\end{frame}

\section{Monte Carlo Exercises}

\subsection{Limiting Performance}

\begin{frame}[plain]
\begin{center}
\includegraphics[height=\textheight]{figure/FIG3}
\end{center}
\end{frame}

\subsection{Hypothesis Testing}

\begin{frame}[plain]
\begin{center}
\includegraphics[width=\textwidth]{figure/FIG4}
\end{center}
\end{frame}

\section{References}
\begin{frame}[allowframebreaks]
\frametitle{References}
\bibliography{library.bib}
\end{frame}

\end{document}