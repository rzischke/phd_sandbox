\documentclass[12pt]{article}

\usepackage[margin=3cm]{geometry}
\usepackage{mathtools}
\usepackage{mathrsfs}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{tensor}
\usepackage{enumitem}
\usepackage{pdflscape}
\usepackage{float}
\usepackage{booktabs}
\usepackage{multicol}
\usepackage{setspace}

\setstretch{1.5}

\usepackage{natbib}
\bibliographystyle{plainnat}
\setcitestyle{authoryear, open={(},close={)}}

\DeclareMathOperator*{\argmin}{\arg\!\min}
\DeclareMathOperator*{\argmax}{\arg\!\max}

\usepackage{lipsum}

\begin{document}

\title{Sampling Variability and Forecast Combinations}

\author{Ryan Zischke}

\maketitle

\section{Introduction}

Since they first garnered the attention of researchers over a half-century ago (\citealp{Stone1961}; \citealp{Bates1969}), forecast combinations -- that is, the combination of either point or distributional forecasts from distinct sources -- have captivated forecasters for their persistently high performance across a wide range of applications (e.g. \citealp{Makridakis2018}; \citealp{Thorey2018}; \citealp{Wang2018}; \citealp{Makridakis2020}; \citealp{Taylor2020}).

The contribution of this thesis is an exploration of the consequences of \textit{sampling variability} for forecast combinations, and especially its impact on forecast performance. The key insight is that forecast combinations are amenable to standard asymptotic analysis, such as that which pertains to estimating functions (e.g. \citealp{Jacod2018}), the generalised method of moments (e.g. \citealp{Newey1994}), and \textit{two-stage} extremum estimation (\citealp{Pagan1986}; \citealp{Newey1994}; \citealp{Frazier2017}). This insight enables forecast practice to be viewed through a more formal prism, and guidance for practitioners to be based on rigorous statistical theory. We shed particular light on the so-called `forecast combination puzzle' \citep{Clemen1989}; i.e. the existance of mixed evidence for placing greater emphasis on high-performing constituent models via the optimisation of a criterion function (`optimal weights'), versus giving equal emphasis to each constituent \textit{a priori} (`equal weights') (see e.g. \citealp{Stock2004}; \citealp{Genre2013}; \citealp{Hsiao2014}; \citealp{Martin2021}).

In general, our work considers distributional and point forecast combinations estimated according to a variety of forecast performance measures, and we do not assume that any given forecast combination is correctly specified with respect to the true data generating process. All three draft papers are attached.

\section{Thesis Structure}

\subsection{Paper 1 - On Measuring the Sampling Variability of Estimated Combinations of Distributional Forecasts}

In the first paper, we begin by demonstrating that a \textit{two-stage} extremum estimation framework (\citealp{Pagan1986}; \citealp{Newey1994}; \citealp{Frazier2017}) can be used to analyse commonly applied forecast combination approaches. Through this lens, we show that, in the limit, all sampling variability in the \textit{performance} of such combinations derives from estimation of the constituent models, with no contribution from estimation of the weights. Regarding the forecast combination puzzle, this finding is in support of optimal combinations, since these have a limiting forecast performance at least as high as their equally weighted counterparts with no added limiting sampling variability. We also show that estimating the parameters of a forecast combination in one stage, with constituents and weights optimised jointly, delivers higher limiting forecast performance with lower sampling variability relative to its (optimally- or equally-weighted) two-stage counterpart. All results are illustrated in an extensive Monte Carlo simulation exercise, and in an empirical illustration using a time series of daily S\&P500 returns.\footnote{We make judiscious use of colour in the figures of this paper, which are best viewed on a computer.} This paper is to all intents and purposes complete, and is being prepared for journal submission.

\subsection{Paper 2 - New Insights into Forecast Combinations}

In the second paper, which is also the focus of the oral presentation, we build on the results of the first paper to argue that the forecast combination puzzle is an artefact of the interaction between: a) the two-stage approach commonly used in forecast combination production, and b) the hypothesis testing approaches commonly employed (e.g. \citealp{Diebold1995}) to crown the equally- or optimally-weighted forecast combination victorious in a given forecasting contest. We demonstrate, for the first time, that such tests lack power at sample sizes commonly seen in investigations of the forecast combination puzzle. We show that this lack of power is a direct consequence of the two-stage nature by which the forecast combinations are produced. More specifically, we show that standard tests of forecast performance have no power against deviations lying in a $\sqrt{n}$-neighbourhood of the best-performing combination. For example, this result demonstrates that in the case of two models, if the best-performing combination is the equally weighted combination, then the test has no power against combination weights of the form $0.5 \pm 0.5/\sqrt{n}$. We illustrate these results by expanding on the Monte Carlo exercises of prominent papers in the forecast combinations literature (\citealp{Smith2009}; \citealp{Geweke2011}), and along the way catalogue several examples of two-stage combinations being outperformed by their one-stage counterparts in surprising contexts.\footnote{The figures of this paper are best viewed on a computer as well.} The paper explores combinations of \textit{both} point forecasts and distributional forecasts.

\subsection{Paper 3 - The Parametric Bootstrap and the Distributional Forecast Combination Puzzle}

In the third paper, we focus on correctly-specified linear pools \citep{Stone1961} of distributional forecasts estimated via maximum likelihood. To this forecast combination we apply the parametric bootstrap, and provide some regularity conditions for its validity. We also derive an elegant analytical expression for characterising forecast performance in a way that accounts for sampling variability, and apply the parametric bootstrap to the problem of estimating this measure of performance. This measure is then used to formalise the effect that sample size has on the relationship between the performance of equally- and optimally-weighted forecast combinations. We highlight some similarities between our measure of forecast performance and the AIC, and illustrate our results with a Monte Carlo simulation.

\section{Timetable and Progress}

The remaining six months of the thesis will primarily be spent on two empirical exercises, one for Paper 2 and one for Paper 3. All progress towards the PhD is documented fully in the attached papers. In the usual way, these fundamental contributions will be supplemented by introductory chapters that provide appropriate background. Note that a substantial effort towards producing a cohesive software framework to support the simulation and empirical exercises has been undertaken, but a documented, publicly available version thereof is out of scope for the thesis. This will be left for future work.

\bibliography{library.bib}

\end{document}