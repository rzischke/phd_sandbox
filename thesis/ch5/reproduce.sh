#!/bin/bash

set -e

Rscript --vanilla simulation_study.R
(cd figure ; ./tex_to_pdf.sh)

