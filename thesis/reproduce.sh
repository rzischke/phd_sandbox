#!/bin/bash

# Abort if any commands fail.
set -e

(cd ch3 ; ./reproduce.sh)
(cd ch4 ; ./reproduce.sh)
(cd ch5 ; ./reproduce.sh)

