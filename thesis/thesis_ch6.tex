\documentclass[12pt]{report}
\usepackage[a4paper, margin=1in]{geometry}
\usepackage{graphicx}
\usepackage{setspace}
\usepackage[hidelinks]{hyperref}
\usepackage{natbib}
\usepackage[nottoc]{tocbibind}
\usepackage{mathtools}
\usepackage{mathrsfs}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{colortbl}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{tabularx}
\usepackage{enumitem}
\usepackage{etoolbox}

\apptocmd{\thebibliography}{\raggedright}{}{}

% The following two lines stop scientific word from placing the contents before the title page.
\let\toc\tableofcontents
\renewcommand{\tableofcontents}{}

\newcommand{\argmin}{\operatornamewithlimits{argmin\,}}
\newcommand{\argmax}{\operatornamewithlimits{argmax\,}}
\newcommand{\plim}{\operatornamewithlimits{plim\,}}
\newcommand{\E}{\mathbb{E}}

\theoremstyle{definition}
\newtheorem{definition}{Definition}[chapter]
\newtheorem{example}{Example}[chapter]

\theoremstyle{remark}
\newtheorem{remark}{Remark}[chapter]

\theoremstyle{plain}
\newtheorem{proposition}{Proposition}[chapter]
\newtheorem{corollary}{Corollary}[chapter]
\newtheorem{lemma}{Lemma}[chapter]
\newtheorem{theorem}{Theorem}[chapter]
\newtheorem{conjecture}{Conjecture}[chapter]
\newtheorem{assumption}{Assumption}[chapter]

\definecolor{Gray}{gray}{0.9}

\setcounter{secnumdepth}{5}

\begin{document}

\doublespacing

\setcounter{chapter}{5}
\chapter{Promising Directions for Future Research} \label{chap:future}

The forecast combinations literature has had a strong empirical focus for most of its history, and many gaps remain at the intersection of large sample asymptotics, scoring functions and scoring rules, and the practical use of forecast combinations. Whilst this thesis has, we believe, gone a good way towards filling some of these gaps, we touch here on three ambitious directions for future research that would address some remaining questions. First, we have produced estimated forecast combinations exclusively in a frequentist manner, but there is now a large literature concerning the production of forecast combinations using the Bayesian paradigm. To what extent do the results concerning the relative performance merits of one- and two-stage combinations found in Chapter \ref{chap:sampvar} apply to one- and two-stage combinations estimated using a Bayesian approach? Second, standard hypothesis tests used to discriminate between the relative performance of different combination methods rely on a variety of regularity conditions imposed on the average out-of-sample losses of the competing forecasts. Given that forecast combinations produce forecasts with average out-of-sample losses that have unusual asymptotic properties (see Chapters \ref{chap:sampvar} and \ref{chap:puzzle}), is it possible that some of the regularity conditions underlying standard tests are violated, and if so, could this unusual behaviour be exploited to produce a more powerful test that is more sensitive to smaller differences in performance? Third, the parametric bootstrap explored in Chapter \ref{chap:boot} is simple to implement, but it assumes that the combination correctly specifies the data generating process. Are there bootstrap-based methods for time-series processes that could be used to conduct inference on the performance of forecast combinations, and do not assume a correctly specified model?

\section[Sampling Variability and the Performance of Bayesian Forecast Combinations]{Sampling Variability and the Performance of\\Bayesian Forecast Combinations}

A variety of Bayesian combination methods exist for both point forecasts and distributional forecasts (for literature reviews, see \citealp{Aastveit2019}, and \citealp{Wang2022}). Like their frequentist counterparts, Bayesian combination methods typically take constituent forecasts\footnote{Note that under the Bayesian paradigm, forecasts often take the form of posterior predictives, whereby uncertainty in the parameters is `integrated out' by taking an expectation over the posterior.} as given when producing the (posterior) predictive of the combination. That is, Bayesian combinations are typically estimated in two steps, whereby the posterior of the parameters underlying each constituent model is formed by applying Bayes rule to the likelihood implied by that constituent alone, and thus the posterior of each constituent does not depend on the other constituents or on the combination. In the one-step approach, we would instead undertake a single application of Bayes rule via the joint likelihood implied by the whole combination.

In Chapter \ref{chap:sampvar}, we saw that the choice between one- and two-step estimation has significant consequences for the performance of the combination, and that these consequences are in favour of using the one-step approach. In particular, we used frequentist, large-sample asymptotic arguments to show that the one-step performance converges faster and higher than the two-step performance, and that the sampling variation of the two-step performance is due entirely to variation in the constituent model parameter estimates.

An open question, therefore, is whether the same applies to Bayesian forecast combinations.\footnote{Evaluating Bayesian methodologies according to their frequentist properties is now common practice; see \cite{Ghosal2017}, as a textbook example.} An answer in the affirmative is suggested by the Bernstein-von Mises Theorem (e.g.\ \citealp{Vaart1998}, Theorem 10.1), which under regularity guarantees that the sampling distribution of the MLE and the corresponding posterior converge to the same asymptotic distribution. A straightforward application of the theorem is complicated, however, by the variety of ways that a two-step Bayesian combination may combine constituent predictives.

\section{A More Powerful Test of Forecast Performance}

There are several standard (frequentist) hypothesis tests in the literature for obtaining evidence (or not) that an alternative set of forecasts for a times series of interest (for example produced using an optimally-weighted forecast combination) is more accurate than a benchmark set of forecasts (for example produced using the equally-weighted counterpart). These tests are often based on several technical assumptions, for example that the difference between the average out-of-sample losses for the models under consideration is asymptotically normal (e.g.\ \citealp{Diebold1995}; \citealp{West1996}; \citealp{Hansen2005}; \citealp{Giacomini2006}). 

A common theme throughout both Chapter \ref{chap:sampvar} and Chapter \ref{chap:puzzle} is that the asymptotic distributions of forecast performance measures can have nonstandard properties when applied to forecast combinations. In Chapter \ref{chap:sampvar}, it is shown that the asymptotic distributions for the forecast performance of both the one- and two-stage combinations are unusual, with the former being nonnormal and the latter having its variability come entirely from estimation uncertainty in the constituent model parameters (with no contribution from estimation uncertainty in the weights). This is revealed to have significant implications for the properties of hypothesis tests regarding the relative performance of competing forecast combinations in Chapter \ref{chap:puzzle}, with some tests having no local power and lacking size control, which is unusual.

Given these results, it is possible that some of the regularity conditions underlying tests of forecast performance may be violated when applying such tests to competing forecast combinations. In this case, it would be valuable to know which regularity conditions no longer hold, and why these conditions are violated. If the out-of-sample loss difference between two competing forecast combinations converges to a nonnormal asymptotic distribution, for example, then it might be possible to construct a more powerful test of forecast performance, with size control, that is better able to detect differences in the performance of competing forecast combinations. 

\section{Bootstrap Methods under Misspecification}

In its most general sense, a bootstrap method exploits an estimate of the data generating process (DGP) in the process of estimating the sampling distribution of a statistic of interest. In showing that a bootstrap-based estimate of a sampling distribution converges to the true sampling distribution as the sample size grows, we often assume that the bootstrap DGP converges (almost surely or in probability) to the true DGP in the sense of weak convergence. For an example in the case of the empirical bootstrap for i.i.d.\ processes, see Theorems 3.6.1 and 3.6.2 of \cite{Vaart1996}.

In Chapter \ref{chap:boot}, we took as our bootstrap DGP the maximiser of the likelihood over a parametric class of stochastic processes. If the true DGP is not in this parametric class, then in general the bootstrap DGP cannot converge to the true DGP. To obtain the validity of the parametric bootstrap we must therefore assume that our model is correctly specified, or in other words that the true DGP is in our chosen parametric class. This is an awkward state of affairs when applying the parametric bootstrap to forecast combinations, which are motivated in large part by the opposite assumption: that we cannot hope to construct a correctly specified parametric model for most empirical phenomena, and that such models are at best an approximation of reality. We discuss the factors motivating the adoption of forecast combinations in Chapter \ref{chap:background}, Section \ref{sec:backforecomb}.

Further research on applying bootstrap-based methods to forecast combinations can avoid assuming a correctly specified model by choosing a bootstrap that is of a nonparametric nature. It is important that the chosen method should account for the serial dependence that is characteristic of time-series processes; bootstrap methods for independent data can be inconsistent when applied to time-series data (see Remark 2.1 of \citealp{Singh1981}). Nonparametric bootstrap methods for dependent processes include the autoregressive sieve bootstrap (\citealp{Kreiss1992}; \citealp{Poskitt2008}; \citealp{Kreiss2011a}; \citealp{Poskitt2015}), the block bootstrap (e.g.\ \citealp{Andrews2002}; \citealp{Dudek2014}), the dependent wild bootstrap (\citealp{Shao2010}), the time frequency toggle bootstrap (\citealp{Kirch2011}), and others; see \cite{Kreiss2011} and \cite{Kreiss2012} for reviews. These methods differ in the strength of the assumptions that they impose on the data generating process, and investigation of the suitability of alternative methods in the context of estimated forecast combinations would be an interesting avenue to pursue in the future.

\bibliographystyle{apalike}
\bibliography{library}

\setcounter{chapter}{1}
\chapter{Background} \label{chap:background}
Headings included for reference purposes.
\section{Forecast Combinations} \label{sec:backforecomb}
\subsection{Motivation} \label{subsec:forecombmotiv}
\chapter{The Impact of Sampling Variability on Estimated Combinations of Distributional Forecasts} \label{chap:sampvar}
\chapter{Solving the Forecast Combination Puzzle} \label{chap:puzzle}
\chapter{The Parametric Bootstrap and the Distributional Forecast Combination Puzzle} \label{chap:boot}

\end{document}
