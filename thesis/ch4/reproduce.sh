#!/bin/bash

set -e

Rscript --vanilla smith_plot.R
(cd figure ; ./tex_to_pdf.sh)

