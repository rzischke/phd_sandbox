# This script does not depend on files produced by other scripts,
# and produces results.RData.

library(tidyverse)
library(tikzDevice)
library(lemon)

dbw <- new.env()
source(file = "dgp_by_weight.R", local = dbw)

evaluation_size <- 0.5
sample_sizes <- unique(as.integer(round(exp(seq(from = log(30), to = log(2000), length.out = 80)))))
# sample_sizes <- unique(as.integer(round(exp(seq(from = log(30), to = log(2000), length.out = 10)))))
num_power_curve_samples <- 5000
# num_power_curve_samples <- 50
num_burn_in_draws <- 5000
opt_sample_size <- 1e7
# opt_sample_size <- 1e4
test_size <- 0.05
ci_conf <- 0.95
max_sample_size <- max(sample_sizes)
sample_sizes_descending <- sort(sample_sizes, decreasing = TRUE)

weight_true <- c(0, 0.25, 0.5, 0.75, 1)
weight_null_constant <- c(0, 0.25, 0.5, 0.75, 1)
weight_null_drifting <- c(0.5, 1)
drifting_factor <- c(1, -1)

weight_true_str <- paste0("$\\eta^{\\star} = ", weight_true, "$")

losses = list(
  "MSE" = list(
    "dgp_by_weight" = dbw$dgp_by_mse_weight,
    "const1_param" = function(const1_param_guess, obs) { dbw$get_coef_mse(const1_param_guess, 1, 2, obs) },
    "const2_param" = function(const2_param_guess, obs) { dbw$get_coef_mse(const2_param_guess, 2, 2, obs) },
    "weight" = dbw$get_weight_mse,
    "one_stage_params" = dbw$get_one_stage_params_mse,
    "const1_loss" = function(const1_param, obs) { dbw$get_mse_const(const1_param, 1, 2, obs) },
    "const1_loss_deriv" = function(const1_param, obs) { dbw$get_mse_const_deriv_wrt_coef(const1_param, 1, 2, obs) },
    "const2_loss" = function(const2_param, obs) { dbw$get_mse_const(const2_param, 2, 2, obs) },
    "const2_loss_deriv" = function(const2_param, obs) { dbw$get_mse_const_deriv_wrt_coef(const2_param, 2, 2, obs) },
    "comb_loss" = dbw$get_mse_comb,
    "comb_loss_deriv_wrt_const1_param" = dbw$get_mse_comb_deriv_wrt_coef1,
    "comb_loss_deriv_wrt_const2_param" = dbw$get_mse_comb_deriv_wrt_coef2,
    "comb_loss_deriv_wrt_weight" = dbw$get_mse_comb_deriv_wrt_weight
  ),
  "Log Loss" = list(
    "dgp_by_weight" = dbw$dgp_by_log_loss_weight,
    "const1_param" = function(const1_param_guess, obs) { dbw$get_coef_log_loss(const1_param_guess, 1, 2, obs) },
    "const2_param" = function(const2_param_guess, obs) { dbw$get_coef_log_loss(const2_param_guess, 2, 2, obs) },
    "weight" = dbw$get_weight_log_loss,
    "one_stage_params" = dbw$get_one_stage_params_log_loss,
    "const1_loss" = function(const1_param, obs) { dbw$get_log_loss_const(const1_param, 1, 2, obs) },
    "const1_loss_deriv" = function(const1_param, obs) { dbw$get_log_loss_const(dbw$get_mse_const_deriv_wrt_const(const1_param, 1, 2, obs)) },
    "const2_loss" = function(const2_param, obs) { dbw$get_log_loss_const(const2_param, 2, 2, obs) },
    "const2_loss_deriv" = function(const2_param, obs) { dbw$get_log_loss_const(dbw$get_mse_const_deriv_wrt_const(const2_param, 2, 2, obs)) },
    "comb_loss" = dbw$get_log_loss_comb,
    "comb_loss_deriv_wrt_const1_param" = dbw$get_log_loss_comb_deriv_wrt_coef1,
    "comb_loss_deriv_wrt_const2_param" = dbw$get_log_loss_comb_deriv_wrt_coef2,
    "comb_loss_deriv_wrt_weight" = dbw$get_log_loss_comb_deriv_wrt_weight_easy
  )
)

qs <- function(x) {
  # Quadratic Spectrum Kernel, see Okui2010 Section 4.1.
  # Does not work for x == 0, in this case we ought to have qs(x) == 1,
  # but the term sin(0)/0 here gives NaN.
  u <- 1.2*pi*x
  return((3/u^2)*(sin(u)/u - cos(u)))
}

var_okui2010 <- function(y) {
  # Estimates the variance of the mean of a univariate time-series y
  # using the method in Okui2010 Section 4.1 with S = sqrt(T).
  n <- length(y)
  S <- ceiling(sqrt(n))
  acf_y <- acf(y, lag.max = n-1, type = "covariance", plot = FALSE)$acf[,1,1]
  j <- 1:(length(acf_y)-1)
  sample_variance <- acf_y[1] + 2*sum(qs(j/S)*acf_y[j+1])
  return(sample_variance)
}

save_file <- "results.RData"

if (file.exists(save_file) && (!exists("save_file_loaded") || is.null(save_file_loaded) || !save_file_loaded)) {
  load(save_file)
}

results_need_saving <- FALSE

if (!exists("results_detail") || is.null(results_detail)) {
  length_sample_size_iteration <- 2*(length(weight_null_constant) + length(weight_null_drifting)) + 1
  length_weight_true_iteration <- num_power_curve_samples*length(sample_sizes)*length_sample_size_iteration
  length_loss_iteration <- length(weight_true)*length_weight_true_iteration
  nrow_results_detail <- length(losses)*length_loss_iteration
  
  results_detail <- list(
    loss_name = rep(NA_character_, nrow_results_detail),
    num_steps = rep(NA_integer_, nrow_results_detail),
    null_type = rep(NA_character_, nrow_results_detail),
    sample_size = rep(NA_integer_, nrow_results_detail),
    train_sample_size = rep(NA_integer_, nrow_results_detail),
    test_sample_size = rep(NA_integer_, nrow_results_detail),
    dgp_ar1_param = rep(NA_real_, nrow_results_detail),
    dgp_ar2_param = rep(NA_real_, nrow_results_detail),
    weight_true = rep(NA_real_, nrow_results_detail),
    const1_param_true = rep(NA_real_, nrow_results_detail),
    const2_param_true = rep(NA_real_, nrow_results_detail),
    weight_est = rep(NA_real_, nrow_results_detail),
    const1_param_est = rep(NA_real_, nrow_results_detail),
    const2_param_est = rep(NA_real_, nrow_results_detail),
    weight_null_base = rep(NA_real_, nrow_results_detail),
    weight_null_drift = rep(NA_real_, nrow_results_detail),
    avg_loss_est = rep(NA_real_, nrow_results_detail),
    avg_loss_null = rep(NA_real_, nrow_results_detail),
    avg_loss_diff = rep(NA_real_, nrow_results_detail), # est vs null
    avg_loss_est_asy_var = rep(NA_real_, nrow_results_detail),
    avg_loss_null_asy_var = rep(NA_real_, nrow_results_detail),
    avg_loss_diff_asy_var = rep(NA_real_, nrow_results_detail)
  )
  
  iid_standard_normal_draws_opt <- rnorm(opt_sample_size)
  
  start_time <- Sys.time()
  print_progress <- function(i) {
    if ((1000*i) %% nrow_results_detail < 1000) {
      percent_complete <- round(1000*i/nrow_results_detail)/10
      now <- Sys.time()
      seconds_remaining <- (nrow_results_detail-i)*as.numeric(difftime(now, start_time, units = "secs"))/i
      eta <- now + seconds_remaining
      hours_remaining <- seconds_remaining/(60*60)
      cat(paste0(percent_complete, "% complete, ", round(100*hours_remaining)/100, " hours remaining, ETA ", eta, "\n"))
    }
  }
  
  i <- as.integer(1)
  for (loss_idx in 1:length(losses)) {
    loss_name <- names(losses)[loss_idx]
    results_detail$loss_name[i:(i-1+length_loss_iteration)] <- loss_name
    fit <- losses[[loss_idx]]
    
    params_by_steps <- function(steps, weight_guess, const1_param_guess, const2_param_guess, obs) {
      if (steps == 1) {
        return(fit$one_stage_params(c(weight_guess, const1_param_guess, const2_param_guess), obs))
      } else if (steps == 2) {
        const1_param <- fit$const1_param(const1_param_guess, obs)
        const2_param <- fit$const2_param(const2_param_guess, obs)
        weight <- fit$weight(weight_guess, const1_param, const2_param, obs)
        return(c(weight, const1_param, const2_param))
      } else {
        return(rep(NA_real_, 3))
      }
    }
    
    for (weight_true_idx in 1:length(weight_true)) {
      weight_true_i <- weight_true[weight_true_idx]
      dgp <- fit$dgp_by_weight(weight_true_i, iid_standard_normal_draws_opt)
      dgp_ar1_param <- dgp[1]
      dgp_ar2_param <- dgp[2]
      const1_param_true <- dgp[4]
      const2_param_true <- dgp[5]
      dgp_draws_opt <- dbw$get_dgp_draws(dgp_ar1_param, dgp_ar2_param, iid_standard_normal_draws_opt)
      
      this_iteration_indices <- i:(i-1+length_weight_true_iteration)
      results_detail$dgp_ar1_param[this_iteration_indices] <- dgp_ar1_param
      results_detail$dgp_ar2_param[this_iteration_indices] <- dgp_ar2_param
      results_detail$const1_param_true[this_iteration_indices] <- const1_param_true
      results_detail$const2_param_true[this_iteration_indices] <- const2_param_true
      results_detail$weight_true[this_iteration_indices] <- weight_true_i
      
      for (curve in 1:num_power_curve_samples) {
        iid_standard_normal_draws_est <- rnorm(num_burn_in_draws + max_sample_size)
        dgp_draws <- dbw$get_dgp_draws(dgp_ar1_param, dgp_ar2_param, iid_standard_normal_draws_est)[(num_burn_in_draws+1):(num_burn_in_draws+max_sample_size)]
        last_weight_est <- rep(weight_true_i, 2)
        last_const1_param_est <- rep(const1_param_true, 2)
        last_const2_param_est <- rep(const2_param_true, 2)
        loss_true_all <- fit$comb_loss(weight_true_i, const1_param_true, const2_param_true, dgp_draws)
        for (n in sample_sizes_descending) {
          train_sample_size <- ceiling((1-evaluation_size)*n)
          test_sample_size <- n - train_sample_size
          dgp_draws_train <- dgp_draws[1:train_sample_size]
          dgp_draws_test <- dgp_draws[(train_sample_size+1-2):n]
          loss_true <- loss_true_all[(train_sample_size-2+1):(n-2)]
          weight_null_base <- c(weight_null_constant, weight_null_drifting)
          weight_null_drift <- c(rep(0, length(weight_null_constant)), drifting_factor/sqrt(n))
          
          this_iteration_indices <- i:(i-1+length_sample_size_iteration)
          results_detail$sample_size[this_iteration_indices] <- n
          results_detail$train_sample_size[this_iteration_indices] <- train_sample_size
          results_detail$test_sample_size[this_iteration_indices] <- test_sample_size
          
          for (num_steps in c(2,1)) {
            params_est <- params_by_steps(num_steps, last_weight_est[num_steps], last_const1_param_est[num_steps], last_const2_param_est[num_steps], dgp_draws_train)
            weight_est <- params_est[1]
            const1_param_est <- params_est[2]
            const2_param_est <- params_est[3]
            
            last_weight_est[num_steps] <- weight_est
            last_const1_param_est[num_steps] <- const1_param_est
            last_const2_param_est[num_steps] <- const2_param_est
            
            loss_est <- fit$comb_loss(weight_est, const1_param_est, const2_param_est, dgp_draws[(train_sample_size+1-2):n])[1:test_sample_size]
            
            this_iteration_indices <- i:(i-1+length(weight_null_base))
            results_detail$num_steps[this_iteration_indices] <- as.integer(num_steps)
            results_detail$weight_est[this_iteration_indices] <- weight_est
            results_detail$const1_param_est[this_iteration_indices] <- const1_param_est
            results_detail$const2_param_est[this_iteration_indices] <- const2_param_est
            results_detail$avg_loss_est[this_iteration_indices] <- mean(loss_est)
            results_detail$avg_loss_est_asy_var[this_iteration_indices] <- var_okui2010(loss_est)
            
            results_detail$null_type[i:(i-1+length(weight_null_constant))] <- "Constant"
            results_detail$null_type[(i+length(weight_null_constant)):(i-1+length(weight_null_base))] <- "Drifting"
            
            for (weight_null_idx in 1:length(weight_null_base)) {
              weight_null <- weight_null_base[weight_null_idx] + weight_null_drift[weight_null_idx]
              loss_null <- fit$comb_loss(weight_null, last_const1_param_est[2], last_const2_param_est[2], dgp_draws_test)[1:test_sample_size]
              loss_diff <- loss_null - loss_est
              
              results_detail$weight_null_base[i] <- weight_null_base[weight_null_idx]
              results_detail$weight_null_drift[i] <- weight_null_drift[weight_null_idx]
              results_detail$avg_loss_null[i] <- mean(loss_null)
              results_detail$avg_loss_null_asy_var[i] <- var_okui2010(loss_null)
              results_detail$avg_loss_diff[i] <- mean(loss_diff)
              results_detail$avg_loss_diff_asy_var[i] <- var_okui2010(loss_diff)
              
              print_progress(i)
              i <- i + as.integer(1)
            }
          }
          
          # Benchmark two-step against one-step alternative.
          
          loss_null <- fit$comb_loss(last_weight_est[2], last_const1_param_est[2], last_const2_param_est[2], dgp_draws_test)[1:test_sample_size]
          loss_est <- fit$comb_loss(last_weight_est[1], last_const1_param_est[1], last_const2_param_est[1], dgp_draws_test)[1:test_sample_size]
          loss_diff <- loss_null - loss_est
          
          results_detail$null_type[i] <- "TwoStep"
          results_detail$avg_loss_null[i] <- mean(loss_null)
          results_detail$avg_loss_null_asy_var[i] <- var_okui2010(loss_null)
          results_detail$avg_loss_est[i] <- mean(loss_est)
          results_detail$avg_loss_est_asy_var[i] <- var_okui2010(loss_est)
          results_detail$avg_loss_diff[i] <- mean(loss_diff)
          results_detail$avg_loss_diff_asy_var[i] <- var_okui2010(loss_diff)
          
          print_progress(i)
          i <- i + as.integer(1)
        }
      }
    }
  }
  
  print(Sys.time() - start_time)
  
  results_detail <- as_tibble(results_detail) %>%
    arrange(
      loss_name,
      num_steps,
      weight_true,
      null_type,
      weight_null_base,
      sample_size
    )
  
  results_need_saving <- TRUE
  
}

if (!exists("power_results") || is.null(power_results)) {
  power_results <<- results_detail %>%
    mutate(null_rejected = case_when(
      avg_loss_diff_asy_var == 0.0 ~ avg_loss_diff > 0.0,
      TRUE ~ (pnorm(avg_loss_diff, sd = sqrt(avg_loss_diff_asy_var/test_sample_size), lower.tail = FALSE) < test_size)
    )) %>%
    group_by(loss_name, num_steps, null_type, sample_size, weight_true, weight_null_base, weight_null_drift) %>%
    summarise(rejection_frequency = mean(null_rejected), .groups = "drop") %>%
    mutate(half_width = rejection_frequency*(1 - rejection_frequency)*qnorm(0.5*(1+ci_conf))/sqrt(num_power_curve_samples)) %>%
    mutate(
      rejection_frequency_ci_low = pmax(rejection_frequency - half_width, 0),
      rejection_frequency_ci_high = pmin(rejection_frequency + half_width, 1)
    ) %>%
    select(
      loss_name,
      num_steps,
      null_type,
      weight_true,
      sample_size,
      weight_null_base,
      weight_null_drift,
      rejection_frequency,
      rejection_frequency_ci_low,
      rejection_frequency_ci_high
    ) %>%
    arrange(
      loss_name,
      null_type,
      weight_true,
      sample_size,
      weight_null_base
    )
  results_need_saving <- TRUE
}

if (results_need_saving) {
  save_file_loaded <<- TRUE
  save(
    save_file_loaded,
    results_detail,
    power_results,
    file = save_file
  )
}
