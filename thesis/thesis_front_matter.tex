\documentclass[12pt]{report}
\usepackage[a4paper, margin=1in]{geometry}
\usepackage{graphicx}
\usepackage{setspace}
\usepackage[hidelinks]{hyperref}
\usepackage{natbib}
\usepackage[nottoc]{tocbibind}
\usepackage{mathtools}
\usepackage{mathrsfs}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{colortbl}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{tabularx}
\usepackage{enumitem}
\usepackage{etoolbox}

\apptocmd{\thebibliography}{\raggedright}{}{}

% The following two lines stop scientific word from placing the contents before the title page.
\let\toc\tableofcontents
\renewcommand{\tableofcontents}{}

\newcommand{\argmin}{\operatornamewithlimits{argmin\,}}
\newcommand{\argmax}{\operatornamewithlimits{argmax\,}}
\newcommand{\plim}{\operatornamewithlimits{plim\,}}
\newcommand{\E}{\mathbb{E}}

\theoremstyle{definition}
\newtheorem{definition}{Definition}[chapter]
\newtheorem{example}{Example}[chapter]

\theoremstyle{remark}
\newtheorem{remark}{Remark}[chapter]

\theoremstyle{plain}
\newtheorem{proposition}{Proposition}[chapter]
\newtheorem{corollary}{Corollary}[chapter]
\newtheorem{lemma}{Lemma}[chapter]
\newtheorem{theorem}{Theorem}[chapter]
\newtheorem{conjecture}{Conjecture}[chapter]
\newtheorem{assumption}{Assumption}[chapter]

\definecolor{Gray}{gray}{0.9}

\setcounter{secnumdepth}{5}

\begin{document}

\begin{titlepage}
\centering
\includegraphics[width=\textwidth]{front/figure/monash-logo.jpg}\par
\vspace{10\baselineskip}
{\Large
	\textbf{Sampling Variability and Estimated Forecast Combinations} \\
	Ryan Alexander Zischke \\
	BE(ECS)(Hons),  BCom
\par}
\vspace{12\baselineskip}
{\large
	A thesis submitted for the degree of Doctor of Philosophy at \\
	Monash University in 2022 \\
	Department of Econometrics and Business Statistics
\par}

\end{titlepage}

\pagenumbering{roman}

\newcount\oldhbadness
\oldhbadness=\hbadness

\doublespacing

\section*{Copyright Notice}

\hbadness=10000

\copyright\ Ryan Zischke 2022. \\

\hbadness=\oldhbadness

\noindent I certify that I have made all reasonable efforts to secure copyright permissions for third-party content included in this thesis and have not knowingly added copyright content to my work without the owner's permission.

\newpage

\section*{Abstract}

In this thesis, we apply several techniques in asymptotic statistics to characterise the impact of sampling variability on forecast combinations produced by optimising a criterion function that rewards predictive accuracy. By optimising the weights together with the constituent model parameters in one step, within a single optimisation program, we obtain a measure of forecast performance that converges at a faster rate and to a better limit, relative to the standard approach of optimising the constituent model parameters before the weights in two steps, using separate optimisation programs. For large samples, almost all sampling variability in the performance of two-step combinations is shown to originate from the parameter estimates of the constituent models, with the contribution from the estimated weights vanishing with increasing sample size. As a result, we find that one reason why hypothesis tests of relative forecast performance often fail to reject equally-weighted combinations in favour of their optimally-weighted two-step counterparts -- a phenomenon called the `forecast combination puzzle' -- is because such tests lack size control and have low power for sample sizes commonly observed in practice. Power is increased and the puzzle is avoided by optimising parameters in one step rather than two. Finally, we show how the parametric bootstrap might be used to estimate the sampling distribution of forecast performance, and to account for sampling variability in the construction of forecast performance measures.

\newpage

\section*{Declaration}

\hbadness=10000

\noindent This thesis is an original work of my research and contains no material which has been accepted for the award of any other degree or diploma at any university or equivalent institution and that, to the best of my knowledge and belief, this thesis contains no material previously published or written by another person, except where due reference is made in the text of the thesis. \\

\noindent Signature: \IfFileExists{front/figure/signature.pdf}{\includegraphics[trim=0 {0.4\baselineskip} 0 {0.4\baselineskip}]{front/figure/signature.pdf}}{}

\noindent Print Name: \IfFileExists{front/figure/print_name.pdf}{\includegraphics[trim=0 {0.5\baselineskip} 0 {0.5\baselineskip}]{front/figure/print_name.pdf}}{}

\noindent Date: \IfFileExists{front/figure/date.pdf}{\includegraphics[trim = 0 {0.4\baselineskip} 0 {0.4\baselineskip}]{front/figure/date.pdf}}{}

\hbadness=\oldhbadness

\newpage

\section*{Publications During Enrolment}

Chapter 3 is currently under review at the Journal of Business \& Economic Statistics, having been submitted on the 6\textsuperscript{th} of June, 2022.

\newpage

\section*{Acknowledgements}

This research was supported by a Monash Business School Graduate Research Scholarship, and by Australian Research Council (ARC) Discovery Grant DP200101414. Thank you to the Milestone Review Panel, consisting of Rob Hyndman, Mervyn Silvapulle and Farshid Vahid-Araghi, for their thoughtful and pertinent comments throughout the PhD. Eric Eisenstat was the discussant for a presentation of an earlier version of Chapter \ref{chap:sampvar} at the 34\textsuperscript{th} PhD Conference in Economics \& Business, and his detailed and comprehensive review led to a substantial improvement of the chapter. Finally, thank you to PhD supervisors Gael Martin, David Frazier and Donald Poskitt for their ongoing support, enduring guidance, sage advice, collegial collaboration, timely feedback and critical revision, on all aspects of the thesis and throughout the entire duration of the PhD.

\toc

\chapter{Introduction}

\pagenumbering{arabic}

Headings included so that they may appear in the table of contents. Now we cite \cite{Aastveit2019} so that a bibliography is generated.

\section{Preamble}
\section{Thesis Overview}
\chapter{Background} \label{chap:background}
\section{Forecast Combinations} \label{sec:backforecomb}
\subsection{Motivation} \label{subsec:forecombmotiv}
\subsection{Point Forecast Combinations} \label{subsec:intropointcomb}
\subsection{Distributional Forecast Combinations}
\subsection{Forecast Combination Puzzle} \label{subsec:intropuzzle}
\section{Measuring Forecast Performance}
\subsection{Evaluating Point Forecasts} \label{subsec:introevalpointcast}
\subsection{Evaluating Distributional Forecasts} \label{subsec:introevaldistcast}
\section{Large Sample Asymptotics} \label{sec:introasy}
\chapter{The Impact of Sampling Variability on Estimated Combinations of Distributional Forecasts} \label{chap:sampvar}
\section{Introduction}
\section[Accuracy of Distributional Forecast Combinations]{Accuracy of Distributional Forecast\\Combinations\label{sec:accuracy}}
\subsection{Scoring Rules\label{subsec:score}}
\subsection{Defining Distributional Forecast Combinations\label{subsec:comb}}
\subsection{Producing Forecast Combinations\label{subsec:produce}}
\subsection{Assessing Accuracy of Forecast Combinations\label{subsec:assess}}
\section{Implications for Forecast Performance\label{sec:implications}}
\section{Monte Carlo Analysis\label{sec:montecarlo}}
\subsection{DGP, Forecast Combination and Scoring Rules\label{subsec:montecarlodgp}}
\subsection{Simulation Design}
\subsection{Results \label{subsec:montecarloresults}}
\section{Performance on S\&P500 Returns\label{sec:emp}}
\subsection{Data, Forecast Combination and Scoring Rules\label{subsec:empdatacomb}}
\subsection{Results \label{subsec:empresults}}
\section{Conclusion\label{sec:conclusion}}
\section{Appendix}
\subsection{Regularity Conditions\label{subsec:regularity}}
\subsection{GMM Representation of One- and Two-Stage Forecast Combinations\label{subsec:gmm}}
\subsection{Structure of the Asymptotic Covariance Matrix of Two-Stage Forecast Combination Parameter Estimates\label{subsec:gmmasycov}}
\subsection{Proofs\label{subsec:proofs}}
\subsection{Monte Carlo Analysis: Results by Scoring Rule\label{subsec:montecarlobyscore}}
\subsection{Performance on S\&P500 Returns: Results by Scoring Rule\label{subsec:empbyscore}}
\chapter{Solving the Forecast Combination Puzzle} \label{chap:puzzle}
\section{Introduction}
\section{A Brief Motivating Example}\label{sec:motiv}
\subsection{Revisiting Smith and Wallis (2009)}
\subsection{Extending the Findings of Smith and Wallis (2009)} \label{subsec:extsmith}
\section{Measuring the Accuracy of `Optimal' Forecast Combinations} \label{sec:acc}
\subsection{Defining `Optimal' Forecast Combinations} \label{subsec:defoptcomb}
\subsubsection{Consistent Scoring Functions}
\subsubsection{Proper Scoring Rules} \label{subsubsec:propriety}
\subsubsection{Estimating Forecast Combinations} \label{subsubsec:estforecomb}
\subsection{Testing the Accuracy of Forecast Combinations}
\subsubsection{Evaluation Scheme}
\subsubsection{Tests of Forecasting Accuracy} \label{subsubsec:tests}
\section{Solving the Forecasting Combination Puzzle}\label{sec:puzzle}
\subsection{Cause of the puzzle}
\subsection{Avoiding the puzzle} \label{subsec:avoid}
\subsection{Revisiting Smith and Wallis (2009) for one- and two-step combinations} \label{subsec:smithone}
\section{Conclusion}
\section{Appendix}
\subsection{Assumptions and Proofs}
\subsubsection{Assumptions and Discussion}\label{app:A}
\subsubsection{Proofs of Main Results}\label{app:proofs}
\subsection{Proofs of Key Lemmas}
\subsection{Implementations Details for Extending the Findings of Smith and Wallis (2009)} \label{subsec:nidextsmith}
\subsubsection{Obtaining the DGP Parameters} \label{subsubsec:dgpparams}
\subsubsection{Estimating Rejection Frequency} \label{subsubsec:estrejfreq}
\subsection{Implementation Details for Cause of the Puzzle} \label{subsec:nidpuzzlecause}
\chapter{The Parametric Bootstrap and the Distributional Forecast Combination Puzzle} \label{chap:boot}
\section{Preamble}
\section{Introduction}
\section{Notation} \label{sec:notation}
\section{The Linear Pool of Distributional Forecasts} \label{sec:distpool}
\section[The Parametric Bootstrap of the Maximum Likelihood Estimator]{The Parametric Bootstrap of the Maximum\\Likelihood Estimator} \label{sec:boot}
\section{Measuring Forecast Combination Performance} \label{sec:perf}
\section[Bootstrapping Forecast Combination Performance]{Bootstrapping Forecast Combination\\Performance} \label{sec:perfboot}
\section{Sample Size and the Forecast Combination Puzzle} \label{sec:fcp}
\section{Asymptotic Equivalence with Expected AIC} \label{sec:aic}
\section{Monte Carlo Simulation} \label{sec:monte}
\section{Conclusion} \label{sec:conclude}
\section{Appendix} \label{sec:bootadx}
\subsection{Proof of Theorem 6} \label{subsec:bootstrapasproof}
\subsection{Proof of Theorem 7} \label{subsec:bootstrapasklproof}
\subsection{Proof of Theorem 8} \label{subsec:thmssfcpproof}
\chapter{Promising Directions for Future Research} \label{chap:future}
\section[Sampling Variability and the Performance of Bayesian Forecast Combinations]{Sampling Variability and the Performance of\\Bayesian Forecast Combinations}
\section{A More Powerful Test of Forecast Performance}
\section{Bootstrap Methods under Misspecification}
\bibliographystyle{apalike}
\bibliography{library}

\end{document}
