\documentclass[12pt]{report}
\usepackage[a4paper, margin=1in]{geometry}
\usepackage{graphicx}
\usepackage{setspace}
\usepackage[hidelinks]{hyperref}
\usepackage{natbib}
\usepackage[nottoc]{tocbibind}
\usepackage{mathtools}
\usepackage{mathrsfs}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{colortbl}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{tabularx}
\usepackage{enumitem}
\usepackage{etoolbox}

\apptocmd{\thebibliography}{\raggedright}{}{}

% The following two lines stop scientific word from placing the contents before the title page.
\let\toc\tableofcontents
\renewcommand{\tableofcontents}{}

\newcommand{\argmin}{\operatornamewithlimits{argmin\,}}
\newcommand{\argmax}{\operatornamewithlimits{argmax\,}}
\newcommand{\plim}{\operatornamewithlimits{plim\,}}
\newcommand{\E}{\mathbb{E}}

\theoremstyle{definition}
\newtheorem{definition}{Definition}[chapter]
\newtheorem{example}{Example}[chapter]

\theoremstyle{remark}
\newtheorem{remark}{Remark}[chapter]

\theoremstyle{plain}
\newtheorem{proposition}{Proposition}[chapter]
\newtheorem{corollary}{Corollary}[chapter]
\newtheorem{lemma}{Lemma}[chapter]
\newtheorem{theorem}{Theorem}[chapter]
\newtheorem{conjecture}{Conjecture}[chapter]
\newtheorem{assumption}{Assumption}[chapter]

\definecolor{Gray}{gray}{0.9}

\setcounter{secnumdepth}{5}

\begin{document}

\doublespacing

\chapter{Introduction}

\pagenumbering{arabic}

\section{Preamble}

At the time of submission of this thesis, there appears to be a consensus that the technique of \textit{forecast combinations} -- that is, the weighted combination of forecasts from distinct models -- is one of the best approaches for producing accurate predictions of future outcomes. This consensus stems from a large body of empirical research demonstrating the superior performance of forecast combinations in out-of-sample evaluations across many different contexts comprising different datasets, models, forecasted phenomena and subject areas. Forecast combinations also obviate the need to commit to a particular theory or model when making forecasts, and provide the option of allowing emphasis to be placed on models in a combination according to the extent that they contribute to forecast performance in the presence of the other models. Because this contribution may vary across datasets, and even across random variables observed within the same dataset, the manner in which weights are assigned to the forecasts in a combination -- including the way in which those weights, and the other parameters that characterise the constituent models, are \textit{estimated} -- has been the subject of intense interest. Critically, the estimation of forecast combinations has typically occurred in a setting of misspecification; i.e.\ under the assumption that the combination does not encompass, or span, the true data generating process.

It is well known that the assumption of correct specification is crucial for many results concerning the optimality of maximum likelihood estimation. Without correct specification, maximising the likelihood is not necessarily the best way to estimate model parameters, and we can usually produce better forecasts -- including via forecast combinations -- by adopting a criterion function based on a bespoke measure of performance that is tailored to the problem at hand. This is the motivation underlying the growing literature on \textit{scoring functions} (for point forecasts) and \textit{scoring rules} (for distributional forecasts), which concerns the construction of these bespoke criteria and the mathematical properties necessary for them to work well. Typically, the choice of score depends on the forecasted quantity, and the decision, if any, to be informed by the set, or combination, of forecasts.

Alongside the literature on forecast combinations, scoring functions and scoring rules sit techniques in asymptotic statistics that also permit model misspecification. In particular, extremum estimation -- whereby parameter estimates minimise a random function of the parameters that is constructed from the data -- provides a means of interpreting estimated forecast combinations in the context of large sample approximations, which includes the production of asymptotically valid sampling distributions for estimated parameters and for other properties of the combination, such as forecast performance. While the dominant way to apply these sampling distributions has traditionally been to construct hypothesis tests and confidence intervals, they are of interest in their own right as complete (albeit approximate) descriptions of the sampling variability associated with the given estimated quantity.

This thesis synthesises advances in these three distinct areas -- forecast combinations, scoring functions and scoring rules, and large sample asymptotics -- to provide new insights into the way that sampling variability affects the stochastic properties of the performance of forecast combinations. These insights follow from the observation that forecast combinations produced according to scoring functions or scoring rules are amenable to large sample asymptotic analysis, including analysis of forecast performance. As a consequence, we also provide academic and professional forecasters with recommendations they can use to produce better performing forecasts, and thereby conduct more sound analyses and make better-informed decisions.

\section{Thesis Overview}

Chapter \ref{chap:background} covers the relevant background material. After arguing for the use of forecast combination over model selection, we review the literature on point and distributional forecast combinations, and the forecast combination puzzle, which is the empirical finding that predictions formed by combining multiple forecasts in ways that seek to optimise forecast performance often do not out-perform more naive, e.g.\ equally-weighted, approaches. Scoring functions and scoring rules are treated next with a focus on \textit{consistency} and \textit{propriety}, which are the key mathematical properties justifying the use of scoring functions and scoring rules, respectively. For the remainder of the background, we introduce the key concepts underlying large sample asymptotics, including the consistency and asymptotic normality of extremum estimators. We then define \textit{one-} and \textit{two-step} estimation, which are two ways of estimating the parameters of a forecast combination that we explore in subsequent chapters. The key feature of these estimators is that they acknowledge that the constituent models of a forecast combination are often subject to estimation error in practice, a fact not usually recognised in the forecast combinations literature.

In Chapter \ref{chap:sampvar} we investigate the performance and sampling variability of estimated forecast combinations, with particular attention given to the combination of forecast \textit{distributions}. Unknown parameters in the forecast combination are optimised according to criterion functions based on proper scoring rules, which are chosen to reward the form of forecast accuracy that matters for the problem at hand, and forecast performance is measured using the out-of-sample expectation of said scoring rule. Our results provide novel insights into the behaviour of estimated forecast combinations. Firstly, we show that, asymptotically, the sampling variability in the performance of standard forecast combinations is determined solely by estimation of the constituent models, with estimation of the combination weights contributing no sampling variability whatsoever, at first order. Secondly, we show that, if computationally feasible, forecast combinations produced in a single step -- in which the constituent model and combination function parameters are estimated jointly -- have superior predictive accuracy and lower sampling variability than standard forecast combinations -- where constituent model and combination function parameters are estimated in two steps. These theoretical insights are demonstrated numerically, both in simulation settings and in an extensive empirical illustration using a time series of S\&P500 returns.

In Chapter \ref{chap:puzzle} we demonstrate that the so-called forecast combination puzzle is a consequence of the methodologies commonly used to produce forecast combinations. In particular, we demonstrate that, due to the two-step manner in which such forecasts are typically produced, tests that aim to discriminate between the predictive accuracy of such competing combinations can have low power, and can lack size control, leading to an outcome that favours the simpler approach. In short, we show that this counter-intuitive result can be completely avoided by the adoption of more efficient, one-step estimation strategies in the production of the combinations. We illustrate these findings both in the context of forecasting a functional of interest and in terms of predictive densities.

Chapter \ref{chap:boot} is an exploration into how the parametric bootstrap might be used to estimate the sampling distribution of the asymptotic performance measure. It is shown that the relative expected performance of equally-weighted and optimally-weighted forecast combinations depends crucially on the sample size, whereby the equally-weighted combination outperforms for low sample sizes and the optimally-weighted combination outperforms for high sample sizes. The parametric bootstrap is proposed as a method that could be used to estimate, for any given sample size, which of the two combinations has a higher expected performance. Finally, we show that there is an asymptotic equivalence between our method for deciding between the two forecast combinations, and a decision rule derived from the Akaike information criterion (AIC).

In Chapter \ref{chap:future}, we conclude the thesis by briefly suggesting some promising directions for future research.

Each chapter of the thesis was written with publication in mind and each targets a slightly different audience. Consequently, these chapters are to be viewed as self-contained pieces of work. In contrast to a conventional PhD thesis, there will be repetition in the introductory material, references, equations and derivations. Finally, to match each chapter with its intended audience, mathematical notation and terminology may differ between chapters.

\chapter{Background} \label{chap:background}
Headings included for reference purposes.
\chapter{The Impact of Sampling Variability on Estimated Combinations of Distributional Forecasts} \label{chap:sampvar}
\chapter{Solving the Forecast Combination Puzzle} \label{chap:puzzle}
\chapter{The Parametric Bootstrap and the Distributional Forecast Combination Puzzle} \label{chap:boot}
\chapter{Promising Directions for Future Research} \label{chap:future}

\end{document}
