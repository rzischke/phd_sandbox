\documentclass[12pt]{article}

\setlength{\parskip}{\baselineskip}%
\setlength{\parindent}{0pt}%

\usepackage{mathtools, mathrsfs, amsmath, amsfonts, amssymb, amsthm, tensor, enumitem, float, booktabs, bm}
\usepackage[margin=2cm]{geometry}

\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{theorem}{Theorem}

\DeclareMathOperator*{\argmin}{\arg\!\min}
\DeclareMathOperator*{\argmax}{\arg\!\max}

\begin{document}

\title{Is Transition Size Equivalent to a Likelihood Ratio Test?}
\author{Ryan Zischke}

\maketitle

Hi Don,

In this reply I will address part of your first question: ``How does the above analysis differ, if at all, from what your own modelling framework would lead to?'' In particular, I will explore one such difference. I will continue to refer to these two methods as ``above analysis'' and ``my own framework''.

The above analysis prefers a model based on a function of the \textbf{log-likelihoods} of the encompassing and nested models. This value is a test-statistic in a hypothesis test, and we calculate the sample size needed for the probability of rejecting $H_0: \boldsymbol{\beta}_2 = \boldsymbol{0}$ to reach a desired threshold given a test size and the residual sum of squares of the two models. My own framework prefers a model based on a function of \textbf{expected log-likelihoods} of the encompassing and nested models. In this case, we choose the model which has the highest expected log-likelihood. We take the expectation with respect to unseen observations that are independent of our sample, and then substitute parameter estimates obtained with our sample. Whereas the above analysis chooses a model based on the distribution of an in-sample statistic, my own frameworks chooses a model based on the in-sample estimates of out-of-sample expectations.

A consequence is that these two methods select a model using thresholds of different statistics. These different statistics are not asymptotically equivalent.

Suppose that the rows of the design matrix are $\boldsymbol{X}^T = ( \boldsymbol{x}_1^T, \boldsymbol{x}_2^T, \cdots, \boldsymbol{x}_N^T)$, where $\boldsymbol{x}_i$ is $1 \times k$ and i.i.d. Also let $\boldsymbol{x}_i = (\boldsymbol{x}_{i,1} : \boldsymbol{x}_{i,2})$ for all $i$, where $\boldsymbol{x}_{i,1}$ is $1 \times k_1$ and $\boldsymbol{x}_{i,2}$ is $1 \times k_2$. Since $\boldsymbol{x}_i$ is i.i.d, we can refer to the distribution of $\boldsymbol{x}$, $\boldsymbol{x}_1$ and $\boldsymbol{x}_2$ without confusion.

All three processes introduced in the above analysis can be expressed as
\begin{equation}
Y_i = \mu_P(\boldsymbol{x}_i) + \epsilon_i,
\end{equation}
where $\epsilon_i \overset{i.i.d.}{\sim} N(0, \sigma_P^2)$, for various choices of $\sigma_P^2$ and $\mu_P$. Here, $P$ represents a measure, which in this case is identified by the function $\mu_P$ and the constant $\sigma_P^2$. For now, we will concern ourselves only with the first two models, $\hat{P}_{12}$ and $\hat{P}_1$:
\begin{align}
\mu_{\hat{P}_{12}}(\boldsymbol{x}_i) &= \boldsymbol{x}_{i,1} \boldsymbol{b}_{1.} + \boldsymbol{x}_{i,2} \boldsymbol{b}_{2.} + e_{12,i}, \\
\sigma_{\hat{P}_{12}}^2 &= \frac{1}{N} \sum_{i=1}^N (Y_i - \mu_{\hat{P}_{12}}(\boldsymbol{x}_i))^2, \\
\mu_{\hat{P}_1}(\boldsymbol{x}_i) &= \boldsymbol{x}_{i,1} \boldsymbol{b}_{1} + e_{1,i}, \\
\sigma_{\hat{P}_1}^2 &= \frac{1}{N} \sum_{i=1}^N (Y_i - \mu_{\hat{P}_1}(\boldsymbol{x}_i))^2.
\end{align}
As short hand, use the following: $\hat{\mu}_{12} = \mu_{\hat{P}_{12}}$, $\hat{\sigma}_{12}^2 = \sigma_{\hat{P}_{12}}^2$, $\hat{\mu}_1 = \mu_{\hat{P}_1}$, and $\hat{\sigma}_1^2 = \sigma_{\hat{P}_1}^2$. We chose these parameters by maximising the log-likelihood scoring rule:
\begin{equation}
S(P) = - \frac{1}{2} \ln (\sigma_P^2) - \frac{(Y - \mu_P(\boldsymbol{x}))^2}{2 \sigma_P^2},
\end{equation}
and we will also use this scoring rule to choose our preferred model. In the above analysis, we have:
\begin{align}
S(\hat{P}_{12}) &= - \frac{1}{2} \ln \hat{\sigma}_{12}^2 - \frac{1}{2}, \\
S(\hat{P}_1) &= - \frac{1}{2} \ln \hat{\sigma}_1^2 - \frac{1}{2}, \\
\therefore\ \exp \left\lbrace 2 \left( S(\hat{P}_{12}) - S(\hat{P}_1) \right) \right\rbrace &= \frac{\hat{\sigma}_1^2}{\hat{\sigma}_{12}^2} \\
&= \frac{k_2}{N-k} LR + 1,
\end{align}
where LR is defined in the above analysis. Let $Q$ be the measure of the DGP, and suppose that the encompassing model is correctly specified. Restrict $P$ to a linear model in $\boldsymbol{x}$, so that $\mu_P(\boldsymbol{x}) = \boldsymbol{x} \boldsymbol{\beta}_P$, where $\boldsymbol{\beta}_P$ is $k \times 1$. Then the expected likelihood is
\begin{equation}
\mathbb{E}_Q[S(P)] = - \frac{1}{2} \ln \sigma_P^2 - \frac{\boldsymbol{\beta}_P^T \mathrm{Var}(\boldsymbol{x}) \boldsymbol{\beta}_P + ( \mathbb{E}[\boldsymbol{x}](\boldsymbol{\beta}_P - \boldsymbol{\beta}_{12}))^2}{2 \sigma_P^2}.
\end{equation}
In my own framework, we have:
\begin{align}
\mathbb{E}_Q[S(\hat{P}_{12}) \mid \hat{P}_{12}] &= -\frac{1}{2} \ln \hat{\sigma}_{12}^2 - \frac{\boldsymbol{b}_{12}^T \mathrm{Var}(\boldsymbol{x}) \boldsymbol{b}_{12} + (\mathbb{E}[\boldsymbol{x}] (\boldsymbol{b}_{12} - \boldsymbol{\beta}_{12}))^2}{2 \hat{\sigma}_{12}^2}, \label{eqn:lr1} \\
\mathbb{E}_Q[S(\hat{P}_1) \mid \hat{P}_1] &= - \frac{1}{2} \ln \hat{\sigma}_1^2 - \frac{\boldsymbol{b}_1^T \mathrm{Var}(\boldsymbol{x}_1) \boldsymbol{b}_1 + \left( \mathbb{E}[\boldsymbol{x}_1] (\boldsymbol{b}_1 - \boldsymbol{\beta}_1 ) - \mathbb{E}[\boldsymbol{x}_2] \boldsymbol{\beta}_2 \right)^2}{2 \hat{\sigma}_1^2},
\end{align}
\begin{align}
\therefore\ \exp & \left \lbrace 2 \left( \mathbb{E}[S(\hat{P}_{12}) \mid \hat{P}_{12}] - \mathbb{E}[S(\hat{P}_1) \mid \hat{P}_1] \right) \right \rbrace =  \\
&\exp \left\lbrace  2 (S(\hat{P}_{12}) - S(\hat{P}_{1})) \right\rbrace \\
&\times \exp \left\lbrace \frac{\boldsymbol{b}_1^T \mathrm{Var}(\boldsymbol{x}_1) \boldsymbol{b}_1 + \left( \mathbb{E}[\boldsymbol{x}_1] (\boldsymbol{b}_1 - \boldsymbol{\beta}_1 ) - \mathbb{E}[\boldsymbol{x}_2] \boldsymbol{\beta}_2 \right)^2}{2 \hat{\sigma}_1^2} \right\rbrace \\
&\times \exp \left\lbrace \frac{\boldsymbol{b}_{12}^T \mathrm{Var}(\boldsymbol{x}) \boldsymbol{b}_{12} + (\mathbb{E}[\boldsymbol{x}] (\boldsymbol{b}_{12} - \boldsymbol{\beta}_{12}))^2}{2 \hat{\sigma}_{12}^2} \right\rbrace,
\end{align}
\begin{align}
\therefore\ \exp & \left \lbrace 2 \left( \mathbb{E}[S(\hat{P}_{12}) \mid \hat{P}_{12}] - \mathbb{E}[S(\hat{P}_1) \mid \hat{P}_1] \right) \right \rbrace =  \\
&\left( \frac{k_2}{N-k} LR + 1 \right) \times \exp \left\lbrace \frac{\boldsymbol{b}_1^T \mathrm{Var}(\boldsymbol{x}_1) \boldsymbol{b}_1 + \left( \mathbb{E}[\boldsymbol{x}_1] (\boldsymbol{b}_1 - \boldsymbol{\beta}_1 ) - \mathbb{E}[\boldsymbol{x}_2] \boldsymbol{\beta}_2 \right)^2}{2 \hat{\sigma}_1^2} \right\rbrace \label{eqn:lr2} \\
&\times \exp \left\lbrace \frac{\boldsymbol{b}_{12}^T \mathrm{Var}(\boldsymbol{x}) \boldsymbol{b}_{12} + (\mathbb{E}[\boldsymbol{x}] (\boldsymbol{b}_{12} - \boldsymbol{\beta}_{12}))^2}{2 \hat{\sigma}_{12}^2} \right\rbrace.
\end{align}
Therefore, in general,
\begin{equation}
\exp \left\lbrace 2 \left( S(\hat{P}_{12}) - S(\hat{P}_1) \right) \right\rbrace \neq \exp \left \lbrace 2 \left( \mathbb{E}[S(\hat{P}_{12}) \mid \hat{P}_{12}] - \mathbb{E}[S(\hat{P}_1) \mid \hat{P}_1] \right) \right \rbrace.
\end{equation}
Since the term in the exponent on line \ref{eqn:lr1} (which is equal to log of the first term on line \ref{eqn:lr2}) is a likelihood ratio statistic, it converges in distribution to a chi-squared distribution as $N \to \infty$, without regard to the distribution of $\boldsymbol{x}$. Since the distribution of $\boldsymbol{x}$ has an asymptotically relevant effect on my own framework, but does not have an asymptotically relevant effect on the above analysis, we can construct experiments where these two methods have substantially different model selection preferences (as a function of sample size). The above analysis and my own framework are substantially different methodologies.

\end{document}
