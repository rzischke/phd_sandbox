\documentclass[12pt]{article}
\usepackage{mathtools, mathrsfs, amsmath, amsfonts, amssymb, amsthm, tensor, enumitem, pdflscape, float, booktabs}
\usepackage[margin=2cm]{geometry}
\usepackage[document]{ragged2e}
\usepackage[backend=biber,bibencoding=utf8,citestyle=authoryear]{biblatex}
\addbibresource{library.bib}

\DeclareMathOperator*{\argmin}{\arg\!\min}
\DeclareMathOperator*{\argmax}{\arg\!\max}

\begin{document}

\title{A Plan for Proving the Validity of the Parametric Bootstrapping of a Linear Pool of Distributional Forecasts}
\author{Ryan Zischke}

\maketitle

\section{Introduction}

This document outlines a plan for proving the validity of the parametric bootstrapping of a linear pool of distributional forecasts, where the weights are estimated using maximum likelihood estimation. Broadly speaking, there are two candidate approaches. The simplest approach is to build upon the asymptotic normality of the maximum likelihood weights estimator with asymptotic equicontinuity arguments. Under this approach, we hope that asymptotic normality of the weights estimate implies the validity of the parametric bootstrap. Here, we are saying nothing of the rate of convergence of the parametric bootstrap, merely that bootstrap converges at some unknown rate. A much more complex approach establishes the existance of an Edgeworth expansion of various (higher-order) partial derivatives of the likelihood function and some outer products thereof. The leads to higher-order improvements in the convergence of parametric bootstrap estimates of confidence regions of the weights. My preference is for the first approach.

\section{The Linear Predictive Pool}

For simplicity, we consider a linear pool of two distributional forecasts of the time series \\ \mbox{$\{ Y_i : i = 1, \cdots, n \}$}, where $Y_i \in \mathbb{R}$. Now let $p_0$ and $p_1$ be two candidate distributional forecasts of $Y$. In particular, the two candidate probability density functions of $Y_i$ given past observations are $p_0(y_i \mid \boldsymbol{y}_{1:i-1})$ and $p_1(y_i \mid \boldsymbol{y}_{1:i-1})$ where $\boldsymbol{y}_{1:i-1} = \begin{bmatrix} y_1 & y_2 & \cdots & y_{i-1} \end{bmatrix}$ is an $i-1$ dimensional vector of observations.

The linear predictive pool is
\begin{equation}
p(y_i \mid \boldsymbol{y}_{1:i-1}; w) = (1 - w) p_0(y_i \mid \boldsymbol{y}_{1:i-1}) + w p_1(y_i \mid \boldsymbol{y}_{1:i-1}),\ 0 \leq w \leq 1,
\end{equation}
which we assume is correctly specified and that the truth is at $w = w_0$. That is, the conditional probability density of $Y_i$ given $\boldsymbol{Y}_{1:i-1} = \boldsymbol{y}_{1:i-1}$ is $p(\cdot \mid \boldsymbol{y}_{1:i-1} ; w_0)$.

We will estimate $w_0$ using maximum likelihood estimation on the final $N \leq n$ observations, assuming that the MLE exists:
\begin{align}
\hat{w}_N(w_0) &= \argmax_{\theta}  \frac{1}{N} \sum_{i = n - N + 1}^{n} \ln p(Y_i \mid \boldsymbol{Y}_{1:i-1}; \theta) \\
&= \argmax_{\theta} \frac{1}{N} \sum_{i = n - N + 1}^{n} \ln \left\lbrace (1 - \theta)p_0(Y_i \mid \boldsymbol{Y}_{1:i-1} ) + \theta p_1(Y_i \mid \boldsymbol{Y}_{1:i-1}) \right\rbrace.
\end{align}
It is understood that ``$\hat{w}_N$'' is equivalent to ``$\hat{w}_N(w_0)$'', the MLE with observations from the truth.

Assuming it exists, the asymptotic variance of $\sqrt{N} ( \hat{w}_N - w_0 )$ is therefore $\sigma^2(w_0)$, where
\begin{equation}
\sigma^2(w) = \left( \lim_{n \to \infty} \frac{1}{n} \sum_{i=1}^n \mathbb{E}_w \left[ - \frac{\partial^2}{\partial w^2} \ln p(Y_i \mid \boldsymbol{Y}_{1:i-1} ; w) \right] \right)^{-1}.
\end{equation}
This expression for the asymptotic variance was lifted from Section 2 of \textcite{Andrews2005}. The expectation $\mathbb{E}_w$ is taken over the probability measure implied by $p(y_i \mid \boldsymbol{y}_{1:i-1} ; w)$, which we will denote by $P(\cdot ; w)$:
\begin{equation}
\mathbb{E}_w [ f(\boldsymbol{Y_{1:i}}) ] = \int f(\boldsymbol{Y_{1:i}}(\omega))\ \mathrm{d} P(\omega ; w).
\end{equation}
For example, the expectation with respect to the truth is $\mathbb{E}_{w_0}$.

In the next section, we will consider the parametric bootstrap estimate of the sampling distribution of the studentised statistic $T_N(w_0)$, where
\begin{equation}
T_N(w) = \frac{\hat{w}_N(w) - w}{\sigma(w)/\sqrt{N}}.
\end{equation}
Denote by $F_N(\cdot ; w)$ the cumulative distribution function of $T_N(w)$.

\section{Parametric Bootstrap}

Observations from the bootstrap process $\{Y_i^* : i = 1, \cdots, n\}$ are generated recursively using the distribution of the linear predictive pool evaluated at the MLE $\hat{w}_N$. That is, $y_i^*$ is drawn from the density $p(\cdot \mid \boldsymbol{y}^*_{1:i-1} ; \hat{w}_N)$, where $\boldsymbol{y}^*_{1:i-1} = \begin{bmatrix} y^*_1 & y^*_2 & \cdots & y^*_n \end{bmatrix}$ is a vector of bootstrap observations for the first $i-1$ indices.

The bootstrap MLE $w^*_N$ is defined in exactly the same way as the original MLE $\hat{w}_N$, except that the original process $\{ Y_i : i = 1, \cdots, n \}$ is replaced by the bootstrap process $\{Y_i^* : i = 1, \cdots, n \}$:
\begin{align}
w^*_N &= \hat{w}_N(\hat{w}_N(w_0)) \\
&= \hat{w}_N(\hat{w}_N) \\
&= \argmax_{w} \frac{1}{N} \sum_{i = n-N+1}^n \ln p(Y^*_i \mid \boldsymbol{Y}^*_{1:i-1} ; w ).
\end{align}

Then the studentised statistic of $w^*_N$ is
\begin{align}
T_N(\hat{w}_N) &= \frac{\hat{w}_N(\hat{w}_N) - \hat{w}_N}{\sigma(\hat{w}_N)/\sqrt{N}} \\
&= \frac{w^*_N - \hat{w}_N}{\sigma(\hat{w}_N)/\sqrt{N}}, \\
\end{align}
where $\sigma^2(\hat{w}_N)$ is the asymptotic variance of $\sqrt{N}(w^*_N - \hat{w}_N)$ (conditional on $\hat{w}_N$). The cumulative distribution function of $T_N(\hat{w}_N)$ (conditional on $\hat{w}_N$) is therefore $F_N(\cdot ; \hat{w}_N)$.

If $\hat{w}_N$ is a good approximation of $w_0$, then hopefully the sampling distribution of the studentised bootstrap weight, $F_N(\cdot ; \hat{w}_N)$, is a good approximation of the sampling distribution of the studentised MLE weight, $F_N(\cdot ; w_0)$. Our goal is to study the convergence, in some sense, of $F_N(\cdot ; \hat{w}_N)$ to $F_N(\cdot ; w_0)$ as $N \to \infty$.

\section{The Simple Approach}

\begin{enumerate}
\item Show the consistency and asymptotic normality of the MLE, $\hat{w}_N$.
\begin{itemize}
\item This is covered by Assumptions 1 - 3 of \textcite{Andrews2005}.
\item The idea is to find a set of regularity conditions on processes defined by $p_0$ and $p_1$ separately that will imply satisfaction of these assumptions for the linear pool $p$ at $w = w_0$.
\item For example, if a process defined by $p_0$ is strong mixing, and a process defined by $p_1$ is strong mixing, does that imply that the process defined by $p$ at $w = w_0$ is strong mixing?
\item Assumptions include that $Y_i$ is markov, a strong mixing condition, that the true parameter is an interior point (so that $0 < w_0 < 1$), four times differentiability of the likelihood, long-run moments of the likelihood and its derivatives that converge uniformly, that the expected likelihood has a unique maximum that is ``well-separated'', and others.
\item See \textcite{Andrews2005} for details on these assumptions.
\item For the strong mixing assumption, \textcite{Bradley2007} is a promising resource. Some chapters contain results specific to Markov processes, which I hope to exploit.
\item For many of the other assumptions, it may be helpful to note that if $\delta < w < 1-\delta$ for some $0 < \delta < 0.5$, then all existing derivatives of the conditional log-likelihood function are bounded above by a constant. That is, there exists $B_k > 0$ such that
\begin{equation}
\left \lVert \frac{\partial^k}{\partial w^k} \ln p(Y_i \mid Y_{1:i-1} ; w) \right \rVert \leq B_k
\end{equation}
for all $i$ and $k \geq 1$, with probability 1.
\end{itemize}
\item Show that $T_N(w_0)$ converges uniformly to $N(0,1)$, and that this implies
\begin{equation}
\sup_x \left \lvert F_N(x ; \hat{w}_N) - F_N(x ; w_0) \right \rvert \overset{p}{\to} 0.
\end{equation}
\begin{itemize}
\item This ought to be a straightforward application of Theorem 2 and Corollory 1 of my own \textit{On the Consistency of the Parametric Bootstrap}. I'd be (pleasantly) suprised if this result didn't exist elsewhere. It appears to be a variation on Theorem 21.9 of \textcite{Davidson1994} in which ``$\theta$'' takes on the role of the $x$ in $F_N(x ; w)$, and where stochastic equicontinuity is achieved by properties of the function $F_N$ and the MLE $\hat{w}_N$, seperately.
\item While we have the validity of the parametric bootstrap, we do not have any higher-order improvements.
\end{itemize}
\end{enumerate}

\section{The Complex Approach}

In this approach, we would prove the existance of an edgeworth expansion for $T_N(w)$, and subtract the expansions for $T_N(\hat{w}_N)$ and $T(w_0)$ to show higher-order improvements of the bootstrap.

\begin{enumerate}
\item Complete step 1 in the simple approach.
\item Satisfy assumption 4 of \textcite{Andrews2005}.
\begin{itemize}
\item We would then have access to the higher-order improvements in Theorem 1 of \textcite{Andrews2005}.
\item The idea again would be to find a set of regularity conditions on processes defined by $p_0$ and $p_1$ seperately that would imply satisfaction of Assumption 4.
\item Assumption 4 has parts (a) - (f), and amounts to a conditional Cramer condition.
\item Andrews remarks that Assumption 4 is ``... rather complicated and is not easy to verify in general.''
\end{itemize}
\end{enumerate}

\section{Conclusion}

My recommendation is to pursue the simple approach, and remark that higher-order improvements are possible. Firstly, I do not currently have the background to fully understand and show the requirements in Assumption 4, though perhaps this will change after auditing measure theory. Second, there is a risk that satisfying assumption 4 would require properties that apply jointly between models in the pool. In such a case these assumptions would need to be verified on a case by case basis for every combination; it would not be possible to declare any particular model bootstrap-safe for inclusion in a pool, and there would be little benefit gained beyond merely refering the reader to Assumption 4. In the shot term, it seems that showing the consistency and asymptotic normality of the MLE of the weight is a clear next-step, since it is required whichever approach (simple or complex) we take.

\printbibliography

\end{document}