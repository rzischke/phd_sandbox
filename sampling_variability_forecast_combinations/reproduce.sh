#!/bin/bash
docker run -ti --rm -v "$PWD":/home/docker/paper -w /home/docker/paper -u docker sampling_variability_forecast_combinations /bin/bash -c \
    ./sampling_variability_forecast_combinations.sh
