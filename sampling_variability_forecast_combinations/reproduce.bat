REM Load docker image if not done already.
docker image inspect sampling_variability_forecast_combinations --format=" " 2>1>NUL
if %ErrorLevel% neq 0 (docker load --input sampling_variability_dockerfile.tar.gz)

REM Reproduce paper within docker container using above image, but first convert dos line endings to unix.
docker run -ti --rm -v %cd%:/home/docker/paper -w /home/docker/paper sampling_variability_forecast_combinations /bin/bash -c ^
    "shopt -s globstar && dos2unix ** && su docker ./sampling_variability_forecast_combinations.sh"

PAUSE
