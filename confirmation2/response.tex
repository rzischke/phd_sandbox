\documentclass[12pt]{article}

\usepackage{natbib}
\bibliographystyle{plainnat}
\setcitestyle{authoryear, open={(},close={)}}
\newcommand{\textcite}[1]{\cite{#1}}

\usepackage[margin=3cm]{geometry}
\usepackage[document]{ragged2e}
\usepackage{parskip}

%\usepackage[backend=biber,bibencoding=utf8,citestyle=authoryear]{biblatex}
%\addbibresource{library.bib}

\begin{document}

\title{Response to Notice of Unsatisfactory Progress}
\author{Ryan Zischke}

\maketitle

Dear Professor Farshid Vahid-Araghi, Professor Rob Hyndman and Professor Mervyn Silvapulle,

In your Notice of Unsatisfactory Progress, you provided five tasks for me to undertake in view of satisfying the requirements of the Confirmation Milestone. I list these five tasks below, responding to each in turn, with references to the confirmation report and draft paper. The tasks are written in italics, and my responses are written in standard font.

The five tasks are:
\begin{enumerate}
\item \textit{Identify a specific research problem of academic interest that can be feasibly investigated within the timeframe of a PhD. Please be guided by your supervisors and trust their judgement.}

The research question is articulated in the Abstract in the Confirmation Report. This is a variation on a call to action by \citet{Clemen1989} applied to distributional forecast combinations, which are increasingly prominent in the literature. The key contributions will be: the investigation of the forecast combination puzzle in the context of distributional forecasts; the formal treatment of (weight) estimation uncertainty via the parametric bootstrap; the extension of all results - theoretical and empirical - to different scoring rules/estimating functions; and the exploration of the impact of model misspecification. All ideas were originally proposed by my supervisors.

We are using existing research on the forecast combination puzzle for point forecasts as a guide for this project on the distributional combination puzzle, so that it can be feasibly investigated within the timeframe of a PhD. In particular, factors that have been shown to play a part in the point forecast combination puzzle are likely to influence the distributional puzzle as well.

\item \textit{However you choose to phrase your research question, briefly discuss the results of key papers in the literature related to that research question in order to give the review panel a sense of the current state of knowledge and the novelty of your proposed contribution. In an academic journal article, such a discussion is included in the Introduction of the paper, and that will be sufficient for your confirmation report as well. We emphasise that we do not expect a full-length literature review.}

See the Introduction of the draft paper for such a discussion as it relates to the first of the three projects we have planned for my PhD. The Confirmation Report reviews the literature that is specific to the other two projects.

\item \textit{Clearly identify the steps necessary for you to answer the research question, and what progress you have made thus far. If there is a simple case where you feel that you can make some headway analytically, please include that in your report.} 

The research question is split into three projects. A complete representation of the progress made thus far is available in the draft paper, which corresponds to the first project. We are yet to gather empirical results in this first project, and plans for this research are outlined in Section 10 and Remark 4 of the draft paper. Plans for the subsequent two projects are outlined in Sections 3 and 4 of the Confirmation Report.

This project in general, and the bootstrap in particular, seem especially resistant to simplicity. In Sections 6 through 8, various results are derived concerning the distributional forecast combination puzzle, assuming a valid parametric bootstrap. The central element to the results in Section 6 is a second order Taylor series expansion, and is reminiscent of some proofs we undertook in your unit last year Mervyn. The result in Section 7 is straightforward, and follows from the definition of the convergence of a sequence. Section 8 shows a curious link between the results of the previous sections and the AIC.

My best attempt at showing the validity of the bootstrap for a distributional forecast combination of AR models with ARCH errors (nested by the framework of \citealp{Douc2004}) is given in Section 12.1, within the Appendix of the draft paper, which is an attempt at a proof for Theorem 1 in Section 4. It is tempting to think that a single example of a forecast combination, of two AR(1) processes, say, could be a simple case that would act as a platform for showing bootstrap validity of more empirically relevant distributional forecast combinations, of which AR processes with ARCH errors are a notable example. This position has several opponents. First, existing research on the parametric bootstrap does not provide assumptions that are easily verifiable on even the simplest of time-series processes. Assumption 4 of \citet{Andrews2005} is the key example of an assumption that is hard to verify, and \citet{Andrews2005} draws attention to this fact on the same page, two paragraphs after stating Assumption 3. These assumptions appear to have been verified for Gaussian processes \citep{Andrews2006}, but forecast combinations are not Gaussian in general. These difficult assumptions imply the existence of an Edgeworth expansion, which in turn implies so called ``higher-order improvements'' on the convergence of bootstrap-based statistics over their more traditional counterparts. Thus far, I have set about finding simpler assumptions for showing the validity of the parametric bootstrap without showing the existence of the Edgeworth expansions (and thus without higher-order improvements). The second opponent is that, even without the Edgeworth expansion assumptions, it appears that a valid parametric bootstrap requires a sort of uniformity that isn't required for standard maximum likelihood inference, and therefore hasn't been sought after in existing research on the asymptotic properties of maximum likelihood estimators of the kinds of model classes that would contain distributional forecast combinations. Section 4 and Section 12.1 (in the Appendix) of the draft paper together attempt to show that Theorem 3 of \citet{Douc2004} implies a valid parametric bootstrap, but I am now of the view that an additional uniformity assumption is required. The primary consequence of the second opponent is that it is difficult to find existing literature that confirm this property. The third opponent is that, if such literature doesn't exist, the amount of work required to show these properties for forecast combinations of AR models with ARCH errors, say, \textit{and also} investigate the distributional forecast combination puzzle, would surpass the threshold of achievability within the timeframe of a PhD. The two papers \citet{Wong2000}, on a mixture of AR models, and \citet{Wong2001}, on a mixture of AR models with ARCH errors, were published from Wong's PhD. These papers contain the definition of the models, stationarity conditions, how to find their likelihood maximising parameters, and some simulation-based and empirical applications. Given the current state of the literature, it appears that proving the validity of the parametric bootstrap on these models requires much else besides. At this stage it appears that proving the satisfaction of this uniformity assumption for the processes and models we wish to study in the various simulation and empirical sections my PhD is likely to become a task for future research, outside the scope of my PhD. Hence the aim is to simply assume this uniformity condition at this stage.

\item \textit{Make sure that all terminology and notation is clearly defined, at all times, and is consistent with the relevant conventions in the literature.} 

Section 2 of the draft paper is dedicated to the notation and terminology used throughout the paper. Where possible, notation is strictly identical to existing literature. There are two instances where no suitable, strictly identical notation could be found, and in these instances notation is defined to be as close as possible to the literature. The first instance is the use of dot notation in probability distributions, which is described in the last paragraph of Section 2, and the notion of a $(P_n)$-uniformity class, which is defined in Definition 4 of Section 12.1.

\item \textit{We recommend that you motivate your research question using a specific empirical application, preferably one that has attracted some attention in the literature, with which you could ultimately highlight the value of your contribution. We do not expect any empirical results at this stage, but we believe that having a specific application in mind will help in keeping your research focused. For example, you mentioned forecast combination as a motivating question in your presentation. In that area, two examples that have attracted much attention are forecasting the equity premium by combining simple bivariate predictive regressions, and combining algorithmic forecasts in M3 and M4 forecast combinations. We do not request that these particular applications are undertaken, but just provide them as examples.}

The specific empirical application motivating my research question is the success of forecast combinations in the M3 and M4 competitions. Indeed, this example is mentioned in the research question itself. This motivating application is supported by existing interest in the forecast combination puzzle, a problem traditionally viewed through an empirical lens. When the results of the M5 are released, I hope that the distributional forecast combination in particular attracts the attention so far afforded to its point forecast counterpart.

\end{enumerate}

%\printbibliography
\bibliography{library.bib}

\end{document}