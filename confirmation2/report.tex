\documentclass[12pt]{article}
\usepackage[margin=3cm]{geometry}
\usepackage{mathtools, mathrsfs, amsmath, amsfonts, amssymb, amsthm, tensor, enumitem, pdflscape, float, booktabs, pgfgantt, multicol}
%\usepackage[backend=biber,bibencoding=utf8,citestyle=authoryear]{biblatex}
%\addbibresource{library.bib}

\usepackage{natbib}
\bibliographystyle{plainnat}
\setcitestyle{authoryear, open={(},close={)}}
\newcommand{\textcite}[1]{\cite{#1}}

\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{theorem}{Theorem}

\DeclareMathOperator*{\argmin}{\arg\!\min}
\DeclareMathOperator*{\argmax}{\arg\!\max}

\begin{document}

\title{On the Distributional Forecast Combination Puzzle}
\author{Ryan Zischke}

\maketitle

\begin{abstract}
Forecast combinations have been a highly competitive forecast strategy for some time now, featuring prominently among the top forecasting methods of the fourth Makridakis competition, for example. Concurrent with the interest in forecast combinations has been an awareness of the peculiar phenomenon that combinations with \textit{equal} weights often outperform those with weights that are optimised with respect to some forecast accuracy criterion. Historically, research on this so-called `forecast combination puzzle' has almost exclusively been restricted to point forecasts. This PhD investigates the combination puzzle in the context of weighted combinations of \textit{distributional forecasts}. This focus is motivated both: by the increased prominence in the literature of distributional forecasts \textit{per se},\footnote{In the fifth Makridakis competition, for instance, competitors will be actually ranked on the quality of their distributional forecasts.} and by the lack of formal treatment of the combination puzzle in the distributional setting. In particular, we will examine how finite-sample estimation error, the choice of estimating function and the degree of model misspecification together affect the performance of combinations based on optimally estimated weights relative to the equally-weighted alternative. A better understanding of the reasons behind the distributional forecast combination puzzle, and guidance as to when an estimation combination is likely to be preferable and when it is not, can only lead to better distributional forecasts for researchers, government bodies and industry.
\end{abstract}

\newpage

\section{Introduction}

The forecast combination puzzle is the peculiar phenomenon whereby, in empirical applications, forecast combinations with equal weights often outperform those with weights that are optimised according to some form of out-of-sample forecast accuracy criterion. Reviewing the forecast combination research at the time, \citet{Clemen1989}, in the context of future research directions, asks ``What is the explanation for the robustness of the simple average of forecasts?'' He identifies two specific key questions. First, ``\ldots why does the simple average work so well \ldots'' and second ``\ldots under what conditions do other specific methods work better?'' There has been a substantial amount of research on the forecast combination puzzle since Clemen's call to action (especially in the last few years), but as far as we are aware, all prior research on the forecast combination puzzle has been restricted to point forecasts. This PhD is a step towards understanding the nature of the forecast combination puzzle for \textit{distributional} forecast combinations.

This PhD is divided into three separate projects. In the first project, we develop a bootstrap-based methodology to measure sampling variation in forecast combinations, and to study the effect of sample size on the relative performance of equally-weighted and optimally-weighted combinations. Sample size is the most well-studied factor affecting this performance difference in point forecasts, so it seems likely to play a significant role in distributional forecast combinations as well. Second, there has been increasing interest in practitioner-defined scoring rules or estimating functions \citep{Gneiting2007,Jacod2018} which are maximised to conduct parameter estimation, including the estimation of combination weights. This second project investigates how the choice of scoring rule impacts the relative performance of distributional forecast combinations with optimal and equal weights, where performance is measured in terms of that same scoring rule. The third project investigates how the difference between the data generating process and the two forecast combinations - equally-weighted and optimally-weighted - affect the relative performance of the combinations.

The structure of this report is as follows. Sections \ref{sec:p1}, \ref{sec:p2} and \ref{sec:p3} outline each of the three projects in turn. Section \ref{sec:timeline} provides a brief proposed timetable for completing the thesis, and progress to date. For a discussion of key papers in the literature about the forecast combination puzzle, see the introduction of the draft paper to the first project, attached as a companion to this report.

\section{The Parametric Bootstrap and the Distributional Forecast Combination Puzzle} \label{sec:p1}

This project is currently underway; see the attached draft paper.

\section{The Role of Estimating Functions in the Distributional Forecast Combination Puzzle} \label{sec:p2}

A scoring rule or estimating function is a function that measures the ``goodness of fit'' between a data generating process, and an estimate thereof. A scoring rule is proper if it is maximised when the estimate is identical to the data generating process, and it is strictly proper if it is maximised nowhere else \citep{Gneiting2007}. Scoring rules can be used as measures of predictive accuracy \citep[Section 6.4]{Gneiting2007} and to estimate parameters \citep{Jacod2018}. The canonical example of a scoring rule is the log score, and estimating parameters (in our case, weights) by maximising the log score is maximum likelihood estimation.

There are cases where it is inappropriate to apply maximum likelihood estimation. Although it has several optimality properties under correct specification, in practice our models are almost always misspecified. If we intend on using our forecast distribution (combination or otherwise) to estimating the probability of events that lie within a subset of the support, we may obtain better performance for forecasting those events by using a scoring rule that focuses on this subset, ignoring the performance of the forecast distribution outside it. This is achieved with the censored-likelihood scoring rule \citep{Diks2011}, which has been applied to distributional forecast combinations to great effect in the context of measuring downside risk in equity markets \citep{Opschoor2017}.\footnote{See also \citet{Loaiza-Maya2019}.}

It is impossible to apply maximum likelihood estimation to distributions that have a mixture of discrete and continuous components. Such distributions appear in the study of precipitation \citep{Krzysztofowicz1999,BjornarBremnes2004}, which can have a point mass at zero rain, and a probability density elsewhere. In such cases we can use the continuous ranked probability score (CRPS) \citep{Matheson1976, Hersbach2000}, which acts to minimise the expected $L^2$ norm between the cumulative distribution functions of the one-step-ahead predictive distribution and the true one-step-ahead conditional distribution.

Several questions come to mind when exploring how the choice of scoring rule might interact with the forecast combination puzzle. How does the relative performance of estimated optimal weights against equal weights vary with the choice of scoring rule in distributional forecast combinations? Are there characteristics of a scoring rule that affect its competitiveness with the equally weighted combination? That I know of, the literature on the forecast combination puzzle - which focuses on point forecasts, as noted above - makes exclusive use of the mean squared forecasting error; so this would be the first study to examine the nature of the forecast combination puzzle across estimating functions in the context of either point forecasts or distributional forecasts.

Similar to the first project, the intent is to have three primary sections to the paper: a theory section, a Monte Carlo study, and an empirical investigation. In the theory section, the aim is to take the bootstrap methodology developed in the first project for the maximum likelihood case, and extend it to estimating functions in general using the results of \citet{Jacod2018}. If it proves too ambitious to bring the parametric bootstrap into this context, we can use limiting normal approximations instead. A Monte Carlo simulation can then compare the performance of optimal distributional forecast combinations produced by various scoring rules against the equally weighted combination in a manner similar to the first project (see the attached draft paper). Finally, we can examine the performance of the various scoring rules in forecasting the many time-series used in the M4 competition \citep{Makridakis2018, Makridakis2020}.

\section{The Distributional Forecast Combination Puzzle and Model Misspecification} \label{sec:p3}

While convenient for analysis, the assumption that a real-world stochastic process conforms to any particular parametric model almost never holds in practice. In their seminal paper on forecast combinations, \citet{Bates1969} remark that different forecasts of the same phenomena typically contain independent information. This would imply that each forecast is misspecified. The choice of exactly how to combine forecasts is almost always made independently of the stochastic process being forecast (a linear combination is often chosen). For this reason, we can hardly expect the forecast combination to be correctly specified either. The assumption of correct specification therefore runs counter to the spirit of forecast combinations, which are often deliberately employed to account for misspecification.

Beginning with \citet{Domowitz1982} and \citet{Bates1985}, there is now a sizeable literature concerned with estimation and inference of parametric models without assuming correct specification; see \citet{Jacod2018} for a recent review of such theory in the context of stochastic processes. Here, estimation is conducted by maximising an estimating function, which is typically derived from a scoring rule. Under regularity, the estimate converges (in probability or almost surely) to the score maximising process, and inference is conducted with respect to this optimal process. Note that the optimal process is only the data generating process if the forecasting model is correctly specified.

In this project, we study the effect of model misspecification on the performance gains of an optimally weighted distributional forecast combination over the equally weighted alternative. To measure misspecification, we plan to compare the divergence function evaluated at the theoretically optimal weights, and evaluated at the equally weighted combination. See Section 2.2 of \citet{Gneiting2007} for further details on the divergence function of a scoring rule. Our hypothesis is that a greater difference between the divergence at the optimally weighted combination and the equally weighted combination will typically lead to the optimally weighted combination outperforming the equally weighted combination at a lower sample size. Further, if the forecasts to be combined all perform well individually, then it seems plausible that the difference between the divergence of the optimal combination and the equally weighted combination would be small. However, if this is usually the case in practice, and if a smaller difference between divergences leads to the optimally weighted combination outperforming the equally weighted combination at a higher sample size, then we would expect the equally weighted combination to outperform the optimally weighted combination for a high proportion of real datasets.

In a similar fashion to the first project, the intent is to produce a paper with theoretical, simulation and empirical components. The methodology for assessing the performance of the forecast combination with estimated optimal weights against the equally weighted alternative from the second project ought to be fully applicable to the misspecification case, leaving room to explore the theoretical underpinnings of the influence of misspecification on the forecast combination puzzle in greater depth. For the simulation component, it seems natural to fix the forecast combination and strategically manipulate the data generating process, noting the divergences of the equally weighted and optimal forecast combinations and the relative performances of those combinations as the data generating process changes. For the empirical section of the paper, we can return to the datasets of the M4 competition to get a sense of the likely divergences for the optimal combination and the equally weighted combination when working on a real dataset. 

\section{Timeline and Progress to Date} \label{sec:timeline}

For a detailed view of progress to date on the first project, see the attached draft paper. Advancing the theoretical component of this project has involved mastering some difficult literature, but that work has borne fruit, and will enable the later papers to proceed more smoothly. While writing is yet to begin on the simulation section, study of the forecast combination puzzle in the M4 datasets is underway. We hope to have the final paper ready for submission by the end of the year.

For the remaining two projects, we have allocated 9 months each: 3 months for theory,  and 2 months each for simulation, empirical analysis and writing. We expect the second project to run from January to September 2021, and the third project to run from Oct 2021 until June 2022. This leaves 2 months to finalise the thesis before submission on the 1\textsuperscript{st} of September, 2022.

%\printbibliography
\bibliography{library.bib}

\end{document}
