library(ggplot2)
library(gsl)

# Distribution of nesting phi-score:
# phiShat ~ -sqrt(1 + (1/t)*Z^2)
# where Z ~ N(0,1)

sigma0 <- 1
So <- -sigma0

mu0_given_asy_tsize_est <- function(T0) {
  square <- (sqrt(3)/(2*T0) + 1)^2
  surd <- sqrt(square - 1)
  return(surd*sigma0)
}

asy_tsize_est_given_mu0 <- function(mu0) {
  num <- sqrt(3)*sigma0/2
  denom <- sqrt(sigma0*sigma0 + mu0*mu0) - sigma0
  return(num/denom)
}

get_phiShat_first_moment <- function(t) {
  return(-sigma0*sqrt(2)*hyperg_U(-1/2,0,t/2)/sqrt(t))
}

get_phiShat_second_moment <- function(t) {
  return(sigma0*sigma0*(1 + 1/t))
}

get_phiShat_variance <- function(t) {
  return(get_phiShat_second_moment(t) - get_phiShat_first_moment(t)^2)
}

get_phiStilde <- function(mu0) {
  return(-sqrt(sigma0*sigma0 + mu0*mu0))
}

get_fin_tsize_est <- function(t,mu0) {
  phiShat_first_moment <- get_phiShat_first_moment(t)
  phiShat_second_moment <- get_phiShat_second_moment(t)
  phiShat_variance <- phiShat_second_moment - phiShat_first_moment^2
  phiShat_bias <- (So - phiShat_first_moment)^2
  
  phiStilde <- get_phiStilde(mu0)
  phiStilde_bias <- (So - phiStilde)^2
  
  fin_tsize_est_squared <- t^2*phiShat_variance/(phiStilde_bias - phiShat_bias)
  fin_tsize_est_squared[fin_tsize_est_squared<0] <- Inf
  return(sqrt(fin_tsize_est_squared))
}

get_phiShat_ese <- function(t) {
  phiShat_first_moment <- get_phiShat_first_moment(t)
  phiShat_second_moment <- get_phiShat_second_moment(t)
  phiShat_variance <- phiShat_second_moment - phiShat_first_moment^2
  phiShat_bias <- So - phiShat_first_moment
  return(phiShat_variance + phiShat_bias^2)
}

get_phiStilde_ese <- function(mu0) {
  phiStilde <- get_phiStilde(mu0)
  error <- So - phiStilde
  return(error*error)
}

get_tsize <- function(mu0){
  mu0min <- min(mu0)
  asy_tsize_max <- asy_tsize_est_given_mu0(mu0min)
  t <- 1:(2*asy_tsize_max)
  
  tsize_out <- rep(0,length(mu0))
  for (i in 1:length(mu0)) {
    m <- mu0[i]
    phiShat_ese <- get_phiShat_ese(t)
    phiStilde_ese <- get_phiStilde_ese(m)
    tsize_out[i] <- max(c(t[phiStilde_ese<=phiShat_ese],1))
  }
  
  return(tsize_out)
}

T_hat <- 10^seq(from = 1, to = 3, length.out = 1000)
mu0 <- mu0_given_asy_tsize_est(T_hat)
T_truth <- get_tsize(mu0)

plot_frame <- data.frame(
  "est" = T_hat,
  "mean" = mu0,
  "est_on_truth" = T_hat/T_truth,
  "est_minus_truth" = T_hat - T_truth
)

mean_vs_tsize_est <- ggplot(data = plot_frame, aes(x=est, y=mean)) +
  geom_line() +
  scale_x_log10(n.breaks = 10) +
  scale_y_log10(n.breaks = 10) +
  xlab("Transition Size Estimate") +
  ylab("Mean of DGP")

estimate_divide_truth <- ggplot(data = plot_frame, aes(x=est, y=est_on_truth)) +
  geom_line() +
  scale_x_log10() +
  xlab("Transition Size Estimate") +
  ylab("Estimate Divide Truth")
#print(estimate_divide_truth)

estimate_minus_truth <- ggplot(data = plot_frame, aes(x=est, y=est_minus_truth)) +
  geom_line() +
  scale_x_log10() +
  xlab("Transition Size Estimate") +
  ylab("Estimate Minus Truth")
#print(estimate_minus_truth)
