\documentclass[12pt]{article}
\usepackage{mathtools,mathrsfs,amsmath,amsfonts,amssymb,amsthm,tensor}
\usepackage[margin=2cm]{geometry}
\usepackage{parskip}

\usepackage[english]{babel}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}

%\usepackage[backend=biber,bibencoding=utf8,citestyle=authoryear]{biblatex}
%\addbibresource{references.bib}
%\usepackage{natbib}
%\bibliographystyle{plainnat}

\DeclareMathOperator*{\argmin}{\arg\!\min}
\DeclareMathOperator*{\argmax}{\arg\!\max}
\newcommand{\thetaproj}[1]{\vec{\prescript{\scriptscriptstyle{#1}\!}{}{\theta}_n}}
\newcommand{\thetaprojcdf}[1]{\vec{\prescript{\scriptscriptstyle{#1}\!}{}{F}_n}}
\newcommand{\zproj}[1]{\vec{\prescript{\scriptscriptstyle{#1\!}{}{Z}}}}

\begin{document}

\title{On the Consistency of the Parametric Bootstrap}
\author{Ryan Zischke}
\maketitle

Suppose we have a sample of size $n$ from a particular member of a parametric family of stochastic processes identified by $\theta \in \mathbb{R}^k$, and suppose this DGP has $\theta = \theta_0$ for some unknown $\theta_0$. Now, let $\hat{\theta}_n$ be a consistent estimator of $\theta_0$ such that $\hat{\theta}_n \overset{p}{\to} \theta_0$, and let $F_n(\cdot ; \theta)$ be the CDF of $T_n(\theta) \in \mathbb{R}^l$, an arbitrary statistic of such a DGP. We will now provide sufficient conditions for the validity of approximating the distribution of $T_n(\theta_0)$ with the distribution of $T_n(\hat{\theta}_n)$.

\begin{theorem}
\label{th:eqdist}
If the set of functions $\lbrace F_n(x, \cdot) : n \in \mathbb{N}, x \in \mathbb{R}^l \rbrace$ is asymptotically equicontinuous at $\theta_0$ as $n \to \infty$, then
\begin{equation}
\sup_{x} \left \lvert F_n(x, \hat{\theta}_n) - F_n(x, \theta_0) \right \rvert \overset{p}{\to} 0.
\end{equation}
\end{theorem}
\begin{proof}
Since $\hat{\theta}_n \overset{p}{\to} \theta_0$, by definition there exists $N_1 : (0,\infty) \mapsto \mathbb{N}$ such that
\begin{equation}
n \geq N_1(\epsilon) \implies \mathrm{P} \left( \left \lVert \hat{\theta}_n - \theta_0 \right \rVert \leq \epsilon \right) \geq 1 - \epsilon.
\end{equation}
Likewise, by the definition of asymptotic equicontinuity there exists $N_2 : (0, \infty) \mapsto \mathbb{N}$ and \mbox{$\delta : (0, \infty) \mapsto (0, \infty)$} such that,
\begin{equation}
n \geq N_2(\epsilon), \lVert \theta - \theta_0 \rVert \leq \delta(\epsilon), x \in \mathbb{R} \implies \left \lvert F_n(x ; \theta) - F_n(x ; \theta_0) \right \rvert \leq \epsilon.
\end{equation}
If $n$ is large enough, $\hat{\theta}_n$ is probably within both $\epsilon$ and $\delta(\epsilon)$ distance of $\theta_0$, and $F_n(x ; \hat{\theta}_n)$ is probably within $\epsilon$ distance of $F_n(x; \theta_0)$. For all $\epsilon > 0$ and $n \geq \max \lbrace N_1(\epsilon), N_1(\delta(\epsilon)), N_2(\epsilon) \rbrace$,
\begin{align}
\mathrm{P} \left(\sup_{x,n} \left \lvert F_n(x ; \hat{\theta}_n) - F_n(x ; \theta_0) \right \rvert \leq \epsilon \right) &\geq \mathrm{P} \left( \left \lVert \hat{\theta}_n - \theta_0 \right \rVert \leq \min \lbrace \epsilon, \delta(\epsilon) \rbrace \right) \\
&\geq 1 - \min \lbrace \epsilon, \delta(\epsilon) \rbrace \\
& \geq 1 - \epsilon.
\end{align}
\end{proof}
A special case that might be of interest is where the statistic $T_n(\theta_0)$ converges in distribution to a random variable with CDF $F_{\infty}(\cdot ; \theta_0)$. If $F_{\infty}$ is sufficiently regular, then the bootstrap procedure is valid.
\begin{theorem}
\label{th:cvdist}
Assume there exists a function $F_{\infty}: \mathbb{R}^l \times \mathbb{R}^k \mapsto \mathbb{R}$ and a neighbourhood $\Theta \subseteq \mathbb{R}^k$ of $\theta_0$ such that $F_n$ converges to $F_{\infty}$ uniformly in $\mathbb{R}^l \times \Theta$. Further, suppose that the set of functions $\lbrace F_{\infty}(x ; \cdot) : x \in \mathbb{R}^l \rbrace$ is equicontinuous at $\theta_0$. Then the set of functions \mbox{$\lbrace F_n(x ; \cdot ) : n \in \mathbb{N}, x \in \mathbb{R}^l \rbrace$} is asymptotically equicontinuous at $\theta_0$.
\end{theorem}
\begin{proof}
Since $\Theta$ is a neighbourhood of $\theta_0$, there exists $\delta_1 \in \mathbb{R}$ such that
\begin{equation}
\lVert \theta - \theta_0 \rVert \leq \delta_1 \implies \theta \in \Theta.
\end{equation}
By the definition of uniform convergence, there exists $N : (0,\infty) \mapsto \mathbb{N}$ such that
\begin{equation}
n \geq N(\epsilon),\theta \in \Theta, x \in \mathbb{R}^l \implies \lvert (F_n - F_{\infty})(x ; \theta)\rvert \leq \epsilon.
\end{equation}
By the definition of equicontinuity, there exists $\delta_2 : (0,\infty) \mapsto (0,\infty)$ such that
\begin{equation}
\lVert \theta - \theta_0 \rVert \leq \delta_2(\epsilon), x \in \mathbb{R}^l \implies \lvert F_{\infty}(x ; \theta) - F_{\infty}(x ; \theta_0) \rvert \leq \epsilon.
\end{equation}
Given $\epsilon > 0$, consider the case that $n \geq N(\epsilon/3)$ and $\lVert \theta - \theta_0 \rVert \leq \min \lbrace \delta_1, \delta_2(\epsilon/3) \rbrace$. Then we have:
\begin{align}
&\sup_x \lvert F_n(x ; \theta) - F_n(x ; \theta_0) \rvert \\
&\leq \sup_x \lvert (F_n - F_{\infty})(x ; \theta) \rvert + \sup_x \lvert (F_n - F_{\infty})(x ; \theta_0) \rvert + \sup_x \lvert F_{\infty}(x ; \theta) - F_{\infty}(x ; \theta_0) \rvert \\
&\leq 2 \times \sup_{x, \vartheta \in \Theta} \lvert (F_n - F_{\infty})(x ; \vartheta) \rvert +  \sup_x \lvert F_{\infty}(x ; \theta) - F_{\infty}(x ; \theta_0) \rvert \\
&\leq \epsilon.
\end{align}
\end{proof}
This leads immediately to the following corollary.
\begin{corollary}
Under the assumptions of theorem \ref{th:cvdist},
\begin{equation}
\sup_{x} \left \lvert F_n(x, \hat{\theta}_n) - F_n(x, \theta_0) \right \rvert \overset{p}{\to} 0.
\end{equation}
\end{corollary}

%Now suppose we know that $\theta_0 \in \Omega \subseteq \mathbb{R}^k$, where $\Omega$ is convex and closed. Unfortunately, the nature of $F_n(\theta_0)$ as a sequence along $n$ differs discontinuously depending on whether $\theta_0$ is in the interior of $\Omega$, or on the boundary. We will need to treat these two cases separately. With this in mind, let us consider multiple estimators of $\theta_0$ with different properties.
%
%First, let $\check{\theta}_n \in \mathbb{R}^k$ be a consistent and asymptotically normal $M$-estimator of $\theta_0$ satisfying the assumptions of Theorem 5.7 and 5.23 of \cite{VanDerVaart1998}. Second, let
%\begin{equation}
%\thetaproj{V} = \Pi_{V}(\check{\theta}_n, \Omega),
%\end{equation}
%where $\Pi$ is the projection operator defined by
%\begin{align}
%\Pi_{W}(x, X) &= \argmin_{y \in X}\ \lVert y - x \rVert_W \\
%&= \argmin_{y \in X}\ (y - x)^T W^{-1} (y - x).
%\end{align}
%Our final estimator $\tilde{\theta}_n \in \Omega$ is an $M$-estimator that is a local maxmiser of it's criterion function over $\Omega$, which we assume is the same criterion function used by $\check{\theta}_n$. We also assume that $\tilde{\theta}_n$ satisfies assumptions A through D of \cite{Geyer1994}. Note that since all three of these estimators are consistent, they may all take on the role of $\hat{\theta}_n$.
%Denote by $\check{F}_n(\cdot, \theta)$, $\thetaprojcdf{V}(\cdot, \theta)$ and $\tilde{F}_n(\cdot, \theta)$ the CDFs of the statistics $\sqrt{n} (\check{\theta}_n - \theta)$, $\sqrt{n}(\thetaproj{V} - \theta)$ and $\sqrt{n}(\tilde{\theta}_n - \theta)$, respectively, in the case that the truth were $\theta_0 = \theta$. Once again, we distinguish between a fixed truth $\theta_0$ and an arbitrary parameter $\theta$ to view these CDFs as functions of the DGP parameter.
%
%By Theorem 5.23 of \cite{VanDerVaart1998}, $\sqrt{n}(\check{\theta}_n - \theta_0) \overset{d}{\to} \mathrm{N}(0, \mathcal{I}_{\theta_0}^{-1})$ for some variance-covariance matrix $\mathcal{I}_{\theta_0}^{-1}$. By the assumptions of the same theorem, we can use theorem \ref{th:cvdist} to verify that
%\begin{equation}
%\sup_{x \in \mathbb{R}^k} \left \lvert \check{F}_n(x, \hat{\theta}_n) - \check{F}_n(x, \theta_0) \right \rvert \overset{p}{\to} 0,
%\end{equation}
%and the parametric bootstrap is valid. The same can be shown for $\sqrt{n}(\tilde{\theta}_n - \theta_0)$ and $\sqrt{n}(\thetaproj{V} - \theta_0)$  in the case that $\theta_0 \in \mathrm{Int}(\Omega)$, though depending on the DGP family we might require that $\hat{\theta}_n \in \Omega$.
%
%The theory for the validity of the parametric bootstrap for the sampling distributions of the constrained estimators  is more delicate when allowing for $\theta_0 \notin \mathrm{Int}(\Omega)$. As $n \to \infty$,
%\begin{equation}
%\sqrt{n}(\tilde{\theta}_n - \theta_0) \overset{d}{\to} \vec{Z}, \label{eqn:asytilde}
%\end{equation}
%where
%\begin{equation}
%\vec{Z} \sim \begin{cases} N(0, \mathcal{I}_{\theta_0}^{-1}) & \theta_0 \in \mathrm{Int}(\Omega) \\
%\Pi_{\mathcal{I}_{\theta_0}^{-1}} \left( N(0,\mathcal{I}_{\theta_0}^{-1}), T_{\Omega}(\theta_0) \right) & \theta_0 \notin \mathrm{Int}(\Omega)
%\end{cases},
%\end{equation}
%and where $T_{\Omega}(\theta_0)$ is the tangent cone of $\Omega$ at the boundary point $\theta_0$ (see \cite{Geyer1994}). We come to the unfortunate conclusion that $\{ \tilde{F}_n(x, \theta) : n \in \mathbb{N}, x \in \mathbb{R}^k \}$ is not asymptotically equicontinuous for $\theta_0$ on the boundary, so neither theorem \ref{th:eqdist} nor theorem \ref{th:cvdist} apply.
%
%Fortunately, the projection operator $\Pi_{V}(\theta, \Omega)$ is continuous in $(\theta, V)$ \citep[Page~213]{Silvapulle2004}. Thus, even if $\theta_0 \notin \mathrm{Int}(\Omega)$, we can use $\thetaprojcdf{}(\cdot, \hat{\theta}_n) = \thetaprojcdf{ \mathcal{I}_n }(\cdot, \hat{\theta}_n)$ to approximate $\tilde{F}_n(\cdot, \theta_0)$, where $\mathcal{I}_n = \mathcal{I}_{\hat{\theta}_n}$. For all $\theta_0 \in \Omega$ and $x \in \mathbb{R}^k$,
%\begin{equation}
%\left \lvert \thetaprojcdf{}(x, \hat{\theta}_n) - \tilde{F}_n(x, \theta_0) \right \rvert \leq \left \lvert \thetaprojcdf{}(x, \hat{\theta}_n) - F_{\vec{Z}} (x) \right \rvert + \left \lvert \tilde{F}_n(x, \theta_0) - F_{\vec{Z}}(x) \right \rvert \overset{p}{\to} 0,
%\end{equation}
%where the first term goes to zero by applying theorem \ref{th:eqdist} and the continuous mapping theorem, and the second term goes to zero by equation \ref{eqn:asytilde}. This demonstrates a clear advantage of the bootstrap over the asymptotic distribution approximation: whereas the bootstrap procedure is valid for all $\theta_0 \in \Omega$, the asymptotic distribution approximation requires assuming either that $\theta_0 \in \mathrm{Int}(\Omega)$, or that $\theta_0 \notin \mathrm{Int}(\Omega)$.
%
%\bibliography{references}

%\printbibliography

\end{document}