\documentclass[12pt]{article}
\usepackage{mathtools, mathrsfs, amsmath, amsfonts, amssymb, amsthm, tensor, enumitem, pdflscape, float, booktabs, parskip}
\usepackage[margin=2cm]{geometry}
%\usepackage[document]{ragged2e}
\usepackage[backend=biber,bibencoding=utf8,citestyle=authoryear]{biblatex}
\addbibresource{library.bib}

\theoremstyle{plain}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{theorem}{Theorem}

\theoremstyle{remark}
\newtheorem{remark}{Remark}

\DeclareMathOperator*{\argmin}{\arg\!\min}
\DeclareMathOperator*{\argmax}{\arg\!\max}

\begin{document}

\title{A Problem with Proving Uniform Asymptotic Normality of the Maximum Likelihood Estimator of a Linear Pool of Distributional Forecasts}
\author{Ryan Zischke}

\maketitle

\raggedright

Gael, David and Don,

I found a paper with sufficient conditions for uniform asymptotic normality of the maximum likelihood estimator, and hence parametric bootstrap validity (\cite{Sweeting1980}). The sufficient conditions amount to existance and convergence (in a particular sense) of the observed fisher information. Rather than establish consistency of the MLE with weaker assumptions, then asymptotic normality with stronger assumptions, this paper has one set of strong assumptions for both asymptotic normality and consistency. Note that identification is assumed implicitly. Give this paper a read if you haven't already.

To date, I have been unable to produce a set of assumptions sufficient for uniform asymptotic normality of the MLE specialised to the distributional forecast combination case. It seems that we require some sort of limit on the level of dependence between the information provided by observations far apart in time, but I am yet to locate a result showing that such a limit on the component processes would be inherited by a mixture of them. As we will see, this is complicated by the fact that the distributional forecast combination seems potentially chaotic, in the sense that the derivative of an expectation with respect to the weights typically exploads as $n \to \infty$.

In section one, we define the distributional forecast combination along with its maximum likelihood estimator, score and information functions. We also provide some basic properties thereof. In section two, we discuss the difficulties associated with satsifying assumption C1 of \textcite{Sweeting1980}. In section three we consider a number of options for how this project could procede.

\section{The Distributional Forecast Combination}

For now, we will work with a distributional forecast combination $f$ of two components, $p_0$ and $p_1$, with one weight $\theta$:
\begin{equation}
p(x_i \mid x_{1:i-1} ; \theta) = (1 - \theta) p_0(x_i \mid x_{1:i-1}) + \theta p_1(x_i \mid x_{1:i-1}),
\end{equation}
and assume $p_o(x_1)$ and $p_1(x_1)$ are given to kickstart the process. When it exists, the maximum likelihood estimator is
\begin{align}
\hat{\theta}_n &= \argmax_{\theta \in (0,1)}  \ln p(X_{1:n} ; \theta) \\
&= \argmax_{\theta \in (0,1)} \sum_{i=1}^n \ln p(X_i \mid X_{i:i-1}) \\
&= \argmax_{\theta \in (0,1)} \sum_{i=1}^n \ln \left( (1 - \theta) p_0(X_i \mid X_{1:i-1}) + \theta p_1(X_i \mid X_{1:i-1}) \right) \\
&= \argmax_{\theta \in (0,1)} \sum_{i=1}^n \ln \left( (1 - \theta) P_{0,i} + \theta P_{1,i} \right), \\
\end{align}
where $P_{0,i} = p_0(X_i \mid X_{1:i-1})$ and $P_{0,i} = p_1(X_i \mid X_{1:i-1})$. We will make reference to the score $S_n(\theta)$ of the sample, along with the score $s_i(\theta)$ of an observation:
\begin{align}
S_n(\theta) &= \frac{\partial}{\partial \theta} \ln p(X_{1:n} ; \theta) \\
&= \sum_{i=i}^n \frac{\partial}{\partial \theta} \ln p(X_i \mid X_{1:i-1} ; \theta) \\
&= \sum_{i=1}^n \frac{P_{1,i} - P_{0,i}}{(1 - \theta) P_{0,i} + \theta P_{1,i}} \\
&= \sum_{i=1}^n s_i(\theta).
\end{align}
Following \textcite{Sweeting1980} write $\mathscr{I}_n$ for the information of the sample, and write $l_i$ for the information of an observation:
\begin{align}
\mathscr{I}_n(\theta) &= - \frac{\partial^2}{\partial \theta^2} \ln p(X_{1:n} ; \theta) \\
&= \sum_{i=1}^n \left( \frac{P_{1,i} - P_{0,i}}{(1-\theta)P_{0,i} + \theta P_{1,i}} \right)^2 \\
&= \sum_{i=1}^n s_i(\theta)^2 \\
&= \sum_{i=1}^n l_i(\theta).
\end{align}

All derivatives
\begin{equation}
\frac{\partial^k}{\partial \theta^k} \ln p(X_{1:i} \mid X_{1:i-1}) = (-1)^{k+1}(k-1)! \left( \frac{P_{1,i} - P_{0,i}}{(1 - \theta) P_{0,i} + \theta P_{1,i}} \right)^k
\end{equation}
are bounded for $0 < \theta < 1$, since
\begin{align}
\left \lvert \frac{P_{1,i} - P_{0,i}}{(1-\theta)P_{0,i} + \theta P_{1,i}}\right \rvert &\leq \frac{P_{1,i}}{(1-\theta)P_{0,i} + \theta P_{1,i}} + \frac{P_{0,i}}{(1-\theta)P_{0,i} + \theta P_{1,i}} \\
&\leq \frac{P_{1,i}}{\theta P_{1,i}} + \frac{P_{0,i}}{(1-\theta) P_{0,i}} \\
&\leq \frac{1}{\theta} + \frac{1}{(1-\theta)} \\
&= \frac{1}{\theta (1 - \theta)}, \\
\therefore\ \left \lvert \frac{\partial^k}{\partial \theta^k} \ln p(X_{1:i} \mid X_{1:i-1}) \right \rvert &\leq \frac{(k-1)!}{\theta^k (1 - \theta)^k}.
\end{align}
As a consequence, the information and its derivative have the bounds
\begin{align}
\lvert \mathscr{I}_n(\theta) \rvert &\leq \frac{n}{\theta^2 (1 - \theta)^2}, \\
\lvert \mathscr{I}_n'(\theta) \rvert &\leq \frac{2n}{\theta^3 (1 - \theta)^3}.
\end{align}

\section{Convergence in Distribution of the Normalised Information}

Assumption C1 of \textcite{Sweeting1980} requires that a normalised information converge in distribution. For now we will not concern ourselves with the uniformity requirements of C1. Simply take $A_n(\theta) = \sqrt{n}$, so that we are looking for some distribution $W(\theta)$ such that
\begin{equation}
\frac{1}{n} \mathscr{I}_n(\theta) \Rightarrow W(\theta),
\end{equation}
where the convergence is pointwise in distribution. In the simpliest case a law of large numbers applies, and the normalised information converges in probability to a constant. Since $\mathscr{I}_n(\theta)/n$ is bounded, its variance exists and is bounded uniformly in $n$. If a law of large numbers were to apply, we would therefore have
\begin{align}
\mathrm{Var}_{\theta} \left( \frac{1}{n} \mathscr{I}_n(\theta) \right) &= \frac{1}{n^2} \sum_{i=1}^n \sum_{j=1}^n \mathrm{Cov}_{\theta}(l_i(\theta), l_j(\theta)) \\
&= \frac{1}{n^2} \sum_{i=1}^n \mathrm{Var}_{\theta}(l_i(\theta)) + \frac{2}{n^2} \sum_{i=2}^n \sum_{j=1}^{t-1} \mathrm{Cov}_{\theta}(l_i(\theta), l_j(\theta)) \\
&\to 0
\end{align}
as $n \to \infty$. Since $l_i$ is bounded, its variance is bounded, and the variance term vanishes. Therefore a necessary condition for a law of large numbers on the normalised information is
\begin{equation}
\lim_{n \to \infty} \frac{1}{n^2} \sum_{i=2}^n \sum_{j=1}^{i-1} \mathrm{Cov}_{\theta}(l_i(\theta),l_i(\theta)) = 0.
\end{equation}
A sufficient condition given by \textcite[Theorem 19.2]{Davidson1994} is
\begin{equation}
\sup_i \lvert \mathrm{Cov}_{\theta}(l_i(\theta), l_{i-m}(\theta)) \rvert = \mathcal{O}((\log m)^{-1-\delta})
\end{equation}
for some $\delta > 0$.

Although this is a mild condition, it is difficult to verify directly based on properties of $p_0$ and $p_1$. Consider the following expression of the covariance:
\begin{equation}
\mathrm{Cov}_{\theta}(l_i(\theta),l_j(\theta)) = \int (l_i(\theta) - \mathbb{E}_{\theta}[l_i(\theta)]) (l_j(\theta) - \mathbb{E}_{\theta}[l_j(\theta)]) \prod_{k=1}^{i} [(1 - \theta)p_{0,i} + \theta p_{1,i}] \mathrm{d}x_{1:i},
\end{equation}
where $i \geq j$. Attempts to transfer some limit on the autocovariances of the processes $p_0$ and $p_1$ are frustrated by the expectation terms, which also contain $p_0$ and $p_1$. Perhaps we could instead use
\begin{equation}
\mathrm{Cov}_{\theta}(l_i(\theta),l_j(\theta)) = \mathbb{E}_{\theta}[l_i(\theta) l_j(\theta)] - \mathbb{E}_{\theta}[l_i(\theta)]\mathbb{E}_{\theta}[l_j(\theta)].
\end{equation}
In order to express each expectation as some function of expectations involving $p_0$ and $p_1$, we will expand the product that defines the probability distribution function. Let $\{0,1\}^n$ be the set of sequences of length $n$, where each element of a sequence is either $0$ or $1$. Then
\begin{align}
p(x_{1:n} ; \theta) &= \prod_{i=1}^n (1 - \theta)p_0(x_i \mid x_{1:i-1}) + \theta p_1(x_i \mid x_{1:i-1}) \\
&= \sum_{k \in \{0,1\}^n} (1 - \theta)^{n - \Sigma k} \theta^{\Sigma k} \prod_{i=1}^n p_{k_i}(x_i \mid x_{1:i-1}), \\
\Sigma k &= \sum_{i=1}^n k_i.
\end{align}
An expectation, of $G_n$ say, can then pass through to the product of some permutation of $p_0$ and $p_1$:
\begin{align}
\mathbb{E}_{\theta}[G_n] &= \int G_n \sum_{k \in \{0,1\}^n} (1 - \theta)^{n - \Sigma k} \theta^{\Sigma k} \prod_{i=1}^n p_{k_i}(x_i \mid x_{1:i-1}) \mathrm{d} x_{1:i} \\
&= \sum_{k \in \{0,1\}^n} (1 - \theta)^{n - \Sigma k} \theta^{\Sigma k} \int G_n \prod_{i=1}^n p_{k_i}(x_i \mid x_{1:i-1}) \mathrm{d} x_{1:i} \\
&= \sum_{k \in \{0,1\}^n} (1 - \theta)^{n - \Sigma k} \theta^{\Sigma k} \mathbb{E}_k[G_n],
\end{align}
where
\begin{equation}
\mathbb{E}_k[G_n] = \int G_n \prod_{i=1}^n p_{k_i}(x_i \mid x_{1:i-1}) \mathrm{d} x_{1:i}.
\end{equation}
It seems that for the covariances to diminish, they would have to do so for the PDFs defined by \textit{almost every permutation of the component conditional distributions}. Actually, the reality is worse. Returning to the covariance, we have
\begin{align}
\mathrm{Cov}_{\theta}(l_i(\theta),l_j(\theta)) =& \sum_{k \in \{0,1\}^i} (1 - \theta)^{i - \Sigma k} \theta^{\Sigma k} \mathbb{E}_k[l_i(\theta), l_j(\theta)] \\
&- \left( \sum_{k \in \{0,1\}^i} (1 - \theta)^{i - \Sigma k} \theta^{\Sigma k} \mathbb{E}_k[l_i(\theta)] \right) \times \left( \sum_{k \in \{0,1\}^i} (1 - \theta)^{i - \Sigma k} \theta^{\Sigma k} \mathbb{E}_k[l_j(\theta)] \right).
\end{align}
So the covariance between the information of different observations depends not only on \textit{every permutation} of the components, but also \textit{cross-product terms between every permutation}.

We could remove the cross-product issue by finding assumptions to ensure a stronger condition: stationarity. If we assume that $p_0$ and $p_1$ are markov processes, then the information associated with an observation is a function of only the last few observations. Then if the unconditional distributions converge to some distribution as $n \to \infty$, we could hope to achieve convergence in distribution of the normalised information. By Levy's continuity theorem, convergence in distribution is characterised by the convergence of the characteristic functions. The characteristic function is
\begin{equation}
\mathbb{E}_{\theta}[e^{i t X_n}] = \sum_{k \in \{0,1\}^n} (1 - \theta)^{n - \Sigma k} \theta^{\Sigma k} \mathbb{E}_k[e^{i t X_n}],
\end{equation}
where $i = \sqrt{-1}$. While this approach doesn't have the cross-product issue of dealing with the autocovariance terms, the permutation issue remains. If we want to ensure that the forecast distribution is stationary, it appears we need almost every permutation of the components to be stationary as well. Or something or the sort.

I've also looked at harris recursive markov chains and mixing processes, and from what I can see we can expect similar issues. While I'm sure one could construct sufficient conditions along these lines in satisfaction of \textcite{Sweeting1980}, it seems unlikely that they would be easily verifiable in practice.

I'll leave you with one final observation. Take the derivative of the expectation with respect to the parameter:
\begin{equation}
\frac{\partial}{\partial \theta} \mathbb{E}_{\theta}[G_n] = \sum_{k \in \{0,1\}^n} \left( \frac{n-\Sigma k}{1 - \theta} + \frac{\Sigma k}{\theta} \right)  (1 - \theta)^{n - \Sigma k} \theta^{\Sigma k } \mathbb{E}_k[G_n].
\end{equation}
This new multiplier seems explosive. And indeed, if we take the derivative at $\theta = 0.5$,
\begin{align}
\left. \frac{\partial}{\partial \theta} \mathbb{E}_{\theta}[G_n] \right\rvert_{\theta = 0.5} &= \sum_{k \in \{0,1\}^n} \left( 2(n - \Sigma k) + 2 \Sigma k \right) (1 - 0.5)^{n - \Sigma k} 0.5^{\Sigma k} \mathbb{E}_k[G_n] \\
&= 2n \sum_{k \in \{0,1\}^n} (1 - 0.5)^{n - \Sigma k} 0.5^{\Sigma k} \mathbb{E}_k[G_n] \\
&= 2 n \mathbb{E}_{\theta}[G_n].
\end{align}
In general, it seems the derivative of the expectation does not exist in the limit. This cannot bode well for analysis. Consider the comment by \textcite{Sweeting1980} that in most applications, $A_n(\theta) = \mathbb{E}_{\theta}[\mathscr{I}_n(\theta)]$ suffices, but without its derivative bounded, C2 won't hold. Reflecting on the appearance of this distributional forecast combination as a distributional random walk, perhaps some of these properties shouldn't be surprising. Maybe therein lies a reason why inference seems conspicuously absent from the forecast combination literature.

\section{Where to go from here?}

\begin{enumerate}
\item \textbf{Continue analysing the distributional forecast combination in the hopes of finding a set of practical sufficient conditions for the uniform asymptotic normality of the maximium likelihood estimator, and hence for the validity of the bootstrap.} Right now I'm not seeing many encouraging signs that such a set of conditions exists, let alone whether establishing such conditions could be done in a timely manner. Perhaps there are avenue's I haven't considered, such as other forms of mixing or stationarity, that ought to be preserved under the linear combination.
\item \textbf{Switch the type of combination in the hopes that a different sort of combination will be easier to analyse.} It seems to me that the issues with the linear pool that I've explored here come from the interaction of dependence and the combination, regardless of the type of combination. In particular, the distributional random walk resemblence still seems to hold. Though I'm happy to be persuaded otherwise, perhaps there's something I haven't considered.
\item \textbf{Brush these analytical concerns under the rug with ``under standard asymptotic arguments (see \cite{Sweeting1980})...''} We could still included the analysis showing the bootstrap works if (uniform) MLE does, but otherwise focus on the distributional forecast combination.
\item \textbf{Attempt to find a counter example: two stationary processes that when combined lead to a model without a (uniformly) asymptotically normal MLE.} This direction remains focused on forecast combinations, but doesn't involve the bootstrap in any significant way. This direction also seems unlikely to lead to any insights about particular datasets or empirical problems. On the other hand, perhaps it is worth shining a light on the potentially chaotic nature of these (or this) combination(s).
\item \textbf{Focus on the bootstrapping of maximum likelihood estimators in general, and restrict our attention to models that are known to be uniformly asymptotically normal in the sense of \textcite{Sweeting1980}.} While this approach draws our focus away from distributional forecast combinations in particular, it would allow us to highlight the generality of our results on bootstrap so far (essentially a time-series parametric-bootstrap version of \textcite{Mammen1992}, but still working on the only-if part...). No doubt there are plenty of models and problems currently active in the literature where the bootstrap can provide further insights. If we went down this road, the proof for the validity of the parametric bootstrap would be feature-complete, and we could immediately make a start on the empirical/practitioner section of the project with the reasonable hope that we would remain undisturbed by issues in the theory down the track. Our approach for the theory differs from e.g. \textcite{Andrews2005} by being easier to apply, but without the higher-order improvements. This is my preferred option.
\end{enumerate}

\printbibliography

\end{document}