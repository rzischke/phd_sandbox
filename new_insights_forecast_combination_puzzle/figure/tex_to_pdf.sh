#!/bin/bash

# The latex files FIG1.tex, FIG2.tex,..., which contain visualisations produced by geweke_plot.R and smith_plot.R,
# can sometimes be too large to compile with pdflatex. Unfortunately, lualatex (which can compile large files) is
# not supported by arxiv. The solution is to compile the plots from .tex to .pdf using lualatex first (which we
# do below), and then \includegraphics the pdf versions in the latex source for the paper and presentation (which
# we compile using pdflatex). After running this script, compilation of the paper and presentation should proceed
# successfully.

for n in {1..9}
do
    #dvilualatex --interaction=nonstopmode --jobname="FIG${n}" "FIG${n}.tex" && dvips -o "FIG${n}.eps" "FIG${n}.dvi"
    lualatex --interaction=nonstopmode --jobname="FIG${n}" "FIG${n}.tex"
done

exit 0

