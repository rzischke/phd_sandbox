\documentclass[english,12pt]{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{times}
\usepackage{amsfonts}
\usepackage{amsmath,amssymb}
\usepackage{amsthm}
\usepackage{natbib}
\usepackage{bm}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{appendix}
\usepackage[figuresright]{rotating}
\usepackage{ctable}
\usepackage{array}
\usepackage{tabularx}
\usepackage{tabulary}
\usepackage{footnote}
\usepackage{threeparttable}
\usepackage{float}
\usepackage{mathrsfs}
\usepackage{dsfont}
\usepackage{nicefrac}
\usepackage{parskip}
\usepackage{bm}
\usepackage{xspace}
\usepackage{color}
\usepackage[colorlinks,linkcolor=blue,citecolor=blue,bookmarks=false,pagebackref]{hyperref}
\usepackage[margin=2cm]{geometry}
\usepackage{setspace}
\usepackage{titling}
\usepackage{mathabx}
\usepackage{placeins}
\setcounter{MaxMatrixCols}{10}
%TCIDATA{OutputFilter=LATEX.DLL}
%TCIDATA{Version=5.50.0.2960}
%TCIDATA{Codepage=1252}
%TCIDATA{<META NAME="SaveForMode" CONTENT="1">}
%TCIDATA{BibliographyScheme=BibTeX}
%TCIDATA{LastRevised=Thursday, December 17, 2020 11:52:15}
%TCIDATA{<META NAME="GraphicsSave" CONTENT="32">}

\setstretch{1.2}
\settowidth{\thanksmarkwidth}{*}
\setlength{\thanksmargin}{-\thanksmarkwidth}
\allowdisplaybreaks
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}[section]
\theoremstyle{plain}
\newtheorem{lemma}{Lemma}[section]
\theoremstyle{plain}
\newtheorem{assumption}{Assumption}[section]
\theoremstyle{plain}
\newtheorem{proposition}{Proposition}[section]
\theoremstyle{plain}
\newtheorem{corollary}{Corollary}[section]
\theoremstyle{remark}
\newtheorem{remark}{Remark}[section]
\theoremstyle{comment}
\newtheorem{comment}{Comment}

\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]
\theoremstyle{definition}
\newtheorem{example}{Example}[section]
\newcommand{\plim}{\operatornamewithlimits{plim\,}}
\newcommand{\argmin}{\operatornamewithlimits{argmin\,}}
\newcommand{\arginf}{\operatornamewithlimits{arginf\,}}
\newcommand{\argmax}{\operatornamewithlimits{argmax\,}}
\newcommand{\argsup}{\operatornamewithlimits{argsup\,}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\diag}{\mathrm{diag}}
\newcommand{\Var}{\mathrm{Var}}
\newcommand{\Cov}{\mathrm{Cov}}
\newcommand{\CV}{\mathrm{CV}}
\newcommand{\Corr}{\mathrm{Corr}}
\newcommand{\bu}{\boldsymbol{u}}
\newcommand{\bz}{\boldsymbol{z}}
\newcommand{\by}{\boldsymbol{y}}
\newcommand{\bx}{\boldsymbol{x}}
\newcommand{\was}{\mathcal{W}}
\newcommand{\pdist}{\mathcal{D}}
\newcommand{\dx}{\text{d}}
\mathchardef\mhyphen="2D
\def\y{y_1^n}
\def\x{x_1^n}
\def \zb{\mathbf{z}}
\def \t{\theta}
\def \p{\partial}
\def \R{\mathbb{R}}
\def \E{\mathbb{E}}
\def \H {\mathrm{H}}
\def \SR {\mathrm{SR}}
\def \mrn {\mathbb{R}^{n}}
\def \mr {\mathbb{R}}
\def \grad {\nabla}
\newcommand{\1}[1]{\mathds{1}\left[#1\right]}
\def \CF {\operatorname{CF}}
\newcommand\floor[1]{\lfloor#1\rfloor}
\def \dt {\mathrm{d}}
\newtheorem{innercustomthm}{Theorem}
\newenvironment{customthm}[1]
{\renewcommand\theinnercustomthm{#1}\innercustomthm}
{\endinnercustomthm}
\floatstyle{ruled}
\newfloat{algorithm}{tbp}{loa}
\providecommand{\algorithmname}{Algorithm}
\floatname{algorithm}{\protect\algorithmname}


\begin{document}

Gael, David and Don,

I'm writing this letter to articulate some surprising (to me at least) results that I came across when visualising the variability of the average loss difference, under the null, as a function of sample size, in the Smith and Wallis exercise of the testing paper. I'm working off of the version of the testing paper dated the 27th of April, and this discussion focuses primarily on the correspondence (or lack thereof) between said variability and Lemma 4.1 on Page 11. I started investigating this variability at your suggestion David, in Remark 4.5 on Page 15.

I had expected that, under the null, the loss difference of the Smith and Wallis exercise would behave according to the asymptotic distribution given in Lemma 4.1: that $\sqrt{P} \Delta_P(\tilde{\theta}_R, \theta^{\star}) \Rightarrow N(0, \Omega)$, where $P$ is the length of the out-of-sample evaluation period, $\tilde{\theta}_R$ is the two-stage parameter estimate obtained with the $R$ in-sample observations, $\theta^{\star}$ is the pseudo-true parameter vector to which $\tilde{\theta}_R$ converges as $R \to \infty$, and $\Delta_P$ is the average loss difference given in the equation after (4) on Page 11 of the paper. The hypothesis test itself is explained at the top of Page 11.

In fact, the statistic $\Delta_P(\tilde{\theta}_R, \theta^{\star})$ is slightly (or not so slightly as we shall see) different from the statistic that is used in the Smith and Wallis exercise. In particular, the asymptotic distribution given in Lemma 4.1 assumes that we are testing a (true) null hypothesis of no inferior forecast accuracy of a \textit{fixed} benchmark $\theta^{\star}$ against the two-stage alternative $\tilde{\theta}_R$. But in the Smith and Wallis exercise we are testing a null hypothesis of no inferior forecast accuracy of an equally-weighted benchmark (for example) against the two-stage optimally-weighted alternative, where under both the null and alternative forecasts the constituent model parameters are set to their two-stage estimates $\tilde{\gamma}_R$. In this case, the benchmark is \textit{not} fixed, and the statistic used is $\Delta_P(\tilde{\theta}_R, (\eta^{\star \prime}, \tilde{\gamma}_R^{\prime})^{\prime})$, \textit{not} $\Delta_P(\tilde{\theta}_R, \theta^{\star})$.

Skipping ahead a little: because both the benchmark and alternative forecasts in $\Delta_P(\tilde{\theta}_R, (\eta^{\star \prime}, \tilde{\gamma}_R^{\prime})^{\prime})$ depend on the same constituent model parameter estimates, it turns out that there is a ``cancelling out'' of the variability in the average loss difference $\Delta_P$ that is due to estimating the constituent models, so that the variability of $\Delta_P(\tilde{\theta}_R, (\eta^{\star \prime}, \tilde{\gamma}_R^{\prime})^{\prime})$ is instead dominated by estimation in the weights. Note that this is the reverse of the behaviour of $\Delta_P(\tilde{\theta}_R, \theta^{\star})$, whose variability is dominated by estimation of the constituent model parameters (this was showed in Lemma 4.1). The reason for the zero size control of the test of no inferior forecast accuracy of the e.g. equally weighted benchmark against the optimally weighted alternative is that $\Delta_P(\tilde{\theta}_R, (\eta^{\star \prime}, \tilde{\gamma}_R^{\prime})^{\prime})$ is not merely $\mathcal{O}_p(P^{-1/2})$, but $\mathcal{O}_p(P^{-1})$. Below I also derive the asymptotic distribution of $\Delta_P(\tilde{\theta}_R, (\eta^{\star \prime}, \tilde{\gamma}_R^{\prime})^{\prime})$, taking inspiration from the beginning of the proof of Lemma A.2 on Page 33. But first I'll go through the surprising numerical results that lead to this.

\begin{figure*}[h]
	\includegraphics[width = \textwidth]{figure/FIG6}
\end{figure*}

At the top of the next page is a figure that plots estimates of the variance ($y$-axis) of a variety of random sequences (colours) across a range of sample sizes ($x$-axis). Here, $n$ is the overall sample size, which is split into an estimation period of length $P = n/2$ and an evaluation period of length $R = n/2$.

The variance of $\sqrt{P} \Delta_P(\tilde{\theta}_R, (\eta^{\star \prime}, \tilde{\gamma}_R^{\prime})^{\prime})$ (green) is the variance of the average loss difference between the equally-weighted two-stage combination and the optimally-weighted two-stage combination, for the DGP and combination given in Smith and Wallis, under the MSE loss, and where $\eta^{\star} = 0.5$. The asymptotic variance $\Omega$ of $\sqrt{P} \Delta_P(\tilde{\theta}_R, \theta^{\star})$ is displayed in red, which is consistent with the actual variance of $\sqrt{P} \Delta_P(\tilde{\theta}_R, \theta^{\star})$ given in blue: the red and blue lines are close for large sample sizes. If it were the case that the asymptotic distribution of the loss difference $\Delta_P(\tilde{\theta}_R, (\eta^{\star \prime}, \tilde{\gamma}_R^{\prime})^{\prime})$ between the equally-weighted and optimally-weighted combinations was identical to the asymptotic distribution of $\Delta_P(\tilde{\theta}_R, \theta^{\star})$ given in Lemma 4.1, i.e. that $\sqrt{P} \Delta_P(\tilde{\theta}_R, (\eta^{\star \prime}, \tilde{\gamma}_R^{\prime})^{\prime}) \Rightarrow N(0, \Omega)$, then we would expect the variance of $\sqrt{P} \Delta_P(\tilde{\theta}_R, (\eta^{\star \prime}, \tilde{\gamma}_R^{\prime})^{\prime})$ in green to approach the asymptotic variance in red as the sample size increased, just as we observed for the variance of $\sqrt{P} \Delta_P(\tilde{\theta}_R, \theta^{\star})$ in blue. Clearly this is not the case, as the green line continues to fall further from the red line as the sample size increases.

That the variance of $\sqrt{P} \Delta_P(\tilde{\theta}_R, (\eta^{\star \prime}, \tilde{\gamma}_R^{\prime})^{\prime})$ in green does not stabilise to some positive value is a sign that it converges at a faster rate than $P^{-1/2}$. If we normalise the loss difference $\Delta_P(\tilde{\theta}_R, (\eta^{\star \prime}, \tilde{\gamma}_R^{\prime})^{\prime})$ by $P$ instead of $\sqrt{P}$, the resulting variance in purple varies remarkably little across all sample sizes considered, indicating that $\Delta_P(\tilde{\theta}_R, (\eta^{\star \prime}, \tilde{\gamma}_R^{\prime})^{\prime})$ converges to zero at a rate of $P^{-1}$.

Given this insight from the numerical results, let us now attempt to find the asymptotic distribution of $P \Delta_P(\tilde{\theta}_R, (\eta^{\star \prime}, \tilde{\gamma}_R^{\prime})^{\prime})$ using standard Taylor-series-based techniques. For this derivation I'm going to assume we have full derivatives available (and not merely left/right derivatives). First define the average out-of-sample loss as follows:
\begin{equation*}
L_P(\theta) = L_P(\eta, \gamma) = P^{-1} \sum_{t=R}^{R+P} \ell_t(\theta),
\end{equation*}
where $\ell_t(\theta)$ is the loss assigned to $\theta$ on observing the observation at time $t$. Expressing the statistic $\Delta_P$ in terms of average losses gives the average loss difference
\begin{equation*}
\Delta_P(\vartheta_R, \theta_R) = L_P(\vartheta_R) - L_P(\theta_R),
\end{equation*}
for two arbitrary sequences of parameter values $\vartheta_R$ and $\theta_R$. Recall that $\ell_t$, $\Delta_P$, $\vartheta_R$ and $\theta_R$ can be found in context in the first half of Page 11 of the paper, where the test of no inferior forecast accuracy is described. Now take a second-order Taylor series expansion of the function $\eta \mapsto L_P(\eta, \tilde{\gamma}_R)$ around $\eta = \eta^{\star}$, and then evaluate the expansion at $\eta = \tilde{\eta}_R$:
\begin{equation*}
L_P(\tilde{\eta}_R, \tilde{\gamma}_R) = L_P(\eta^{\star}, \tilde{\gamma}_R) + (\tilde{\eta}_R - \eta^{\star})^{\prime} \grad_{\eta} L_P(\eta^{\star}, \tilde{\gamma}_R) + \frac{1}{2} (\tilde{\eta}_R - \eta^{\star})^{\prime} [\grad_{\eta \eta} L_P(\overline{\eta}_R, \tilde{\gamma}_R)] (\tilde{\eta}_R - \eta^{\star}),
\end{equation*}
where $\lVert \overline{\eta}_R - \eta^{\star} \rVert \leq \lVert \tilde{\eta}_R - \eta^{\star} \rVert$, and where $\grad_{\eta} f$ and $\grad_{\eta \eta} f$ represent the gradient and hessian, respectively, of a function $f$ with respect to the weights $\eta$. This next step is the most important. Take a first-order Taylor series expansion (really just an application of the mean value theorem) of the function $\eta \mapsto \grad_{\eta} L_P(\eta, \tilde{\gamma}_R)$ around $\eta = \tilde{\eta}_P$, where $\tilde{\eta}_P = \argmin_{\eta} L_P(\eta, \tilde{\gamma}_R)$. Evaluate this expansion at $\eta = \eta^{\star}$, and substitute this expansion in place of the gradient term in the initial expansion above:
\begin{align*}
L_P(\tilde{\eta}_R, \tilde{\gamma}_R) =& L_P(\eta^{\star}, \tilde{\gamma}_R) + (\tilde{\eta}_R - \eta^{\star})^{\prime} \left( \grad_{\eta} L_P(\tilde{\eta}_P, \tilde{\gamma}_R) + [\grad_{\eta \eta} L_P(\overline{\eta}_P, \tilde{\gamma}_R)] (\eta^{\star} - \tilde{\eta}_P) \right) \\ &+ \frac{1}{2} (\tilde{\eta}_R - \eta^{\star})^{\prime} [\grad_{\eta \eta} L_P(\overline{\eta}_R, \tilde{\gamma}_R)] (\tilde{\eta}_R - \eta^{\star}),
\end{align*}
where $\lVert \overline{\eta}_P - \eta^{\star} \rVert \leq \lVert \tilde{\eta}_P - \eta^{\star} \rVert$. Since $\eta = \tilde{\eta}_P$ minimises $L_P(\eta, \tilde{\gamma}_R)$, standard regularity conditions give $\grad_{\eta} L_P(\tilde{\eta}_P, \tilde{\gamma}_R) = 0$. Now move the leading $L_P(\eta^{\star}, \tilde{\gamma}_R)$ term to the left-hand side, multiply by $P$, and rearrange the right-hand side so that the appropriate sample-size multipliers are in place for an application of the continuous mapping theorem:
\begin{align*}
P(L_P(\tilde{\eta}_R, \tilde{\gamma}_R) - L_P(\eta^{\star}, \tilde{\gamma}_R))
=& \sqrt{\frac{P}{R}} \sqrt{R}(\tilde{\eta}_R - \eta^{\star})^{\prime} [\grad_{\eta \eta} L_P(\overline{\eta}_P, \tilde{\gamma}_R)] \sqrt{P}(\eta^{\star} - \tilde{\eta}_P)  \\ &+ \frac{P}{R} \frac{1}{2} \sqrt{R} (\tilde{\eta}_{R} - \eta^{\star})^{\prime} [\grad_{\eta \eta} L_P(\overline{\eta}_R, \tilde{\gamma}_R)] \sqrt{R}(\tilde{\eta}_R - \eta^{\star}).
\end{align*}
Under regularity, we have $\grad_{\eta \eta} L_P(\overline{\eta}_P, \tilde{\gamma}_R) \overset{p}{\to} \grad_{\eta \eta} \mathcal{L}(\eta^{\star}, \gamma^{\star})$ and $\grad_{\eta \eta} L_P(\overline{\eta}_R, \tilde{\gamma}_R) \overset{p}{\to} \grad_{\eta \eta} \mathcal{L}(\eta^{\star}, \gamma^{\star})$, where $\mathcal{L}$ is the limiting average loss (in the paper, it features in Lemma 4.1, for example). If we assume that $P/R \to c$ and $(\sqrt{R} (\tilde{\eta}_R - \eta^{\star}), \sqrt{P} (\tilde{\eta}_P - \eta^{\star})) \Rightarrow (X, Y)$ as $n \to \infty$ for some pair of random variables $X$ and $Y$, then by the continuous mapping theorem, we obtain the desired asymptotic distribution:
\begin{align*}
P \Delta_P(\tilde{\theta}_R, (\eta^{\star \prime}, \tilde{\gamma}_{R}^{\prime})^{\prime}) &= P(L_P(\tilde{\eta}_R, \tilde{\gamma}_R) - L_P(\eta^{\star}, \tilde{\gamma}_R)) \\
&\Rightarrow \sqrt{c} X^{\prime} [\grad_{\eta \eta} \mathcal{L}(\eta^{\star}, \gamma^{\star})] (-Y) + c \frac{1}{2} X^{\prime} [\grad_{\eta \eta} \mathcal{L}(\eta^{\star}, \gamma^{\star})] X \\
&= X^{\prime} [\grad_{\eta \eta} \mathcal{L}(\eta^{\star}, \gamma^{\star})] \left( \frac{1}{2} c X - \sqrt{c} Y \right).
\end{align*}

\end{document}